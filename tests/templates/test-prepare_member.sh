#!/usr/bin/bash

#-- > auto-icon test: test the prepare member job. < --#

# Copyright 2023 - 2024 Karlsruhe Institute of Technology (KIT) and Ludwig-Maximilians-Universität München (LMU)
# SPDX-FileContributor: Andreas Baer
#
# SPDX-License-Identifier: BSD-3-Clause

set -e  #-- This will be set by autosubmit anyway and allows for proper shell syntax checking.

#-- ----- -------------------- ----- --#
#-- Write the list of files or directory to an output file
#-- and calculate an md5sum of it.
#-- Arguments:
#--   Output file to be written
#--   Type ('DIR'|'FILE')
#-- Outputs:
#--   md5sum of output file
#-- ----- -------------------- ----- --#
function write_files_list() {
    local OUTPUT=$1
    local KIND=$2
    if [ "$KIND" = DIR ]
    then
        find -L . -type d | sort > "$OUTPUT"
    elif [ "$KIND" = FILE ]
    then
        find -L . -type f | sort | xargs md5sum > "$OUTPUT"
    else
        echo "ERROR [510]: wrong kind supplied ('$KIND' is neither DIR nor FILE)."
        exit 1
    fi
    #-- print out sum
    md5sum "$OUTPUT" | cut -d' ' -f 1
}

#-- ----- -------------------- ----- --#
#-- Extract the relevant information out of a reference file
#-- and write it to a temporary file
#-- Arguments:
#--   File name of reference file
#--   Type ('DIR'|'FILE')
#-- Outputs:
#--   File name of temporary file
#-- ----- -------------------- ----- --#
function write_ref_file() {
    local INPUT=$1
    local KIND=$2
    TMPFN=$(mktemp)
    sed -n "/^$MEMBERSTRING $KIND /s/^$MEMBERSTRING $KIND //p" "$INPUT" > "$TMPFN"
    echo "$TMPFN"
}



#-- ----- Main script ----- --#
OUTDIR="%DIRECTORIES.OUTDIR%/%SDATE%/%MEMBER%"
JOBNAME="%JOBNAME%"
LOGDIR="%CURRENT_LOGDIR%"
EXPNAME="%EXPNAME%"
SDATE="%SDATE%"
MEMBER="%MEMBER%"
MEMBERSTRING="$EXPNAME $SDATE $MEMBER"
REFERENCE="${LOGDIR}/ref-prepare_experiment_${JOBNAME#%DEFAULT.EXPID%_}"
NEW_REFERENCE="${LOGDIR}/ref-prepare_experiment_${JOBNAME#%DEFAULT.EXPID%_}-new"
LIST_DIRS="${LOGDIR}/ref-prepare_experiment-${EXPNAME}_${SDATE}_${MEMBER}-dirs.dat"
LIST_FILES="${LOGDIR}/ref-prepare_experiment-${EXPNAME}_${SDATE}_${MEMBER}-files.dat"

cd "$OUTDIR"

SUM_DIRS=$(write_files_list "$LIST_DIRS" DIR)
SUM_FILES=$(write_files_list "$LIST_FILES" FILE)
REF_DIRS=$(write_ref_file "$REFERENCE" DIR)
REF_FILES=$(write_ref_file "$REFERENCE" FILE)

TMPFN_DIRS=$(mktemp)
TMPFN_FILES=$(mktemp)
sed "s/^/$MEMBERSTRING DIR /" "$LIST_DIRS" > "$TMPFN_DIRS"
sed "s/^/$MEMBERSTRING FILE /" "$LIST_FILES" > "$TMPFN_FILES"
cat "$TMPFN_DIRS" "$TMPFN_FILES" > "$NEW_REFERENCE"

REF_SUM_DIRS=$(md5sum "$REF_DIRS" | cut -d' ' -f 1)
REF_SUM_FILES=$(md5sum "$REF_FILES" | cut -d' ' -f 1)

#-- Check for difference of files and directories
#-- Store if it failed
FAILED=0
if [ ! "$SUM_FILES" = "$REF_SUM_FILES" ]
then
    echo "ERROR: files are not equal!" >&2
    diff "$REF_FILES" "$LIST_FILES" || FAILED=$((FAILED+1))
fi
if [ ! "$SUM_DIRS" = "$REF_SUM_DIRS" ]
then
    echo "ERROR: dirs are not equal!" >&2
    diff "$REF_DIRS" "$LIST_DIRS" || FAILED=$((FAILED+1))
fi

#-- Clean up
rm "$REF_FILES" "$REF_DIRS" "$TMPFN_DIRS" "$TMPFN_FILES" "$LIST_DIRS" "$LIST_FILES"

#-- Fail the test
if [ "$FAILED" -gt 1 ]
then
    echo "ERROR: not everything is equal, test failed!" >&2
    exit 1
fi
