! Copyright 2023 - 2024 Karlsruhe Institute of Technology (KIT) and Ludwig-Maximilians-Universität München (LMU)
! SPDX-FileContributor: Andreas Baer
!
! SPDX-License-Identifier: BSD-3-Clause

!-- > Atmo namelist for REINIT_CHEM template. < --!

&parallel_nml
 nproma         = 8  ! optimal setting 8 for CRAY; use 16 or 24 for IBM
 p_test_run     = .false.
 l_test_openmp  = .false.
 l_log_checks   = .false.
 num_io_procs   = 4   ! up to one PE per output stream is possible
 iorder_sendrecv = 3  ! best value for CRAY (slightly faster than option 1)
 num_restart_procs = 20
/
&grid_nml
 !dynamics_grid_filename  = "%EXPERIMENT.GRID%"
 lredgrid_phys           = .false.
 lfeedback               = .true.
 ifeedback_type          = 2
/
&initicon_nml
 init_mode       = 2            ! operation mode 2: IFS
 lread_ana       = .true.   ! analysis data will be read
 filetype        =   %SIMULATION.FILETYPE%
 ltile_coldstart = .false.
/
&run_nml
 num_lev        = 90
 lvert_nest     = .true.       ! use vertical nesting if a nest is active
 dtime          = ${DTIME}     ! timestep in seconds
 ldynamics      = .TRUE.       ! dynamics
 ltransport     = .true.
 iforcing       = 3            ! NWP forcing
 ltestcase      = .false.      ! false: run with real data
 msg_level      = 7            ! print maximum wind speeds every 5 time steps
 timers_level   = 10            ! can be increased up to 10 for detailed timer output
 output         = "nml"
 lart           = .true.
/
&nwp_phy_nml
 inwp_gscp       = 1
 inwp_convection = 1
 inwp_radiation  = 1
 inwp_cldcover   = 1
 inwp_turb       = 1
 inwp_satad      = 1
 inwp_sso        = 1
 inwp_gwd        = 1
 inwp_surface    = 1
 icapdcycl       = 3 ! apply CAPE modification to improve diurnalcycle over tropical land (optimizes NWP scores)
 latm_above_top  = .false., .true.  ! the second entry refers to the nested domain (if present)
 efdt_min_raylfric = 7200.
 itype_z0         = 2
 icpl_aero_conv   = 1
 icpl_aero_gscp   = 1
 ! resolution-dependent settings - please choose the appropriate one
 dt_rad    = 2160.
 dt_conv   = 720.
 dt_sso    = 1440.
 dt_gwd    = 1440.
/
&nwp_tuning_nml
 tune_zceff_min = 0.075 ! ** default value to be used for R3B7; use 0.05 for R2B6 in order to get similar temperature biases in upper troposphere **
 itune_albedo   = 1     ! somewhat reduced albedo (w.r.t. MODIS data) over Sahara in order to reduce cold bias
/
&turbdiff_nml
 tkhmin  = 0.75  ! new default since rev. 16527
 tkmmin  = 0.75  !
 pat_len = 100.
 c_diff  = 0.2
 rat_sea = 0.85  ! ** new since r20191: 8.5 for R3B7, 8.0 for R2B6 in order to get similar temperature biases in the tropics **
 rlam_heat = 10.
 ltkesso = .true.
 frcsmot = 0.2      ! these 2 switches together apply vertical smoothing of the TKE source terms
 imode_frcsmot = 2  ! in the tropics (only), which reduces the moist bias in the tropical lower troposphere
 ! use horizontal shear production terms with 1/SQRT(Ri) scaling to prevent unwanted side effects:
 itype_sher = 3
 ltkeshs    = .true.
 a_hshr     = 2.0
/
&lnd_nml
 ntiles         = 3      !!! 1 for assimilation cycle and forecast
 nlev_snow      = 3      !!! 1 for assimilation cycle and forecast
 lmulti_snow    = .true. !!! .false. for assimilation cycle and forecast
 itype_heatcond = 2
 idiag_snowfrac = 2
 lsnowtile      = .false.  !! later on .true. if GRIB encoding issues are solved
 lseaice        = .true.
 llake          = .false.
 itype_lndtbl   = 3  ! minimizes moist/cold bias in lower tropical troposphere
 itype_root     = 2
/
&radiation_nml
 irad_o3       = 7
 irad_aero     = 6
 albedo_type   = 2 ! Modis albedo
 vmr_co2       = 390.e-06 ! values representative for 2012
 vmr_ch4       = 1800.e-09
 vmr_n2o       = 322.0e-09
 vmr_o2        = 0.20946
 vmr_cfc11     = 240.e-12
 vmr_cfc12     = 532.e-12
/
&nonhydrostatic_nml
 iadv_rhotheta  = 2
 ivctype        = 2
 itime_scheme   = 4
 exner_expol    = 0.333
 vwind_offctr   = 0.2
 damp_height    = 50000.
 rayleigh_coeff = 0.10
 divdamp_order  = 24    ! for data assimilation runs, '2' provides extra-strong filtering of gravity waves
 divdamp_type   = 32    !!! optional: 2 for assimilation cycle if very strong gravity-wave filtering is desired
 divdamp_fac    = 0.004
 igradp_method  = 3
 l_zdiffu_t     = .true.
 thslp_zdiffu   = 0.02
 thhgtd_zdiffu  = 125.
 htop_moist_proc= 22500.
 hbot_qvsubstep = 22500. ! use 19000. with R3B7
/
&sleve_nml
 min_lay_thckn   = 20.
 max_lay_thckn   = 400.   ! maximum layer thickness below htop_thcknlimit
 htop_thcknlimit = 14000. ! this implies that the upcoming COSMO-EU nest will have 60 levels
 top_height      = 75000.
 stretch_fac     = 0.9
 decay_scale_1   = 4000.
 decay_scale_2   = 2500.
 decay_exp       = 1.2
 flat_height     = 16000.
/
&transport_nml
!                qv, qc, qi, qr, qs
 itype_vlimit  = 1,1,1,1,1
 ivadv_tracer = 3, 3, 3, 3, 3
 itype_hlimit = 3, 4, 4, 4 , 4
 ihadv_tracer = 52, 2,2,2,2
 iadv_tke      = 0
/
&diffusion_nml
 hdiff_order      = 5
 itype_vn_diffu   = 1
 itype_t_diffu    = 2
 hdiff_efdt_ratio = 24.0
 hdiff_smag_fac   = 0.025
 lhdiff_vn        = .TRUE.
 lhdiff_temp      = .TRUE.
/
&interpol_nml
 nudge_zone_width  = 8
 lsq_high_ord      = 3
 l_intp_c2l        = .true.
 l_mono_c2l        = .true.
 support_baryctr_intp = .false.
/
&extpar_nml
 itopo          = 1
 n_iter_smooth_topo = 1
 heightdiff_threshold = 3000.
/
&io_nml
 itype_pres_msl = 5  ! IFS method with bug fix for self-consistency between SLP and geopotential
 itype_rh       = 1  ! RH w.r.t. water
 restart_write_mode = "dedicated procs multifile"
/
&output_nml
 filetype                     =  %SIMULATION.FILETYPE%         ! output format: 2=GRIB2, 4=NETCDFv2
 dom                          =  -1                  ! write all domains
 output_start     = "${OUTPUT_START}"
 output_end       = "${OUTPUT_END}"
 output_interval  = "%EXPERIMENT.OUTPUT_INTERVAL%"
 steps_per_file               = 8
 include_last                 =  .TRUE.
 output_filename              = 'icon-art-plev-%CHUNK_START_YEAR%-%CHUNK_START_MONTH%-%CHUNK_START_DAY%'                ! file name base
 p_levels                     = 100000,95000,92500,90000,85000,80000,70000,60000,50000,40000,30000,25000,20000,15000,10000,7000,5000,3000,2000,1000,700,500,300,200,100
 pl_varlist                   = 'z_mc','temp','rho','TRNH3_chemtr','TRHNO3_chemtr','TRH2SO4_chemtr','TRHCL_chemtr','nmb_sol_ait','nmb_sol_acc','nmb_sol_coa','nmb_insol_acc','nmb_insol_coa','nmb_mixed_acc','nmb_mixed_coa','nmb_giant','h2o_sol_ait','h2o_sol_acc','h2o_sol_coa','h2o_insol_acc','h2o_insol_coa','h2o_mixed_acc','h2o_mixed_coa','so4_sol_ait','so4_sol_acc','so4_sol_coa','so4_mixed_acc','so4_mixed_coa','so4_insol_acc','so4_insol_coa','nh4_sol_ait','nh4_sol_acc','nh4_sol_coa','nh4_mixed_acc','nh4_mixed_coa','nh4_insol_acc','nh4_insol_coa','no3_sol_ait','no3_sol_acc','no3_sol_coa','no3_mixed_acc','no3_mixed_coa','no3_insol_acc','no3_insol_coa','dusta','dustb','dust_mixed_acc','dust_mixed_coa','dustc'
 remap                        = 0
/
&output_nml
 filetype                     =  %SIMULATION.FILETYPE%        ! output format: 2=GRIB2, 4=NETCDFv2
 dom                          =  -1                 ! write all domains
 output_start     = "${OUTPUT_START}"
 output_end       = "${OUTPUT_END}"
 output_interval  = "%EXPERIMENT.OUTPUT_INTERVAL%"
 steps_per_file               = 8
 include_last                 =  .TRUE.
 output_filename              = 'icon-art-aero-%CHUNK_START_YEAR%-%CHUNK_START_MONTH%-%CHUNK_START_DAY%'
 ml_varlist                   = 'z_mc','temp','pres','rho','group:ART_CHEMISTRY','group:ART_AEROSOL','acc_emiss_dusta','acc_emiss_dustb','acc_emiss_dustc','emiss_dusta','emiss_dustb','emiss_dustc','diam_giant','diam_insol_acc','diam_insol_coa','diam_mixed_acc','diam_mixed_coa','diam_sol_acc','diam_sol_ait','diam_sol_coa', 'group:ART_ROUTINE_DIAG'
 remap                        = 0
/
&output_nml
 filetype                     =  %SIMULATION.FILETYPE%        ! output format: 2=GRIB2, 4=NETCDFv2
 dom                          =  -1                 ! write all domains
 output_start     = "${OUTPUT_START}"
 output_end       = "${OUTPUT_END}"
 output_interval  = "%EXPERIMENT.OUTPUT_INTERVAL%"
 steps_per_file               = 8
 include_last                 =  .TRUE.
 output_filename              = 'icon-art-met-%CHUNK_START_YEAR%-%CHUNK_START_MONTH%-%CHUNK_START_DAY%'                ! file name base
 ml_varlist                   = 'z_mc','temp','pres','rho','omega','fis','geopot','qv','qc','qi','qr','qs','group:MODE_INIANA','group:DWD_FG_SFC_VARS','group:DWD_FG_ATM_VARS'
 remap                        = 0
/
&art_nml
 lart_diag_out   = .TRUE.
 lart_aerosol    = .TRUE.
 lart_chem       = .TRUE.
 lart_chemtracer = .TRUE.

 iart_init_aero   = 0
 iart_init_gas    = 4
 iart_dust        = 1

 iart_modeshift   = 1          ! 0 = off; 1 = on
 iart_isorropia   = 1          ! 0 = off; 1 = on

 cart_emiss_xml_file   = '%DIRECTORIES.INDIR%/xml/emissions_R2B05_0014_cs_tr.xml'
 cart_chemtracer_xml   = '%DIRECTORIES.INDIR%/xml/tracers_chemtracer.xml'
 cart_input_folder     = '.'
 cart_cheminit_coord   = 'mozart_coord.nc'
 cart_cheminit_file    = 'camchem-data/camchem-%CHUNK_START_YEAR%-%CHUNK_START_MONTH%-%CHUNK_START_DAY%-remapkx.nc'
 cart_cheminit_type    = 'MOZART'

 cart_diagnostics_xml = '%DIRECTORIES.INDIR%/xml/diagnostics_dust.xml'
 cart_aerosol_xml = '%DIRECTORIES.INDIR%/xml/tracers_aerosol.xml'
 cart_modes_xml   = '%DIRECTORIES.INDIR%/xml/modes_dust_rad_case0.xml'
 cart_coag_xml    = '%DIRECTORIES.INDIR%/xml/coagulate_coarse.xml'
 cart_aero_emiss_xml = '%DIRECTORIES.INDIR%/xml/aero_emiss_dust_aging.xml'
/
