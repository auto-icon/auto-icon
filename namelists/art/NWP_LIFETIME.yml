# Copyright 2004-2024 DWD, MPI-M, DKRZ, KIT, ETH, MeteoSwiss
# Copyright 2023-2024 Karlsruhe Institute of Technology (KIT) and Ludwig-Maximilians-Universität München (LMU)
# SPDX-FileContributor: Andreas Baer
#
# SPDX-License-Identifier: BSD-3-Clause

NAMELIST:
  parallel_nml:
    nproma: 8  # optimal setting 8 for CRAY; use 16 or 24 for IBM
    p_test_run: False
    l_test_openmp: False
    l_log_checks: False
    num_io_procs: 2   # up to one PE per output stream is possible
    iorder_sendrecv: 3  # best value for CRAY (slightly faster than option 1)

  # grid_nml:
  #   dynamics_grid_filename: ${DYNAMICS_GRID_FILENAME}
  #   radiation_grid_filename: ${RADIATION_GRID_FILENAME}
  #   lredgrid_phys: True

  initicon_nml:
    init_mode: 2           # operation mode 2: IFS
    zpbl1: 500. 
    zpbl2: 1000. 

  run_nml:
    num_lev: 90
    lvert_nest: True       # use vertical nesting if a nest is active
    # dtime: ${DTIME}     # timestep in seconds
    ldynamics: True       # dynamics
    ltransport: True
    iforcing: 3            # NWP forcing
    ltestcase: False      # false: run with real data
    msg_level: 7            # print maximum wind speeds every 5 time steps
    ltimer: True      # set True for timer output
    timers_level: 15            # can be increased up to 10 for detailed timer output
    output: "nml"
    lart: True

  nwp_phy_nml:
    inwp_gscp: 1
    inwp_convection: 1
    inwp_radiation: 4
    inwp_cldcover: 1
    inwp_turb: 1
    inwp_satad: 1
    inwp_sso: 1
    inwp_gwd: 1
    inwp_surface: 1
    icapdcycl: 3 # apply CAPE modification to improve diurnalcycle over tropical land (optimizes NWP scores)
    latm_above_top: [False, True]  # the second entry refers to the nested domain (if present)
    efdt_min_raylfric: 7200.
    itype_z0: 2
    icpl_aero_conv: 1
    icpl_aero_gscp: 1
 # resolution-dependent settings - please choose the appropriate one
    dt_rad: 2160.
    dt_conv: 720.
    dt_sso: 1440.
    dt_gwd: 1440.

  nwp_tuning_nml:
    tune_zceff_min: 0.075 # ** default value to be used for R3B7; use 0.05 for R2B6 in order to get similar temperature biases in upper troposphere **
    itune_albedo: 1     # somewhat reduced albedo (w.r.t. MODIS data) over Sahara in order to reduce cold bias

  turbdiff_nml:
    tkhmin: 0.75  # new default since rev. 16527
    tkmmin: 0.75  #           " 
    pat_len: 100.
    c_diff: 0.2
    ltkesso: True
    frcsmot: 0.2      # these 2 switches together apply vertical smoothing of the TKE source terms
    imode_frcsmot: 2  # in the tropics (only), which reduces the moist bias in the tropical lower troposphere
 # use horizontal shear production terms with 1/SQRT(Ri) scaling to prevent unwanted side effects:
    itype_sher: 3    
    ltkeshs: True
    a_hshr: 2.0

  lnd_nml:
    ntiles: 3      ### 1 for assimilation cycle and forecast
    nlev_snow: 3      ### 1 for assimilation cycle and forecast
    lmulti_snow: True ### False for assimilation cycle and forecast
    itype_heatcond: 2
    idiag_snowfrac: 2
    lsnowtile: False  ## later on True if GRIB encoding issues are solved
    lseaice: True
    llake: False
    itype_lndtbl: 3  # minimizes moist/cold bias in lower tropical troposphere
    itype_root: 2

  radiation_nml:
    irad_o3: 10
    irad_aero: 6
    ecrad_data_path:  'ecrad_data'
    albedo_type: 2 # Modis albedo
    vmr_co2: 390.e-06 # values representative for 2012
    vmr_ch4: 1800.e-09
    vmr_n2o: 322.0e-09
    vmr_o2: 0.20946
    vmr_cfc11: 240.e-12
    vmr_cfc12: 532.e-12

  nonhydrostatic_nml:
    iadv_rhotheta: 2
    ivctype: 2
    itime_scheme: 4
    exner_expol: 0.333
    vwind_offctr: 0.2
    damp_height: 50000.
    rayleigh_coeff: 0.10
    divdamp_order: 24    # for data assimilation runs, '2' provides extra-strong filtering of gravity waves 
    divdamp_type: 32    ### optional: 2 for assimilation cycle if very strong gravity-wave filtering is desired
    divdamp_fac: 0.004
    igradp_method: 3
    l_zdiffu_t: True
    thslp_zdiffu: 0.02
    thhgtd_zdiffu: 125.
    htop_moist_proc: 22500.
    hbot_qvsubstep: 22500. # use 19000. with R3B7

  sleve_nml:
    min_lay_thckn: 20.
    max_lay_thckn: 400.   # maximum layer thickness below htop_thcknlimit
    htop_thcknlimit: 14000. # this implies that the upcoming COSMO-EU nest will have 60 levels
    top_height: 75000.
    stretch_fac: 0.9
    decay_scale_1: 4000.
    decay_scale_2: 2500.
    decay_exp: 1.2
    flat_height: 16000.

  dynamics_nml:
    iequations: 3
    divavg_cntrwgt: 0.50
    lcoriolis: True

  transport_nml:
#                qv, qc, qi, qr, qs
    itype_vlimit: [1,1,1,1,1]
    ivadv_tracer: [3, 3, 3, 3, 3]
    itype_hlimit: [3, 4, 4, 4 , 4]
    ihadv_tracer: [52, 2,2,2,2]
    iadv_tke: 0

  diffusion_nml:
    hdiff_order: 5
    itype_vn_diffu: 1
    itype_t_diffu: 2
    hdiff_efdt_ratio: 24.0
    hdiff_smag_fac: 0.025
    lhdiff_vn: True
    lhdiff_temp: True

  interpol_nml:
    nudge_zone_width: 8
    lsq_high_ord: 3
    l_intp_c2l: True
    l_mono_c2l: True
    support_baryctr_intp: False

  extpar_nml:
    itopo: 1
    n_iter_smooth_topo: 1
    heightdiff_threshold: 3000.

  io_nml:
    itype_pres_msl: 4  # IFS method with bug fix for self-consistency between SLP and geopotential
    itype_rh: 1  # RH w.r.t. water

  output_nml:
    output_bounds:  [0., 864000., 3600.] # start, end, increment
    include_last:  True
    output_filename: 'icon-art-%EXPNAME%-chem'                # file name base
    ml_varlist: ['temp','pres','group:ART_CHEMISTRY', 'qv']
    output_grid:  True
    remap: 1
    reg_lon_def: [-180.,0.5,180.]
    reg_lat_def: [-90.,0.5,90.]

  output_nml-diag:
    output_bounds:  [0., 864000., 3600.] # start, end, increment
    include_last:  True
    output_filename: 'icon-art-%EXPNAME%-diag'                # file name base
    ml_varlist: ['temp','pres','group:ART_DIAGNOSTICS', 'qv']
    output_grid:  True
    remap: 1
    reg_lon_def: [-180.,0.5,180.]
    reg_lat_def: [-90.,0.5,90.]

  art_nml:
    lart_psc: True
    lart_diag_out: True
    lart_aerosol: False
    lart_chem: True
    lart_chemtracer: True
    cart_chemtracer_xml: 'tracers_chemtracer.xml'
    cart_input_folder: '.'
    iart_init_gas: 0

