# Copyright 2004-2024 DWD, MPI-M, DKRZ, KIT, ETH, MeteoSwiss
# Copyright 2023-2024 Karlsruhe Institute of Technology (KIT) and Ludwig-Maximilians-Universität München (LMU)
# SPDX-FileContributor: Andreas Baer
#
# SPDX-License-Identifier: BSD-3-Clause

NAMELIST:
# parallel_nml: MPI parallelization -------------------------------------------
  parallel_nml:
    nproma:                          8         # loop chunk length
    p_test_run:                     False        # True means verification run for MPI parallelization
    num_io_procs:                          2         # number of I/O processors
    num_restart_procs:                          0         # number of restart processors
    num_prefetch_proc:                          1         # number of processors for LBC prefetching
    iorder_sendrecv:                          3         # sequence of MPI send/receive calls



# run_nml: general switches ---------------------------------------------------
  run_nml:
    ltestcase:                     False        # real case run
    num_lev:                         60         # number of full levels (atm.) for each domain
    lvert_nest:                     False        # no vertical nesting
    ldynamics:                      True        # compute adiabatic dynamic tendencies
    ltransport:                      True        # compute large-scale tracer transport
    ntracer:                          0         # number of advected tracers
    iforcing:                          3         # forcing of dynamics and transport by parameterized processes
    msg_level:                          7         # detailed report during integration
    ltimer:                      True        # timer for monitoring the runtime of specific routines
    timers_level:                         10         # performance timer granularity
    check_uuid_gracefully:                      True        # give only warnings for non-matching uuids
    output:                        "nml"       # main switch for enabling/disabling components of the model output
    lart:                      True        # main switch for ART
    debug_check_level:                         10


# art_nml: Aerosols and Reactive Trace gases extension-------------------------------------------------
  art_nml:
    lart_chem:                        True       # enables chemistry
    lart_pntSrc:                        False      # enables point sources
    lart_aerosol:                        False      # main switch for the treatment of atmospheric aerosol
    lart_chemtracer:                        True       # main switch for the treatment of chemical tracer
    lart_diag_out:                        True       # If this switch is set to True, diagnostic
                                                                  # ... output elds are available. Set it to
                                                                  # ... False when facing memory problems.
    iart_init_gas:                            4
    cart_cheminit_file: 'cams_gqpe_20180101_00.nc'
    cart_cheminit_type: 'EMAC'
    cart_cheminit_coord: 'cams_gqpe_20180101_00.nc'
    iart_seasalt:                            0        # enable seasalt
    cart_chemtracer_xml:             'tracers_OEM.xml'       # path to xml file for passive tracers
    cart_input_folder:                   '.'       # absolute Path to ART source code


# oem_nml: online emission module ---------------------------------------------
  oemctrl_nml:
    gridded_emissions_nc:   'OEM/tno.nc'
    vertical_profile_nc:   'OEM/vertical_profiles.nc'
    hour_of_day_nc:   'OEM/hourofday.nc'
    day_of_week_nc:   'OEM/dayofweek.nc'
    month_of_year_nc:   'OEM/monthofyear.nc'
    hour_of_year_nc:   'OEM/hourofyear.nc'



# diffusion_nml: horizontal (numerical) diffusion ----------------------------
  diffusion_nml:
    lhdiff_vn:                      True        # diffusion on the horizontal wind field
    lhdiff_temp:                      True        # diffusion on the temperature field
    lhdiff_w:                      True        # diffusion on the vertical wind field
    hdiff_order:                          5         # order of nabla operator for diffusion
    itype_vn_diffu:                          1         # reconstruction method used for Smagorinsky diffusion
    itype_t_diffu:                          2         # discretization of temperature diffusion
    hdiff_efdt_ratio:                         24.0       # ratio of e-folding time to time step 
    hdiff_smag_fac:                          0.025     # scaling factor for Smagorinsky diffusion


# dynamics_nml: dynamical core -----------------------------------------------
  dynamics_nml:
    iequations:                          3         # type of equations and prognostic variables
    divavg_cntrwgt:                          0.50      # weight of central cell for divergence averaging
    lcoriolis:                      True        # Coriolis force


# extpar_nml: external data --------------------------------------------------
  extpar_nml:
    itopo:                          1         # topography (0:analytical)
 # default extpar filename is used: extpar_<gridname>.nc
    #extpar_filename:      'testcase_DOM01.extpar.nc'    # filename of external parameter input file
    n_iter_smooth_topo:                        [1,1]         # iterations of topography smoother
    hgtdiff_max_smooth_topo:                   [750.,750.]        # see Namelist doc
    heightdiff_threshold:                 [2250.,1500.,3000.]


# initicon_nml: specify read-in of initial state ------------------------------
  initicon_nml:
    init_mode:                          2         # 7: start from DWD fg with subsequent vertical remapping 
    lread_ana:                    False         # no analysis data will be read
    # This differs from the default and has to be provided explicitly to work with non-standard grid names.
    ifs2icon_filename: "ifs_init.nc"        # initial data filename
    ana_varnames_map_file:              "map_file.ana"        # dictionary mapping internal names onto GRIB2 shortNames
    ltile_coldstart:                      True        # coldstart for surface tiles
    ltile_init:                     False        # set it to True if FG data originate from run without tiles


# grid_nml: horizontal grid --------------------------------------------------
  grid_nml:
    dynamics_parent_grid_id:                          0         # array of the indexes of the parent grid filenames
    lredgrid_phys:                      True        # True=radiation is calculated on a reduced grid
    lfeedback:                      True        # specifies if feedback to parent grid is performed
    l_limited_area:                      True        # True performs limited area run
    ifeedback_type:                          2         # feedback type (incremental/relaxation-based)
    start_time:                          0.        # Time when a nested domain starts to be active [s]


# gridref_nml: grid refinement settings --------------------------------------
  gridref_nml:
    denom_diffu_v:                        150.        # denominator for lateral boundary diffusion of velocity


# interpol_nml: settings for internal interpolation methods ------------------
  interpol_nml:
    nudge_zone_width:                          8         # width of lateral boundary nudging zone
    support_baryctr_intp:                     False        # barycentric interpolation support for output
    nudge_max_coeff:                       0.07
    nudge_efold_width:                        2.0


# io_nml: general switches for model I/O -------------------------------------
  io_nml:
    itype_pres_msl:                          5         # method for computation of mean sea level pressure
    itype_rh:                          1         # method for computation of relative humidity
    lmask_boundary:                      True        # mask out interpolation zone in output


# limarea_nml: settings for limited area mode ---------------------------------
  limarea_nml:
    itype_latbc:                          1         # 1: time-dependent lateral boundary conditions
    dtime_latbc:                      10800         # time difference between 2 consecutive boundary data
    nlev_latbc:                         90         # Number of vertical levels in boundary data
    latbc_boundary_grid:   "lateral_boundary.grid.nc"       # Grid file defining the lateral boundary
    latbc_path:                  'icbc'       # Absolute path to boundary data
    latbc_varnames_map_file:            'map_file.latbc'
    latbc_filename:   "ifs_201801<d><h>_lbc.nc"        # boundary data input filename
    init_latbc_from_fg:                     False        # True: take lbc for initial time from first guess


# lnd_nml: land scheme switches -----------------------------------------------
  lnd_nml:
    ntiles:                          1         # number of tiles
    nlev_snow:                          3         # number of snow layers
    lmulti_snow:                      False       # True for use of multi-layer snow model
    idiag_snowfrac:                         20         # type of snow-fraction diagnosis
    lsnowtile:                       True       # True=consider snow-covered and snow-free separately
    itype_root:                          2         # root density distribution
    itype_heatcond:                          3         # type of soil heat conductivity
    itype_lndtbl:                          4         # table for associating surface parameters
    itype_evsl:                          4         # type of bare soil evaporation
    cwimax_ml:                      5.e-4         # scaling parameter for max. interception storage
    c_soil:                       1.75         # surface area density of the evaporative soil surface
    c_soil_urb:                        0.5         # same for urban areas
    lseaice:                      True        # True for use of sea-ice model
    llake:                      True        # True for use of lake model


# nonhydrostatic_nml: nonhydrostatic model -----------------------------------
  nonhydrostatic_nml:
    iadv_rhotheta:                          2         # advection method for rho and rhotheta
    ivctype:                          2         # type of vertical coordinate
    itime_scheme:                          4         # time integration scheme
    ndyn_substeps:                          5         # number of dynamics steps per fast-physics step
    exner_expol:                          0.333     # temporal extrapolation of Exner function
    vwind_offctr:                          0.2       # off-centering in vertical wind solver
    damp_height:                      12500.0       # height at which Rayleigh damping of vertical wind starts
    rayleigh_coeff:                          1.5       # Rayleigh damping coefficient
    divdamp_order:                         24         # order of divergence damping 
    divdamp_type:                         3          # type of divergence damping
    divdamp_fac:                          0.004     # scaling factor for divergence damping
    igradp_method:                          3         # discretization of horizontal pressure gradient
    l_zdiffu_t:                      True        # specifies computation of Smagorinsky temperature diffusion
    thslp_zdiffu:                          0.02      # slope threshold (temperature diffusion)
    thhgtd_zdiffu:                        125.0       # threshold of height difference (temperature diffusion)
    htop_moist_proc:                      22500.0       # max. height for moist physics
    hbot_qvsubstep:                      22500.0       # height above which QV is advected with substepping scheme


# nwp_phy_nml: switches for the physics schemes ------------------------------
  nwp_phy_nml:
    inwp_gscp:                          2         # cloud microphysics and precipitation
    inwp_convection:                          1         # convection
    lshallowconv_only:                      False        # only shallow convection
    inwp_radiation:                          1         # radiation
    inwp_cldcover:                          1         # cloud cover scheme for radiation
    inwp_turb:                          1         # vertical diffusion and transfer
    inwp_satad:                          1         # saturation adjustment
    inwp_sso:                          1         # subgrid scale orographic drag
    inwp_gwd:                          0         # non-orographic gravity wave drag
    inwp_surface:                          1         # surface scheme
    latm_above_top:                      True        # take into account atmosphere above model top for radiation computation
    ldetrain_conv_prec:                      True
    efdt_min_raylfric:                       7200.        # minimum e-folding time of Rayleigh friction
    itype_z0:                          2         # type of roughness length data
    icapdcycl:                          3         # apply CAPE modification to improve diurnalcycle over tropical land
    icpl_aero_conv:                          1         # coupling between autoconversion and Tegen aerosol climatology
    icpl_aero_gscp:                          1         # coupling between autoconversion and Tegen aerosol climatology
    lrtm_filename:                'rrtmg_lw.nc'       # longwave absorption coefficients for RRTM_LW
    cldopt_filename:             'rrtm_cldopt.nc'       # RRTM cloud optical properties
    dt_rad:                         720.       # time step for radiation in s
    dt_conv:                 [120.,90.,90.]       # time step for convection in s (domain specific)
    dt_sso:               [120.,360.,360.]       # time step for SSO parameterization
    dt_gwd:               [360.,360.,360.]       # time step for gravity wave drag parameterization


# nwp_tuning_nml: additional tuning parameters ----------------------------------
  nwp_tuning_nml:
    itune_albedo:                          1         # reduced albedo (w.r.t. MODIS data) over Sahara
    tune_gkwake:                        1.8
    tune_gkdrag:                        0.01
    tune_minsnowfrac:                        0.3


# output_nml: specifies an output stream --------------------------------------
  output_nml:
    filetype:                          4         # output format: 2=GRIB2, 4=NETCDFv2
    dom:                          1         # write domain 1 only
    output_bounds:        [0., 10000000., 10800.]        # start, end, increment
    steps_per_file:                          1         # number of steps per file
    mode:                          1         # 1: forecast mode (relative t-axis), 2: climate mode (absolute t-axis)
    include_last:                      True
    output_filename:                    'ICON-ART-OEM'
    filename_format: '<output_filename>_<datetime>' # file name base
    steps_per_file_inclfirst:                    False
    output_grid:                      True
    remap:                          1         # 1: remap to lat-lon grid
    #north_pole:                   [-170.,40.]       # definition of north_pole for rotated lat-lon grid
    reg_lon_def:              [-16.0,0.1,36.0]       #
    reg_lat_def:               [32.0,0.1,74.0]       #
    ml_varlist:  ['group:ATMO_ML_VARS',
                  'group:nh_prog_vars',
                  'z_mc', 'z_ifc',
                  'group:ART_CHEMISTRY']

  output_nml-latbc:
    filetype:                          4         # output format: 2=GRIB2, 4=NETCDFv2
    dom:                          1         # write domain 1 only
    output_bounds:        [0., 10000000., 3600.]        # start, end, increment
    steps_per_file:                          1         # number of steps per file
    mode:                          1         # 1: forecast mode (relative t-axis), 2: climate mode (absolute t-axis)
    include_last:                      True
    output_filename:                    'LATBC-ICON-ART-OEM'
    filename_format: '<output_filename>_<datetime>' # file name base
    steps_per_file_inclfirst:                    False
    output_grid:                      True
    remap:                          1         # 1: remap to lat-lon grid
    #north_pole:                   -170.,40.       # definition of north_pole for rotated lat-lon grid
    reg_lon_def:              [-16.0,0.1,36.0]       #
    reg_lat_def:               [32.0,0.1,74.0]       #
    ml_varlist:  ['group:LATBC_PREFETCH_VARS',
                  'group:icon_lbc_vars']


# radiation_nml: radiation scheme ---------------------------------------------
  radiation_nml:
    irad_o3:                          7         # ozone climatology
    irad_aero:                          6         # aerosols
    albedo_type:                          2         # type of surface albedo
    vmr_co2:                    390.e-06
    vmr_ch4:                   1800.e-09
    vmr_n2o:                   322.0e-09
    vmr_o2:                     0.20946
    vmr_cfc11:                    240.e-12
    vmr_cfc12:                    532.e-12


# sleve_nml: vertical level specification -------------------------------------
  sleve_nml:
    min_lay_thckn:                         20.0       # layer thickness of lowermost layer
    top_height:                      23000.0       # height of model top
    stretch_fac:                          0.65      # stretching factor to vary distribution of model levels
    decay_scale_1:                       4000.0       # decay scale of large-scale topography component
    decay_scale_2:                       2500.0       # decay scale of small-scale topography component
    decay_exp:                          1.2       # exponent of decay function
    flat_height:                      16000.0       # height above which the coordinate surfaces are flat


# transport_nml: tracer transport ---------------------------------------------
  transport_nml:
    npassive_tracer:                          0         # number of additional passive tracers
    ivadv_tracer:             [ 3, 3, 3, 3, 3, 3 ]        # tracer specific method to compute vertical advection
    itype_hlimit:             [ 3, 4, 4, 4, 4, 4 ]        # type of limiter for horizontal transport
    ihadv_tracer:             [52, 2, 2, 2, 2, 22]        # tracer specific method to compute horizontal advection
    llsq_svd:                      True        # use SV decomposition for least squares design matrix


# turbdiff_nml: turbulent diffusion -------------------------------------------
  turbdiff_nml:
    tkhmin:                          0.75      # scaling factor for minimum vertical diffusion coefficient
    tkmmin:                          0.75      # scaling factor for minimum vertical diffusion coefficient
    pat_len:                        750.0       # effective length scale of thermal surface patterns
    c_diff:                          0.2       # length scale factor for vertical diffusion of TKE
    # rat_sea:                          7.5       # controls laminar resistance for sea surface
    rat_sea: 8.5  # ** new since r20191: 8.5 for R3B7, 8.0 for R2B6 in order to get similar temperature biases in the tropics **
    rlam_heat: 1.0   # check before production runs
    ltkesso:                        True      # consider TKE-production by sub-grid SSO wakes
    frcsmot:                          0.2       # these 2 switches together apply vertical smoothing of the TKE source terms
    imode_frcsmot:                            2       # in the tropics (only), which reduces the moist bias in the tropical lower troposphere
    itype_sher:                            3       # type of shear forcing used in turbulence
    ltkeshs:                        True      # include correction term for coarse grids in hor. shear production term
    a_hshr:                          2.0       # length scale factor for separated horizontal shear mode
    icldm_turb:                            1       # mode of cloud water representation in turbulence
    ldiff_qi:                        True

