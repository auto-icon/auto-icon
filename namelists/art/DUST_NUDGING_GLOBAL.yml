# Copyright 2023 - 2024 Karlsruhe Institute of Technology (KIT) and Ludwig-Maximilians-Universität München (LMU)
# SPDX-FileContributor: Andreas Baer
#
# SPDX-License-Identifier: BSD-3-Clause

#-- > Atmo namelist for ERA5_DUST_REINIT template. < --#

NAMELIST:
  parallel_nml:
    nproma: 8  # optimal setting 8 for CRAY; use 16 or 24 for IBM
    num_io_procs: 12   # up to one PE per output stream is possible
    num_restart_procs: 4
    iorder_sendrecv: 3  # best value for CRAY (slightly faster than option 1)

  initicon_nml:
    lconsistency_checks: False
  
  run_nml:
    num_lev: 90
    ltransport: True
    iforcing: 3            # NWP forcing
    ltestcase: False      # false: run with real data
    msg_level: 7            # print maximum wind speeds every 5 time steps
    timers_level: 10            # can be increased up to 10 for detailed timer output
    output: "nml"
    lart: True

  nwp_phy_nml:
    inwp_radiation: 4
    icapdcycl: 3 # apply CAPE modification to improve diurnalcycle over tropical land (optimizes NWP scores)
    efdt_min_raylfric: 7200.

  nwp_tuning_nml:
    tune_zceff_min: 0.075 # ** default value to be used for R3B7; use 0.05 for R2B6 in order to get similar temperature biases in upper troposphere **
    itune_albedo: 1     # somewhat reduced albedo (w.r.t. MODIS data) over Sahara in order to reduce cold bias

  turbdiff_nml:
    rat_sea: 0.85  # ** new since r20191: 8.5 for R3B7, 8.0 for R2B6 in order to get similar temperature biases in the tropics **
    frcsmot: 0.2      # these 2 switches together apply vertical smoothing of the TKE source terms
    imode_frcsmot: 2  # in the tropics (only), which reduces the moist bias in the tropical lower troposphere
 # use horizontal shear production terms with 1/SQRT(Ri) scaling to prevent unwanted side effects:
    itype_sher: 3    
    ltkeshs: True
    a_hshr: 2.0

  lnd_nml:
    ntiles: 3      ### 1 for assimilation cycle and forecast
    nlev_snow: 3      ### 1 for assimilation cycle and forecast
    lmulti_snow: True ### False for assimilation cycle and forecast
    idiag_snowfrac: 2
    llake: False

  radiation_nml:
    irad_o3: 7
    irad_aero: 9
    albedo_type: 2 # Modis albedo
    vmr_co2: 390.e-06 # values representative for 2012
    vmr_ch4: 1800.e-09
    vmr_n2o: 322.0e-09
    vmr_o2: 0.20946    # this value is at the default but kept such that all vmr_* values are given here.
    vmr_cfc11: 240.e-12
    vmr_cfc12: 532.e-12
    ecrad_data_path: 'ecrad_data'

  nonhydrostatic_nml:
    exner_expol: 0.333
    vwind_offctr: 0.2
    damp_height: 50000.
    rayleigh_coeff: 0.10
    divdamp_order: 24    # for data assimilation runs, '2' provides extra-strong filtering of gravity waves 
    divdamp_type: 32    ### optional: 2 for assimilation cycle if very strong gravity-wave filtering is desired
    divdamp_fac: 0.004
    thslp_zdiffu: 0.02
    thhgtd_zdiffu: 125.

  sleve_nml:
    min_lay_thckn: 20.
    max_lay_thckn: 400.   # maximum layer thickness below htop_thcknlimit
    htop_thcknlimit: 14000. # this implies that the upcoming COSMO-EU nest will have 60 levels
    top_height: 75000.
    stretch_fac: 0.9

  transport_nml:
#                qv, qc, qi, qr, qs
    itype_vlimit: [1, 1, 1, 1, 1]
    ivadv_tracer: [3, 3, 3, 3, 3]
    itype_hlimit: [3, 4, 4, 4, 4]
    ihadv_tracer: [52,2, 2, 2, 2]
    iadv_tke: 0

  diffusion_nml:
    hdiff_efdt_ratio: 24.0
    hdiff_smag_fac: 0.025

  extpar_nml:
    itopo: 1
    n_iter_smooth_topo: 1

  io_nml:
    itype_pres_msl: 4  # IFS method with bug fix for self-consistency between SLP and geopotential
    restart_write_mode: "dedicated procs multifile"

  output_nml-1:
    include_last:  True
    output_filename: 'icon-art-%EXPNAME%-aero'                # file name base
    ml_varlist: ['temp','z_ifc','rho','pres','u','v','w','group:ART_AEROSOL']

  art_nml:
    lart_diag_out: True
    lart_aerosol: True
    iart_ari: 1
    iart_init_aero: 0
    iart_seasalt: 0
    iart_dust: 1
    iart_nonsph: 1
    cart_input_folder: '.'
    cart_aerosol_xml: 'xml/tracers_dustrad.xml'
    cart_modes_xml: 'xml/modes_dustrad.xml'
    cart_coag_xml: 'xml/coagulate_dustrad.xml'
    cart_aero_emiss_xml: 'xml/aero_emiss_dustrad.xml'
    cart_diagnostics_xml: 'xml/diagnostics_dust.xml'
