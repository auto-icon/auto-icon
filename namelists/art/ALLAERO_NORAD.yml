# Copyright 2004-2024 DWD, MPI-M, DKRZ, KIT, ETH, MeteoSwiss
# Copyright 2023-2024 Karlsruhe Institute of Technology (KIT) and Ludwig-Maximilians-Universität München (LMU)
# SPDX-FileContributor: Andreas Baer
#
# SPDX-License-Identifier: BSD-3-Clause

NAMELIST:
  parallel_nml:
    nproma: 8  # optimal setting 8 for CRAY; use 16 or 24 for IBM
    p_test_run: False
    l_test_openmp: False
    l_log_checks: False
    num_io_procs: 1   # up to one PE per output stream is possible
    iorder_sendrecv: 3  # best value for CRAY (slightly faster than option 1)

  grid_nml:
    lfeedback: False
    ifeedback_type: 2                   #
    lredgrid_phys: False
    start_time: 0.

  initicon_nml:
    init_mode: 5               # operation mode 2: IFS
    dwdfg_filename: "dwdFG_R2B06_DOM01.grb"  # initial data filename
    dwdana_filename: "dwdana_R2B06_DOM01.grb"
    ana_varnames_map_file: "map_file.ana"    # dictionary mapping internal names onto GRIB2 shortNames
    lread_ana: True         # no analysis data will be read
    ltile_coldstart: False          # coldstart for surface tiles
    ltile_init: False         # set it to True if FG data originate from run without tiles
    dt_iau: 10800.           # time interval (in s) during which the IAU procedure is performed
    dt_shift: -5400.           # time (in s) by which the model start is shifted ahead, given by ini_datetime_string.
    iterate_iau: True
    zpbl1: 500.
    zpbl2: 1000.
    lp2cintp_incr: True
    lp2cintp_sfcana: True
    check_ana(1)%list: ['T_SO','W_SO','P','QV','T','U','V','FRESHSNW','H_SNOW']
    check_ana(2)%list: ['T_SO','W_SO','FRESHSNW','H_SNOW']

  run_nml:
    num_lev: 90
    lvert_nest: True       # use vertical nesting if a nest is active
    ldynamics: True       # dynamics
    ltransport: True
    iforcing: 3            # NWP forcing
    ltestcase: False      # false: run with real data
    msg_level: 7            # print maximum wind speeds every 5 time steps
    ltimer: True       # set True for timer output
    timers_level: 1            # can be increased up to 10 for detailed timer output
    output: "nml"
    lart: True
    check_uuid_gracefully: True

  nwp_phy_nml:
    inwp_gscp: 1
    inwp_convection: 1
    inwp_radiation: 4
    inwp_cldcover: 1
    inwp_turb: 1
    inwp_satad: 1
    inwp_sso: 1
    inwp_gwd: 1
    inwp_surface: 1
    icapdcycl: 3     # apply CAPE modification to improve diurnalcycle over tropical land (optimizes NWP scores)
    latm_above_top: False  # the second entry refers to the nested domain (if present)
    efdt_min_raylfric: 7200.
    itype_z0: 2
    icpl_aero_conv: 0
    icpl_aero_gscp: 0
 # resolution-dependent settings - please choose the appropriate one
    dt_rad: 1440.
    dt_conv: 360.
    dt_sso: 720.
    dt_gwd: 720.

  nwp_tuning_nml:
    tune_zceff_min: 0.05 # ** default value to be used for R3B7; use 0.05 for R2B6 in order to get similar temperature biases in upper troposphere **
    itune_albedo: 1     # somewhat reduced albedo (w.r.t. MODIS data) over Sahara in order to reduce cold bias

  turbdiff_nml:
    tkhmin: 0.75  # new default since rev. 16527
    tkmmin: 0.75  #            
    pat_len: 100.
    c_diff: 0.2
    rlam_heat: 10.
    rat_sea: 0.875 # ** new since r20191: 8.5 for R3B7, 8.0 for R2B6 in order to get similar temperature biases in the tropics **
    ltkesso: True
    frcsmot: 0.2      # these 2 switches together apply vertical smoothing of the TKE source terms
    imode_frcsmot: 2  # in the tropics (only), which reduces the moist bias in the tropical lower troposphere
 # use horizontal shear production terms with 1/SQRT(Ri) scaling to prevent unwanted side effects
    itype_sher: 3    
    ltkeshs: True
    a_hshr: 2.0

  lnd_nml:
    ntiles: 3      ### 1 for assimilation cycle and forecast
    nlev_snow: 1      ### 1 for assimilation cycle and forecast
    lmulti_snow: False ### False for assimilation cycle and forecast
    itype_heatcond: 2
    idiag_snowfrac: 20
    lsnowtile: True  ## later on True if GRIB encoding issues are solved
    lseaice: True
    llake: True
    frlake_thrhld: 0.05
    itype_lndtbl: 3  # minimizes moist/cold bias in lower tropical troposphere
    itype_root: 2

  radiation_nml:
    irad_o3: 7
    irad_aero: 6
    albedo_type: 2 # Modis albedo
    vmr_co2: 390.e-06 # values representative for 2012
    vmr_ch4: 1800.e-09
    vmr_n2o: 322.0e-09
    vmr_o2: 0.20946
    vmr_cfc11: 240.e-12
    vmr_cfc12: 532.e-12
    ecrad_data_path: "ecrad_data"

  nonhydrostatic_nml:
    iadv_rhotheta: 2
    ivctype: 2
    itime_scheme: 4
    exner_expol: 0.333
    vwind_offctr: 0.2
    damp_height: 50000.
    rayleigh_coeff: 0.10
    divdamp_order: 24    # for data assimilation runs, '2' provides extra-strong filtering of gravity waves 
    divdamp_type: 32    ### optional: 2 for assimilation cycle if very strong gravity-wave filtering is desired
    divdamp_fac: 0.004
    igradp_method: 3
    l_zdiffu_t: True
    thslp_zdiffu: 0.02
    thhgtd_zdiffu: 125.
    htop_moist_proc: 22500.
    hbot_qvsubstep: 22500. # use 19000. with R3B7

  sleve_nml:
    min_lay_thckn: 20.
    max_lay_thckn: 400.   # maximum layer thickness below htop_thcknlimit
    htop_thcknlimit: 14000. # this implies that the upcoming COSMO-EU nest will have 60 levels
    top_height: 75000.
    stretch_fac: 0.9
    decay_scale_1: 4000.
    decay_scale_2: 2500.
    decay_exp: 1.2
    flat_height: 16000.

  dynamics_nml:
    iequations: 3
    divavg_cntrwgt: 0.50
    lcoriolis: True

  transport_nml:
    ctracer_list: '12345'
    ivadv_tracer: [3,3,3,3,3,3,3,3,3,3,3]
    itype_hlimit: [3,4,4,4,4,3,3,3,3,3,3]
    ihadv_tracer: [52,2,2,2,2,22,22,22,22,22,22]
    iadv_tke: 0

  diffusion_nml:
    hdiff_order: 5
    itype_vn_diffu: 1
    itype_t_diffu: 2
    hdiff_efdt_ratio: 24.0
    hdiff_smag_fac: 0.025
    lhdiff_vn: True
    lhdiff_temp: True

  interpol_nml:
    nudge_zone_width: 8
    lsq_high_ord: 3
    l_intp_c2l: True
    l_mono_c2l: True
    support_baryctr_intp: True

  gribout_nml:
    generatingCenter: 78
    generatingSubcenter: 255

  extpar_nml:
    itopo: 1
    n_iter_smooth_topo: 1
    heightdiff_threshold: 3000.

  io_nml:
    itype_pres_msl: 4  # IFS method with bug fix for self-consistency between SLP and geopotential
    itype_rh: 1  # RH w.r.t. water

  output_nml:
    mode:  1
    include_last:  True
    output_filename: 'icon-art-%EXPNAME%-aero-reg'            # file name base
    ml_varlist: ['z_ifc','rho','temp', 'group:ART_AEROSOL']
    remap:  1
    reg_lon_def: [-180.,0.5,179.5]
    reg_lat_def: [90.,-0.5, -90.]

  art_nml:
    lart_diag_out: True
    lart_aerosol: True
    iart_ari: 0
    iart_init_aero: 5
    iart_seasalt: 0
    iart_dust: 0
    iart_fire: 1
    iart_volcano: 0  
    iart_aero_washout: 1
    iart_nonsph: 0
    iart_isorropia: 0
    cart_io_suffix: 'TCNR'
    cart_aerosol_xml: 'xml/tracers_allaero_norad.xml'
    cart_input_folder: '.'
    cart_modes_xml: 'xml/modes_allaero_norad.xml'
    cart_aero_emiss_xml: 'xml/aero_emiss_allaero_norad.xml'
    cart_diagnostics_xml: 'xml/diagnostics_allaero_norad.xml'

