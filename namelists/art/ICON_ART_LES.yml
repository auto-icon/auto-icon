# Copyright 2023 - 2024 Karlsruhe Institute of Technology (KIT) and Ludwig-Maximilians-Universität München (LMU)
# SPDX-FileContributor: Andreas Baer
#
# SPDX-License-Identifier: BSD-3-Clause

#-- > Atmo namelist for ICON_ART_LES template. < --#

NAMELIST:
  parallel_nml:
    nproma: 8

  grid_nml:
    is_plane_torus: True

  run_nml:
    num_lev: 90
    ltransport: True
    ntracer:  1
    iforcing: 3
    timers_level: 10
    lart: True
    msg_level: 7

  dynamics_nml:
    lcoriolis: False

  nh_testcase_nml:
    nh_test_name: 'CBL'
    u_cbl: [0.45, 0.001]
    v_cbl: [0.0, 0.001]
    th_cbl: [290.0, 0.006]

  les_nml:
    is_dry_cbl: True 
    shflx: 0.15
    isrfc_type: 2
    expname: '%EXPNAME%'
    sampl_freq_sec: 30.
    ldiag_les_out: True
    les_metric: True

  diffusion_nml:
    hdiff_order: 4
    lsmag_3d: True

  nwp_phy_nml:
    inwp_convection: 0
    inwp_radiation: 0
    inwp_cldcover: 0
    inwp_turb: 5
    inwp_surface: 0
    dt_rad: 8.64
    dt_conv: 2.88
    dt_sso: 5.76
    dt_gwd: 5.76

  extpar_nml:
    itopo: 1
    n_iter_smooth_topo: 1
    itype_lwemiss: 0

  lnd_nml:
    lseaice: False
    llake: False
    lmelt: False
    itype_lndtbl: 1

  turbdiff_nml:
    lconst_z0: True

  nonhydrostatic_nml:
    igradp_method: 2
    rayleigh_coeff: 0.1
    divdamp_fac: 0.004
    vwind_offctr: 0.4
    damp_height: 1600

  sleve_nml:
    min_lay_thckn: 6.0
    top_height: 2000
    stretch_fac: 0.9

  io_nml:
    lkeep_in_sync: True

  output_nml:
    output_filename: '%EXPNAME%_insta'
    output_grid: True
    ml_varlist:  ['plcov','ustar_thres','ustar','gz0','u','v','w','temp','theta_v','pres_sfc','pres','tkvh','z_ifc','z_mc','rho','tcm','emiss_dusta','emiss_dustb','emiss_dustc','acc_emiss_dusta','acc_emiss_dustb','acc_emiss_dustc','diam_dusta','diam_dustb','diam_dustc','umfl_s', 'vmfl_s', 'u_10m', 'v_10m', 't_g', 't_2m', 'dusta', 'dustb', 'dustc']

  art_nml:
    lart_diag_out: True
    lart_chem: False
    lart_aerosol: True
    lart_pntSrc: False
    iart_dust: 1
    iart_ari: 0
    iart_init_aero: 0
    iart_seasalt: 0
    cart_io_suffix: 'TCNR'
    cart_input_folder: '.'
    cart_aerosol_xml: 'tracers_aerosol.xml'
    cart_modes_xml: 'modes.xml'
    cart_diagnostics_xml: 'diagnostics_aerosol.xml'

