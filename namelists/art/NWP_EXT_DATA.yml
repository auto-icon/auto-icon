# Copyright 2023 - 2024 Karlsruhe Institute of Technology (KIT) and Ludwig-Maximilians-Universität München (LMU)
# SPDX-FileContributor: Andreas Baer
#
# SPDX-License-Identifier: BSD-3-Clause

#-- > Atmo namelist for NWP_EXT_DATA template. < --#

NAMELIST:
  parallel_nml:
    nproma: 8
    num_io_procs: 1
    iorder_sendrecv: 3

  run_nml:
    num_lev: 90
    ltransport: True
    iforcing: 3
    ltestcase: False
    msg_level: 7
    timers_level: 10
    output: "nml"
    lart: True

  initicon_nml:
    init_mode: 2
    lread_ana: True
    ltile_coldstart: False                                                                              

  nwp_phy_nml:
    icapdcycl: 3
    efdt_min_raylfric: 7200.
    icpl_aero_conv: 1
    icpl_aero_gscp: 1

  nwp_tuning_nml:
    tune_zceff_min: 0.075
    itune_albedo: 1

  turbdiff_nml:
    rat_sea: 0.85
    frcsmot: 0.2
    imode_frcsmot: 2
    itype_sher: 3    
    ltkeshs: True
    a_hshr: 2.0

  lnd_nml:
    ntiles: 3
    nlev_snow: 3
    lmulti_snow: True
    idiag_snowfrac: 2
    llake: False

  radiation_nml:
    irad_o3: 7
    irad_aero: 6
    albedo_type: 2
    vmr_co2: 390.e-06
    vmr_ch4: 1800.e-09
    vmr_n2o: 322.0e-09
    vmr_o2: 0.20946
    vmr_cfc11: 240.e-12
    vmr_cfc12: 532.e-12

  nonhydrostatic_nml:
    exner_expol: 0.333
    vwind_offctr: 0.2
    damp_height: 50000.
    rayleigh_coeff: 0.10
    divdamp_order: 24
    divdamp_type: 32
    divdamp_fac: 0.004
    thslp_zdiffu: 0.02
    thhgtd_zdiffu: 125.

  sleve_nml:
    min_lay_thckn: 20.
    max_lay_thckn: 400.
    htop_thcknlimit: 14000.
    top_height: 75000.
    stretch_fac: 0.9

  transport_nml:
#--               qv,qc,qi,qr,qs
    itype_vlimit: [1, 1, 1, 1, 1]
    ivadv_tracer: [3, 3, 3, 3, 3]
    itype_hlimit: [3, 4, 4, 4, 4]
    ihadv_tracer: [52,2, 2, 2, 2]
    iadv_tke: 0

  diffusion_nml:
    hdiff_efdt_ratio: 24.0
    hdiff_smag_fac: 0.025

  extpar_nml:
    itopo: 1
    n_iter_smooth_topo: 1

  io_nml:
    itype_pres_msl: 4

  output_nml:
    steps_per_file_inclfirst: False
    output_filename: 'icon-art-%EXPNAME%-chem'
    ml_varlist: ['temp','pres','u','v','group:ART_CHEMISTRY']

  output_nml-remap:
    steps_per_file_inclfirst: False
    output_filename: 'icon-art-%EXPNAME%-remap-chem'
    ml_varlist: ['temp','pres','u','v','group:ART_CHEMISTRY']
    output_grid:  True
    remap: 1
    reg_lon_def: [-180.,0.5,180.]
    reg_lat_def: [-90.,0.5,90.]

  art_nml:
    lart_diag_out: True
    lart_aerosol: False
    lart_chem: True
    lart_chemtracer: True
    cart_emiss_xml_file: "emissions_R2B04_fortestsuite.xml"
    cart_chemtracer_xml: "tracers_chemtracer_ext_data.xml"
    cart_input_folder: "."
    iart_init_gas: 4
    cart_cheminit_coord: "emac_T42L47_coord.nc"
    cart_cheminit_file: "ART_EMAC_H2SO4_iconR2B04-grid-2004-01-01-08_0012.nc"
    cart_cheminit_type: "EMAC"
    cart_ext_data_xml: "ext_dataset_minimal.xml"

