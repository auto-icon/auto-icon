# Copyright 2023 - 2024 Karlsruhe Institute of Technology (KIT) and Ludwig-Maximilians-Universität München (LMU)
# SPDX-FileContributor: Andreas Baer
#
# SPDX-License-Identifier: BSD-3-Clause

#-- > YAC coupling parameters for the SmART template. < --#

definitions:
  atm2oce: &atm2oce
    src_component: atmo  #<-- TODO: flexibilize
    src_grid: icon_atmos_grid
    tgt_component: ocean  #<-- TODO: flexibilize
    tgt_grid: icon_ocean_grid
    time_reduction: average
    src_lag: 1  #<-- TODO: flexibilize
    tgt_lag: 1  #<-- TODO: flexibilize
  oce2atm: &oce2atm
    src_component: ocean  #<-- TODO: flexibilize
    src_grid: icon_ocean_grid
    tgt_component: atmo  #<-- TODO: flexibilize
    tgt_grid: icon_atmos_grid
    time_reduction: average
    src_lag: 1  #<-- TODO: flexibilize
    tgt_lag: 1  #<-- TODO: flexibilize
  interp_stacks:
    hcsbb_interp_stack: &hcsbb_interp_stack
      interpolation:
        - bernstein_bezier
        - nnn:
            n: 4
            weighted: arithmetic_average
        - fixed:
            user_value: -999.9
    conserv_interp_stack: &conserv_interp_stack
      interpolation:
        - conservative:
            order: 1
            enforced_conservation: false
            partial_coverage: true
            normalisation: fracarea
        - fixed:
            user_value: -999.9
    spmap_interp_stack: &spmap_interp_stack
      interpolation:
        - source_to_target_map

timestep_unit: ISO_format
calendar: proleptic-gregorian
coupling:
  - <<: [ *atm2oce, *hcsbb_interp_stack ]
    coupling_period: "%EXPERIMENT.TIMESTEP_COUPLING%"  #<-- TODO: flexibilize
    field: [surface_downward_eastward_stress,
            surface_downward_northward_stress]
#  - <<: [ *atm2oce, *hcsbb_interp_stack ]
#    coupling_period: "%EXPERIMENT.TIMESTEP_COUPLING%"
#    field: [10m_wind_speed]
  - <<: [ *atm2oce, *conserv_interp_stack ]
    coupling_period: "%EXPERIMENT.TIMESTEP_COUPLING%"  #<-- TODO: flexibilize
    field: [surface_fresh_water_flux,
            total_heat_flux,
            atmosphere_sea_ice_bundle]
  - <<: [ *oce2atm, *conserv_interp_stack ]
    coupling_period: "%EXPERIMENT.TIMESTEP_COUPLING%"  #<-- TODO: flexibilize
    field: [sea_surface_temperature,
            ocean_sea_ice_bundle]
#  - <<: [ *oce2atm, *conserv_interp_stack ]
#    coupling_period: "%EXPERIMENT.TIMESTEP_COUPLING%"
#    field: [eastward_sea_water_velocity,
#            northward_sea_water_velocity]
  - <<: [ *atm2oce, *spmap_interp_stack ]
    coupling_period: "%EXPERIMENT.TIMESTEP_COUPLING%"  #<-- TODO: flexibilize
    field: river_runoff