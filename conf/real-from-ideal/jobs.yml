# Copyright 2023 - 2024 Karlsruhe Institute of Technology (KIT) and Ludwig-Maximilians-Universität München (LMU)
# SPDX-FileContributor: Oriol Tinto Prims, Fabian Jakub
#
# SPDX-License-Identifier: BSD-3-Clause

JOBS:
  TRANSFER_PROJECT:
    FILE: templates/common/transfer_project.sh
    PLATFORM: LOCAL

  BUILD_ICON:
    FILE: templates/common/build_icon-spack.sh
    DEPENDENCIES: TRANSFER_PROJECT
    WALLCLOCK: 04:00
    TASKS: 16
    RETRIALS: 2 # retry because spack downloads sometimes timeout
    NODES: 1

  BUILD_PYTHON_ENVIRONMENT:
    FILE: templates/common/build_python_environment.sh
    # Right now we rely on spack for building icon and having a python interpreter, so we need this dependency:
    DEPENDENCIES: BUILD_ICON
    WALLCLOCK: 01:00
    TASKS: 16
    NODES: 1

  PREPARE_EXPERIMENT:
    FILE: templates/real-from-ideal/prepare_experiment.sh
    DEPENDENCIES: BUILD_ICON
    RUNNING: once
    WALLCLOCK: 01:00

  PREPARE_IDEAL_DIRECTORY:
    FILE: templates/real-from-ideal/prepare_ideal_directory.sh
    DEPENDENCIES: PREPARE_EXPERIMENT
    RUNNING: date
    WALLCLOCK: 00:10

  PREPARE_IDEAL_NAMELIST:
    FILE: templates/real-from-ideal/prepare_ideal_namelist.py
    DEPENDENCIES: PREPARE_IDEAL_DIRECTORY BUILD_PYTHON_ENVIRONMENT TRANSFER_PROJECT
    RUNNING: date
    WALLCLOCK: 00:10
    TYPE: python
    EXECUTABLE: "%AISHAREDIR%/%PYTHON_ENVIRONMENT.FOLDER_NAME%/bin/python3"

  RUN_IDEAL:
    FILE: templates/real-from-ideal/run_ideal.sh
    DEPENDENCIES: PREPARE_IDEAL_NAMELIST
    RUNNING: date
    WALLCLOCK: 01:00

  EXTPAR_FROM_IDEALIZED:
    FILE: templates/real-from-ideal/extpar_from_idealized.py
    DEPENDENCIES: RUN_IDEAL
    RUNNING: date
    WALLCLOCK: 01:00
    TYPE: python
    EXECUTABLE: "%AISHAREDIR%/%PYTHON_ENVIRONMENT.FOLDER_NAME%/bin/python3"

  FG_ANA_FROM_IDEALIZED:
    FILE: templates/real-from-ideal/fg_ana_from_idealized.py
    DEPENDENCIES: RUN_IDEAL
    RUNNING: date
    WALLCLOCK: 01:00
    TYPE: python
    EXECUTABLE: "%AISHAREDIR%/%PYTHON_ENVIRONMENT.FOLDER_NAME%/bin/python3"

  PREPARE_MEMBER:
    FILE: templates/real-from-ideal/prepare_member.sh
    DEPENDENCIES: FG_ANA_FROM_IDEALIZED EXTPAR_FROM_IDEALIZED
    RUNNING: member
    WALLCLOCK: 01:00

  PREPARE_NAMELIST:
    FILE: templates/real-from-ideal/prepare_namelist.py
    DEPENDENCIES: TRANSFER_PROJECT BUILD_PYTHON_ENVIRONMENT PREPARE_MEMBER RUN_ICON-1
    WALLCLOCK: 00:05
    RUNNING: chunk
    TYPE: python
    EXECUTABLE: "%AISHAREDIR%/%PYTHON_ENVIRONMENT.FOLDER_NAME%/bin/python3"

  RUN_ICON:
    FILE: templates/common/run_icon-spack.sh
    DEPENDENCIES: PREPARE_NAMELIST
    WALLCLOCK: 04:00
    RUNNING: chunk
    TASKS: 64
    MEMORY: 81920
    CUSTOM_DIRECTIVES: [ "#SBATCH --exclusive" ]

  COMPRESS:
    FILE: templates/common/compress.py
    DEPENDENCIES: RUN_ICON BUILD_PYTHON_ENVIRONMENT COMPRESS-1
    RUNNING: chunk
    TYPE: python
    EXECUTABLE: "%AISHAREDIR%/%PYTHON_ENVIRONMENT.FOLDER_NAME%/bin/python3"
    TASKS: 16
    MEMORY: 16384
    WALLCLOCK: 01:00

  TRANSFER:
    FILE: templates/common/transfer.sh
    DEPENDENCIES: COMPRESS
    # Since this is running locally, can simply leave a long wallclock.
    WALLCLOCK: 24:00
    RUNNING: member
    PLATFORM: LOCAL

  CLEAN:
    FILE: templates/common/clean.sh
    DEPENDENCIES: TRANSFER
    WALLCLOCK: 00:10
    RUNNING: member
