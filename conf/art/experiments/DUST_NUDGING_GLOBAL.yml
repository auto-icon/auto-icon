# Copyright 2023 - 2024 Karlsruhe Institute of Technology (KIT) and Ludwig-Maximilians-Universität München (LMU)
# SPDX-FileContributor: Andreas Baer
#
# SPDX-License-Identifier: BSD-3-Clause

#-- > Experiment configuration for template ERA5_DUST_REINIT. < --#

#-- Resource requirements for your simulation
JOBS:
  RUN_ICON:
    NODES: 2
    WALLCLOCK: '00:10'
  PREP_REMAP_ERA2ICON:
    # The wallclock might have to be adapted to the number of files per chunk (about 3 min per file for Levante on one node).
    THREADS: 128        # This has to be adapted to the platform.


#-- ----- Grid info ----- --#
GRID:
  FILELIST:
    - GRID
    - EXTPAR
    - ERA5
    - STY
  RADGRID: True
  DOM1:
    TYPE: G
    R: 2
    B: 5
    GRID_NUMBER: 14
    EXTPAR_DATE: 20131206


#-- ----- Basic experiment info ----- --#
EXPERIMENT:
  #-- ----- Timing info ----- --#
  DATELIST: 20111201
  MEMBERS: "ecrad-nsph"
  CHUNKSIZEUNIT: day
  CHUNKSIZE: 2
  NUMCHUNKS: 1
  CHUNKINI: ''
  CALENDAR: standard

  TIMESTEP: 360
  TIMESTEP_FACTORS:
    DT_RAD: 6
    DT_CONV: 2
    DT_SSO: 4
    DT_GWD: 4
  CHECKPOINT_INTERVAL: 'P10D'
  NUDGING_INTERVAL: 43200   #-- Maximum 1 day = 86400
  NUDGING_RANGE: full       #-- "default": use namelist defaults, "full": use entire atmosphere height
  
  #-- ----- Defaults for all output files ----- --#
  OUTPUT_INTERVAL: 'PT12H'
  FILE_INTERVAL: 'P1D'
  STEPS_PER_FILE: 0

#-- ----- Directories and files info ----- --#
DIRECTORIES:
  INDIR:  "%DIRECTORIES.PREFIX_INDIR%/input/%EXPNAME%"
  OUTDIR: "%DIRECTORIES.PREFIX_OUTDIR%/output/%EXPNAME%"
  REFDIR: "%DIRECTORIES.PREFIX_REFDIR%/output/%EXPNAME%-ref"
  LINK_FILES:
    FILES:
      - 'xml'
    ICON_DATA:
      - 'rrtmg_lw.nc'
      - 'ECHAM6_CldOptProps.nc'
      - 'externals/ecrad/data|ecrad_data'
    ART:
      - 'runctrl_examples/xml_ctrl/DUST_RAD/Meng_ICON_CMD_USE.xml|Meng_ICON_CMD_USE.xml'
