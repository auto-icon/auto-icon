# Copyright 2023 - 2024 Karlsruhe Institute of Technology (KIT) and Ludwig-Maximilians-Universität München (LMU)
# SPDX-FileContributor: Andreas Baer
#
# SPDX-License-Identifier: BSD-3-Clause

#-- > Experiment configuration (full documented template file). < --#

#-- Resource requirements for your simulation
JOBS:
  RUN_ICON:
    #-- NODES = number of nodes to use
    NODES: 2
    #-- If NODES: 1, provide the number of processors as well!
    # PROCESSORS: 76
    #-- WALLCLOCK = wall time for the job (defaults to 04:00)
    WALLCLOCK: '00:30'


#-- ----- Grid info ----- --#
#-- The grid info is provided with the following sections.
GRID:
  #-- The FILELIST section provides a list of all input files that shall be used
  #-- for the current run.
  FILELIST:
    - GRID
    - EXTPAR
    # - IFS
    - DWDFG
    - DWDANA
    - BCF   #-- ART file
    - IAE   #-- ART file
    - STY   #-- ART file
  
  #-- The FILENAMES section provides patterns (with python format string notation)
  #-- for the file names in use. The provided list represents the default and providing
  #-- entries yourself is only necessary, if you need to change them. The placeholders
  #-- are those provided to each domain section,
  #-- i.e. R, B, TYPE, DOM (domain index), GRID_NUMBER, ART_IO_SUFFIX
  #--      EXTPAR_DATE, SDATE (start date of the global run)
  #-- Arbitrary files can also be added by inserting another key (e.g. INC) here and in the
  #-- FILELIST section.
  #-- You can also specify a FILENAMES section (with arbitrary keys) per domain, overwriting
  #-- the defaults from this section.
  # FILENAMES:
  #   GRID: "icon_grid_{GRID_NUMBER:04d}_R{R:02d}B{B:02d}_{TYPE:s}.nc"
  #   EXTPAR: "icon_extpar_{GRID_NUMBER:04d}_R{R:02d}B{B:02d}_{TYPE:s}_{EXTPAR_DATE:08d}.nc"
  #   #-- For MPIM grids instead:
  #   # EXTPAR:  "icon_extpar_grid_{GRID_NUMBER:04d}_R{R:02d}B{B:02d}_{TYPE:s}.nc"
  #   IFS: "uc1_ifs_t1279_grb2_remap_rev832_{GRID_NUMBER:04d}_R{R:02d}B{B:02d}_{TYPE:s}.nc"
  #   DWDFG: "dwdFG_R{R:1d}B{B:02d}_DOM{DOM:02d}.grb"
  #   DWDANA: "dwdana_R{R:1d}B{B:02d}_DOM{DOM:02d}.grb"
  #   ART: "ART_{AFT:3s}_iconR{R:1d}B{B:02d}-grid_{ART_IO_SUFFIX:s}.nc"

  #-- If the Radiation grid follows the official scheme (Grid number is smaller by one than the coarsest (global) grid)
  #-- then the radiation grid can be created automatically by turning on the following switch
  RADGRID: False
  #-- Alternatively, it can be specified explitily with the following section:
  # DOM0:
    # R: 2
    # B: 3
    # TYPE: R
    # GRID_NUMBER: 11
  #-- Each domain gets an own section. Domain numbering (DOMx with an integer x) represents the ordering of the
  #-- domains for ICON. Indices start at 1 for regular domains. A (global) radiation grid can be supplied explicitly
  #-- (see DOM0 above) and has index 0.
  #-- Look up default grids on the public repositories at
  #--     http://icon-downloads.mpimet.mpg.de/dwd_grids.xml   <- DWD grids/extpar data
  #--     http://icon-downloads.mpimet.mpg.de/mpim_grids.xml  <- MPIM grids data
  #-- to obtain the grid number for the respective R and B values.
  DOM1:
    #-- Type (G: global, R: radiation grid, O: ocean, Nxx: nested grid numbered xx, L: LAM grid, L*: radgrid or nest for LAM, ...)
    #-- The type letter(s) have to correspond to the suffix letters of the grids in the list (s. above).
    TYPE: G
    #-- R and B values of the grid. Leading zeroes are ignored.
    R: 2
    B: 4
    #-- Grid number of the official grid to use (s. above).
    GRID_NUMBER: 12
    #-- Date of the corresponding extpar dataset. The dataset has to be provided by the user or be present in the public repository.
    EXTPAR_DATE: 20131001
    #-- If the extpar file contains tiles, set the following to True.
    EXTPAR_TILES: False
    #-- For ART files, this suffix is added to the file name.
    ART_IO_SUFFIX: 'TCNR'
    #-- Basenames for the different input files can be provided here.
    #-- Final format: <basename>_ZZZZ_RxxByy[_T][_<DATE>][_tiles].nc
    #--     ZZZZ:   grid number padded to 4 digits
    #--     xx, yy: R and B values padded to 2 digits each
    #--     T:      Grid type (for GRID and EXTPAR)
    #--     DATE:   datestring (EXTPAR: YYYYMMDD; IFS,INC: YYYYMMDDHH (start date, see below))
    #--     _tiles: added if EXTPAR_TILES is set to true
    #-- BASENAME and FULLNAME are going to be replaced by the FILENAMES section, where you can specify arbitrary patterns.
    # BASENAME:
      # GRID:   'icon_grid'           #-- default value
      # EXTPAR: 'icon_extpar'         #-- <- for DWD extpar data (default)
      # EXTPAR: 'icon_extpar_grid'    #-- <- for MPIM extpar data
      # IFS:    ''                    #-- No default value exists!
      # INC:    ''                    #-- Other/additional input file.
    #-- Full names of the input files can alternatively be provided, e.g. for custom created grids/files.
    #-- BASENAME and FULLNAME are going to be replaced by the FILENAMES section, where you can specify arbitrary patterns.
    # FULLNAME:
      # GRID:   ''
      # EXTPAR: ''
      # IFS:    ''
      # INC:    ''
    #-- An optional FILENAMES section (with arbitrary keys) per domain, overwriting
    #-- the defaults from the outer section.
    # FILENAMES:
    #   GRID: "icon_grid_{GRID_NUMBER:04d}_R{R:02d}B{B:02d}_{TYPE:s}.nc"
    #   EXTPAR: "icon_extpar_{GRID_NUMBER:04d}_R{R:02d}B{B:02d}_{TYPE:s}_{EXTPAR_DATE:08d}.nc"
    #   #-- For MPIM grids instead:
    #   # EXTPAR:  "icon_extpar_grid_{GRID_NUMBER:04d}_R{R:02d}B{B:02d}_{TYPE:s}.nc"
    #   IFS: "uc1_ifs_t1279_grb2_remap_rev832_{GRID_NUMBER:04d}_R{R:02d}B{B:02d}_{TYPE:s}_{SDATE}.nc"
    #   DWDFG: "dwdFG_R{R:1d}B{B:02d}_DOM{DOM:02d}.grb"
    #   DWDANA: "dwdana_R{R:1d}B{B:02d}_DOM{DOM:02d}.grb"
    #   ART: "ART_{AFT:3s}_iconR{R:1d}B{B:02d}-grid_{ART_IO_SUFFIX:s}.nc"
    #-- If in a specific domain some of the files in the FILELIST are not required
    #-- (because they are infered from the parent domain), you can put them in the SKIP_FILES list for this domain.
    # SKIP_FILES: [DWDFG, DWDANA]


#-- ----- Basic experiment info ----- --#
EXPERIMENT:
  #-- ----- Timing info ----- --#
  #-- Standard autosubmit experiment directives
  DATELIST: 20000101
  MEMBERS: "default"
  CHUNKSIZEUNIT: month
  CHUNKSIZE: 1
  NUMCHUNKS: 1
  CHUNKINI: ''
  CALENDAR: standard
  #-- Custom auto-icon experiment directives
  #-- Time step of the simulation in seconds
  #-- An expression for bash (f90 namelists) or python (yaml namelist) is possible.
  TIMESTEP: 360
  #-- [yaml only] Multiplicative factors for the NWP time steps
  TIMESTEP_FACTORS:
    DT_RAD: 3
    DT_CONV: 1
    DT_SSO: 2
    DT_GWD: 2
  #-- Interval for writing checkpoint files (ISO 8601 period)
  CHECKPOINT_INTERVAL: 'P10D'
  
  #-- ----- Defaults for all output files ----- --#
  #-- Does not overwrite values provided in the namelist (.yml files)
  #-- 'FILE_INTERVAL' and 'STEPS_PER_FILE' are mutually exclusive!
  #-- Interval for writing all output (ISO 8601 period)
  OUTPUT_INTERVAL: 'PT12H'
  #-- File interval (ISO 8601 period, default: '')
  FILE_INTERVAL: 'P01D'
  #-- Steps per output file, defaults to 0
  STEPS_PER_FILE: 0
  
  #-- ----- Misc info ----- --#
  #-- Namelist parameters that shall apply to all chunks except the first one.
  #-- Format: yaml list with entries "<namelist name>:<parameter>:<value>"
  NML_SUB_NOT_FIRST: []


#-- ----- Reinitialization info ----- --#
REINITIALIZATION:
  #-- Continue with meterology, aerosol and/or chemistry from previous run.
  #-- Set to false if you want to reinitialize or do not neet that data.
  CONTINUE_MET: False
  CONTINUE_AERO: False
  CONTINUE_CHEM: False
  #-- Namelist parameters that shall apply to all chunks except the first one.
  #-- Format: yaml list with entries "<namelist name>:<parameter>:<value>"
  REINIT_SUBSTITUTES:
    - "initicon_nml:init_mode:7"
    - "initicon_nml:lread_ana:.false."
    - "initicon_nml:ltile_coldstart:.true."
    - "art_nml:iart_init_aero:5"


#-- ----- Directories and files info ----- --#
DIRECTORIES:
  #-- Directories for input and output data (prefixes are defined per platform)
  INDIR:  "%DIRECTORIES.PREFIX_INDIR%/input/%EXPNAME%"
  OUTDIR: "%DIRECTORIES.PREFIX_OUTDIR%/output/%EXPNAME%"
  #-- For the testcase (job include 'testcase'), the reference output directory is given by REFDIR
  # REFDIR: "%DIRECTORIES.PREFIX_REFDIR%/output/%EXPNAME%-ref"
  #-- Archive for input data: if ARCHIVE is specified, this URL will be downloaded, unzipped and
  #-- provided in INDIR. Non-zip archives or archives on local storage are not supported.
  # ARCHIVE: "https://bwsyncandshare.kit.edu/s/YdaqfMDgJPfo8QF/download?path=/%EXPNAME%"
  LINK_FILES:
    #-- ----- Input data ----- --#
    #-- Files to be linked to the working directory. For format specification see
    #-- https://gitlab.dkrz.de/auto-icon/auto-icon/-/wikis/Usage/Input-data#syntax-for-link-specification
    #
    #-- Files and directories relative to INDIR
    FILES: []
    #-- Files relative to the ICON root directory
    ICON_DATA:
      - 'rrtmg_lw.nc'
      - 'ECHAM6_CldOptProps.nc'
    #-- Files relative to the ART root directory
    ART: []


#-- ----- CDO postprocessing info ----- --#
CDO:
  #-- Global options (str or list of str)
  # OPTIONS:
  #-- SPLITVARS is a special section to split all files in the output directory into
  #--           one file per variable of the format <fn>-<var>.<suffix>, where <fn>
  #--           is the original file name without extension.
  # SPLITVARS:
  #-- Each section starts a specific operator.
  #-- The section name is unique but arbitrary.
  SELDUST:
    #-- Operator name
    OPERATOR: selname
    #-- Arguments to pass to the operator.
    ARGS: [dusta, dustb, dustc]
    #-- Input file(s) (one of):
    #-- - single item
    #-- - list of items
    #-- - dict of pairs: selection key & single/list of item(s)
    #-- Each 'item' is a file name/pattern/section name
    #-- Patterns match only files ending at %SIMULATION.FNO_PATTERN% .
    FILES: 'icon-art-%EXPNAME%-aero*'
    #-- Output file (if applicable).
    #-- A pattern of the form '/<sub>/<rep>/' will use as output file name
    #-- the input name and replace <sub> with <rep>.
    #-- If multiple input files are present, a single operator per input file
    #-- will be created.
    OUTPUT: '/aero/dust/'
  #-- Another section, demonstrating more complex input files.
  ADD:
    OPERATOR: add
    #-- The operator is binary, i.e. it takes exactly two file input arguments.
    BINARY: True
    #-- Input file(s):
    #-- For the given file name, select the variable with name in the list
    #-- and use the result as input for the operator. Each variable name will give a single input.
    #-- This implicitly uses operator chaining.
    FILES:
      'icon-art-%EXPNAME%-aero_*.nc':
        name: [dusta, dustb, dustc]
    #-- Output file:
    #-- The keyword "PASS" indicates that the output will not be written to a file but passed
    #-- on to another operator (to be accessed with the section name).
    #-- The keyword "PASSEACH" is similar but each physical file will be passed separately
    #-- such that it can be used in another operator separately.
    #-- E.g.: PASS results in one output with the sum of (dusta+dustb+dustc) of all files matching
    #--       the pattern 'icon-art-%EXPNAME%-aero_*.nc'.
    #--       PASSEACH results in one output per matching file containing only the sum 
    #--       (dusta+dustb+dustc) of the respective single input file.
    OUTPUT: PASS
  VAR_RENAME:
    OPERATOR: chname
    ARGS: [dusta, dust_total]
    #-- Input file(s):
    #-- Use the output from section "ADD" (where the output was specified as "PASS")
    #-- If PASSEACH would have been specified, then the OUTPUT should be a substitution
    #-- pattern to ensure that each of the output streams are correctly written to a 
    #-- separate file (after renaming variables).
    FILES: ADD
    #-- Output file (if applicable).
    OUTPUT: 'icon-art-%EXPNAME%-dust_total.nc'
  MERGE:
    OPERATOR: merge
    FILES:
      'icon-art-%EXPNAME%-aero*[0-9].nc':
        name: [z_mc, rho]
    ADDITIONAL_FILES:
      #-- The FILES section will result in a set of files, for each of which a single
      #-- command is triggered. The ADDITIONAL_FILES section will add further input
      #-- files to these operators:
      #-- For each input file, and each item in the list, a new input file is used.
      #-- These additional files are of the same form as input files. If a
      #-- substitute/replace pattern '/<sub>/<rep>/' is specified, the filename will
      #-- be derived from the input file.
      '/aero/all_dust_total/':
        name: [dust_total]
    OUTPUT: '/aero/cb_dust/'
