# Copyright 2023 - 2024 Karlsruhe Institute of Technology (KIT) and Ludwig-Maximilians-Universität München (LMU)
# SPDX-FileContributor: Andreas Baer
#
# SPDX-License-Identifier: BSD-3-Clause

#-- > Experiment configuration for template VOLAERO_RAD. < --#

#-- Resource requirements for your simulation
JOBS:
  RUN_ICON:
    NODES: 8
    WALLCLOCK: '01:00'

SAMOA:
  LIST: "samoa_list_volaero_rad"



#-- ----- Grid info ----- --#
GRID:
  FILELIST:
    - GRID
    - EXTPAR
    - DWDFG
  RADGRID: False
  DOM1:
    TYPE: G
    R: 2
    B: 6
    GRID_NUMBER: 24
    EXTPAR_DATE: 20200917
    EXTPAR_TILES: True
    FILENAMES:
      DWDFG: 'icon_init_{GRID_NUMBER:04d}_R{R:02d}B{B:02d}_{SDATE}.nc|icon_init_{GRID_NUMBER:04d}_R{R:02d}B{B:02d}_{SDATE}.nc'



#-- ----- Basic experiment info ----- --#
EXPERIMENT:
  #-- ----- Timing info ----- --#
  DATELIST: 2019062112
  MEMBERS: "default"
  CHUNKSIZEUNIT: day
  CHUNKSIZE: 1
  NUMCHUNKS: 1
  CHUNKINI: ''
  CALENDAR: standard

  TIMESTEP: 180
  TIMESTEP_FACTORS:
    DT_RAD: 6
    DT_CONV: 2
    DT_SSO: 4
    DT_GWD: 4
  CHECKPOINT_INTERVAL: 'P10D'
  
  #-- ----- Defaults for all output files ----- --#
  OUTPUT_INTERVAL: 'PT06M'
  FILE_INTERVAL: ''
  STEPS_PER_FILE: 1
  
  #-- ----- Misc info ----- --#
  # NML_SUB_NOT_FIRST: []



#-- ----- Directories and files info ----- --#
DIRECTORIES:
  INDIR:  "%DIRECTORIES.PREFIX_INDIR%/input/%EXPNAME%"
  OUTDIR: "%DIRECTORIES.PREFIX_OUTDIR%/output/%EXPNAME%"
  REFDIR: "%DIRECTORIES.PREFIX_REFDIR%/output/%EXPNAME%-ref"
  ARCHIVE: "https://bwsyncandshare.kit.edu/s/YdaqfMDgJPfo8QF/download?path=/%EXPNAME%"
  LINK_FILES:
    FILES: []
    ICON_DATA:
      - 'rrtmg_lw.nc'
      - 'ECHAM6_CldOptProps.nc'
      - 'externals/ecrad/data|ecrad_data'
      - 'run/ana_varnames_map_file.txt|map_file.ana'
    ART:
      - 'Linoz2004Br.dat'
      - 'Simnoy2002.dat'
      - 'FJX_j2j.dat'
      - 'FJX_j2j_extended.dat'
      - 'FJX_scat-aer.dat'
      - 'FJX_scat-cld.dat'
      - 'FJX_scat-ssa.dat'
      - 'FJX_scat-UMa.dat'
      - 'FJX_spec.dat'
      - 'FJX_spec_extended.dat'
      - 'FJX_spec_extended_lyman.dat'
      - 'atmos_std.dat'
      - 'atmos_h2och4.dat'
      - 'runctrl_examples/xml_ctrl/VOLAERO_RAD|xml'
