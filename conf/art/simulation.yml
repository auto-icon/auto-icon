# Copyright 2023 - 2024 Karlsruhe Institute of Technology (KIT) and Ludwig-Maximilians-Universität München (LMU)
# SPDX-FileContributor: Andreas Baer
#
# SPDX-License-Identifier: BSD-3-Clause

#-- > General config specific to ICON_CASE: art < --#

SIMULATION:
  #-- Format specification according to the 'date' util
  #-- > Do not change unless you know what you are doing.
  DATE_FORMAT: '+%Y-%m-%dT%H:%M:%SZ'
  #-- Filetype for input and output data, passed on to ICON directly.
  FILETYPE: 4
  #-- Require the output directory to be clean to prevent accidental overwriting.
  REQUIRE_CLEAN_OUTDIR: True
  #-- Filename patterns for (all) output files.
  FNO_PATTERN: "*_DOM[0-9][0-9]_[MHPI]L_[0-9][0-9][0-9][0-9].nc"
  #-- Do the remapping even if the file is present.
  REQUIRE_REMAPPING: False
  #-- Interpolation method for ERA2ICON interpolation.
  INTERPOLATION_METHOD: 2
  #-- Required input variables
  INPUT_VARIABLES:
    - T
    - VN
    - W
    - LNPS
    - GEOP_SFC
    - GEOP_ML
    - QV
    - QC
    - QI
    - QR
    - QS
    - T_SNOW
    - W_SNOW
    - RHO_SNOW
    - ALB_SNOW
    - SKT
    - SST
    - STL1
    - STL2
    - STL3
    - STL4
    - CI
    - W_I
    - LSM
    - SMIL1
    - SMIL2
    - SMIL3
    - SMIL4

DIRECTORIES:
  #-- Locations for storing workflow files, e.g. file and domain lists
  WORKFLOW: "%CURRENT_LOGDIR%/workflow_data"

NAMELIST:
  #-- > Do not change unless you know what you are doing.
  MASTER: 'icon_master.namelist'
  ATMO: '%EXPNAME%.nml'

SAMOA:
  URL: "https://gitlab.dkrz.de/art/SAMOA.git"
  BRANCH: "samoa-icon-art_volaero-fix"

REINITIALIZATION:
  CONTINUE_VARIABLES:
    MET:
      - 'group:MODE_INIANA'
      - 'group:DWD_FG_SFC_VARS'
      - 'group:DWD_FG_ATM_VARS'
    AERO:
      - 'group:ART_CHEMISTRY'
      - 'group:ART_AEROSOL'
      - 'group:ART_ROUTINE_DIAG'
    CHEM:
      - 'group:ART_CHEMISTRY'
      - 'clc'
      - 'theta_v'
      - 'pv'
