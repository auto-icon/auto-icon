# Copyright 2023 - 2024 Karlsruhe Institute of Technology (KIT) and Ludwig-Maximilians-Universität München (LMU)
# SPDX-FileContributor: Andreas Baer, Oriol Tinto Prims, Fabian Jakub
#
# SPDX-License-Identifier: BSD-3-Clause

#-- > Gitlab-ci config file. < --#

variables:
  PROJECT_BRANCH: $CI_COMMIT_REF_NAME

#-- Workflow rules
workflow:
  name: "KIT pipeline: level = $LEVEL, source = $CI_PIPELINE_SOURCE"

clean-spack:
  stage: clean
  script:
    - 'rm -rf /scratch/g/gitlab-runner/autoicon-spack || :'
    - 'rm -rf /scratch/g/gitlab-runner/autoicon-spackcache || :'
    - 'rm -rf /scratch/g/gitlab-runner/autoicon-spackconfig || :'
  tags:
    - slurm.meteo.physik.lmu.de

  rules:
    - if: '$CLEAN_UP == "true"'
      when: always
    - when: never

build-only:
  stage: build
  script:
    - ./examples/00_build_only.sh
  tags:
    - slurm.meteo.physik.lmu.de
  variables:
    SRUN_OPTIONS: "--time 04:00:00 --mem 18G -n 4 -c 2"

  artifacts:
    name: build-only-logs
    when: always
    paths:
      - ./autosubmit/**/tmp
    expire_in: 7 days

real-from-ideal:
  stage: run_tests
  script:
    - ./examples/01_real-from-ideal.sh
  tags:
    - slurm.meteo.physik.lmu.de
  variables:
    SRUN_OPTIONS: "--time 04:00:00 --mem 18G -n 4 -c 2"

  artifacts:
    name: real-from-ideal-logs
    when: always
    paths:
      - ./autosubmit/**/tmp
    expire_in: 7 days

real-from-ideal-psp:
  stage: run_tests
  script:
    - ./examples/01_real-from-ideal+psp.sh
  tags:
    - slurm.meteo.physik.lmu.de
  variables:
    SRUN_OPTIONS: "--time 04:00:00 --mem 18G -n 4 -c 2"

  artifacts:
    name: real-from-ideal+psp-logs
    when: always
    paths:
      - ./autosubmit/**/tmp
    expire_in: 7 days

real-from-dwd-ana:
  stage: run_tests
  script:
    - ./examples/02_real-from-dwd-ana.sh
  tags:
    - slurm.meteo.physik.lmu.de
  variables:
    SRUN_OPTIONS: "--time 04:00:00 --mem 64G -n 16 -c 2"

  artifacts:
    name: real-from-dwd-ana-logs
    when: always
    paths:
      - ./autosubmit/**/tmp
    expire_in: 7 days

real-from-dwd-ana-B6:
  stage: run_tests
  script:
    - ./examples/02_real-from-dwd-ana_B6.sh
  tags:
    - slurm.meteo.physik.lmu.de
  variables:
    SRUN_OPTIONS: "--time 04:00:00 --mem 64G -n 16 -c 2"

  artifacts:
    name: real-from-dwd-ana-B6-logs
    when: always
    paths:
      - ./autosubmit/**/tmp
    expire_in: 7 days
