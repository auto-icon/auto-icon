#!/usr/bin/bash

#-- > Generator script for the gitlab-ci file for KIT pipelines. < --#

# Copyright 2023 - 2024 Karlsruhe Institute of Technology (KIT) and Ludwig-Maximilians-Universität München (LMU)
# SPDX-FileContributor: Andreas Baer
#
# SPDX-License-Identifier: BSD-3-Clause

set -e

#-- ----- Variables and definitions ----- --#
CONFIGFILE=".gitlab-ci/testcases.yml"
mapfile -d" " -t archlist < <(echo -n "${ARCHITECTURES}")
mapfile -t experiments < <(yq 'keys | .[]' "$CONFIGFILE")

#-- ----- Write configuration ----- --#
#-- General templates
cat .gitlab-ci/job-templates.yml

#-- Specific jobs
for expname in "${experiments[@]}"
do
    for arch in "${archlist[@]}"
    do
        #-- Skip if pipeline level does not fit.
        lvl=$(yq ".${expname}.LEVEL.${arch}" "$CONFIGFILE")
        expid=$(yq ".${expname}.EXPID.${arch}" "$CONFIGFILE")
        flags=$(yq ".${expname}.INIT_FLAGS" "$CONFIGFILE")
        install_options=$(yq ".${expname}.ICON_INSTALL_OPTIONS" "$CONFIGFILE")
        if [ "${lvl}" -lt "$LEVEL" ]; then
            echo "#-- Skipping ${arch}-${expname} with level ${lvl}"
            continue
        fi
        #-- Templates
        cat << EOF
.template-${expname}-${arch}:
  variables:
    EXPNAME: ${expname}
    HPCARCH: ${arch}
    EXPID: ${expid}
    INIT_FLAGS: '${flags}'
EOF
        if [ "$install_options" ] && [ ! "$install_options" = "null" ]; then
            echo "    ICON_INSTALL_OPTIONS: '${install_options}'"
        fi
        #-- Create jobs
        cat << EOF
as_create-${expname}-${arch}:
  extends:
    - .as_create
    - .template-${expname}-${arch}
EOF
        #-- Run jobs
        cat << EOF
as_run-${expname}-${arch}:
  extends:
    - .as_run
    - .template-${expname}-${arch}
  needs:
    - as_create-${expname}-${arch}
EOF
        #-- Clean jobs
        cat << EOF
as_clean-${expname}-${arch}:
  extends:
    - .as_clean
    - .template-${expname}-${arch}
  needs:
    - as_run-${expname}-${arch}
EOF
    done
done
