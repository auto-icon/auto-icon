#!/usr/bin/python3

#-- > Preparation of a member: working directory and input files < --#

# Copyright 2023 - 2024 Karlsruhe Institute of Technology (KIT) and Ludwig-Maximilians-Universität München (LMU)
# SPDX-FileContributor: Andreas Baer
#
# SPDX-License-Identifier: BSD-3-Clause

import sys
sys.path.append("%PYTHON_ENVIRONMENT.MODULES_DIR%")

import logging
import os
from pathlib import Path
from subprocess import PIPE, STDOUT, run

from experiment_config import FullExperimentConfig
from icon_file import ExtraFile, InputFile, JsbachFile
import icon_file_handler as ifh
from python_utils import checkCleanDir


if __name__ == "__main__":
    logger = logging.getLogger(__name__)
    expconf = FullExperimentConfig()
    logging.basicConfig(level=expconf.LOGLEVEL)
    logger.setLevel(expconf.LOGLEVEL)
    expconf.SDATE = "%SDATE%"
    expconf.MEMBER = "%MEMBER%"
    expconf.check(level="member")

    outdir = Path(expconf.OUTDIR, expconf.SDATE, expconf.MEMBER)
    workflowdir = Path(expconf.WORKFLOWDIR, expconf.SDATE)
    logger.info(f"Using outdir '{outdir}'.")
    if expconf.REQUIRE_CLEAN_OUTDIR and not checkCleanDir(outdir):
        raise RuntimeError(f"[410] Outdir '{outdir}' is required to be clean but ist not!")
    outdir.mkdir(parents=True, exist_ok=True)
    workflowdir.mkdir(parents=True, exist_ok=True)
    os.chdir(outdir)

    #-- Domains
    fn_domains = Path(expconf.WORKFLOWDIR, expconf.SDATE, "domains.pkl")
    domains = ifh.loadDomains(fn_domains)
    for domain in domains:
        for file in domain.getFilesList():
            if isinstance(file, InputFile):
                logger.info(file)
                file.createLink()
            else:
                logger.info(f"Skipping file {file!r}.")
    #-- Extra files
    fn_files = Path(expconf.WORKFLOWDIR, expconf.SDATE, "extraFiles.pkl")
    filesList = ifh.loadFiles(fn_files)
    for file in filesList:
        if isinstance(file, ExtraFile):
            logger.info(file)
            file.createLink()
        else:
            logger.info(f"Skipping file '{file}'.")
    #-- Jsbach files
    fn_files = Path(expconf.WORKFLOWDIR, "jsbachFiles.pkl")
    if fn_files.exists():
        filesList = ifh.loadFiles(fn_files)
        for file in filesList:
            if isinstance(file, JsbachFile):
                logger.info(file)
                file.createLink()
            else:
                logger.info(f"Skipping file '{file}'.")

    logger.debug("Trying to invoke icon ...")
    res = run([expconf.ICONFOLDER + "/bin/icon", "--version"],
              stdout=PIPE, stderr=STDOUT, text=True, check=expconf.ICON_CHECK_STRICT)
    print(res.stdout)
    with open("icon_version.txt", 'w') as f:
        f.write(res.stdout)
