#!/usr/bin/bash

#-- > Move the outputs to the proper destination. < --#

# Copyright 2023 - 2024 Karlsruhe Institute of Technology (KIT) and Ludwig-Maximilians-Universität München (LMU)
# SPDX-FileContributor: Oriol Tinto Prims, Fabian Jakub, Andreas Baer
#
# SPDX-License-Identifier: BSD-3-Clause

set -e  #-- This will be set by autosubmit anyway and allows for proper shell syntax checking.

#-- Get some variables provided by autosubmit.
ROOTDIR_KIT="%DIRECTORIES.OUTDIR%"
ROOTDIR_LMU="%HPCROOTDIR%"
STARTDATE="%SDATE%"
MEMBER="%MEMBER%"
OUTPUT_FILES="%SIMULATION.OUTPUT_FILE_NAMES%"
OUTPUT_PATTERN="%SIMULATION.FNO_PATTERN%"
CURRHOST="%CURRENT_HOST%"
HPCUSER="%HPCUSER%"
HPCHOST="%HPCHOST%"

if [ -z "$OUTPUT_FILES" ] && [ "$OUTPUT_PATTERN" ]
then
  #-- If the output files are not specified explicitly,
  #-- use the general pattern for output files.
  OUTPUT_FILES="$OUTPUT_PATTERN"
fi

#-- Define output dir in remote machine.
OUTPUT_DIR="${ROOTDIR_LMU}/output/${STARTDATE}/${MEMBER}"
if [ ! -d "$OUTPUT_DIR" ]
then
  OUTPUT_DIR="${ROOTDIR_KIT}/${STARTDATE}/${MEMBER}"
  if [ ! -d "$OUTPUT_DIR" ]
  then
    echo "ERROR [512]: No output directory exists."
    exit 1
  fi
fi

MAIN_LOCAL_FOLDER="%DATA_MANAGEMENT.LOCAL_DESTINATION_FOLDER%/%DEFAULT.EXPID%"

DESTINATION_DIR="${MAIN_LOCAL_FOLDER}/${STARTDATE}/${MEMBER}"

mkdir -p ${DESTINATION_DIR}

if [ "$CURRHOST" == "localhost" ]
then
  #-- Job running on the LOCAL platform.
  if [[ "$HPCHOST" == "localhost" ]]; then
    cd "$OUTPUT_DIR"
    rsync -v ${OUTPUT_FILES} "${DESTINATION_DIR}/"
  else
    #-- Copy the output files.
    for file_name in ${OUTPUT_FILES}; do
      rsync -v ${HPCUSER}@${HPCHOST}:${OUTPUT_DIR}/${file_name} "${DESTINATION_DIR}/"
    done
  fi
else
  #-- Job running on a remote platform.
  cd "$OUTPUT_DIR"
  rsync -v ${OUTPUT_FILES} "${DESTINATION_DIR}/"
fi
