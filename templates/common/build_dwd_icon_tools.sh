#!/usr/bin/bash

#-- > Build the ICON model. < --#

# Copyright 2023 - 2024 Karlsruhe Institute of Technology (KIT) and Ludwig-Maximilians-Universität München (LMU)
# SPDX-FileContributor: Andreas Baer
#
# SPDX-License-Identifier: BSD-3-Clause

set -e  #-- This will be set by autosubmit anyway and allows for proper shell syntax checking.
MODULES="%ENVIRONMENT.MODULES.GIT%"
if [ "${MODULES}" ]; then module load ${MODULES}; fi

#-- ----- -------------------- ----- --#
#-- Check if icon tools binaries exists.
#-- Arguments:
#--   Prefix directory, i.e. check $1/bin/<binary>
#-- Outputs:
#--   Writes confirmation if exists to stdout
#-- Returns:
#--   0 if exists
#--   2 otherwise
#-- ----- -------------------- ----- --#
function check_for_binary() {
    local PREFIX=$1
    binaries=(cdi icondelaunay icongpi icongridgen iconremap iconsub)
    for binary in "${binaries[@]}"
    do
        if [ -f "$PREFIX/bin/$binary" ]
        then
            echo "Binary $PREFIX/bin/$binary exists."
        else
            return 2
        fi
    done
}

#-- ----- -------------------- ----- --#
#-- Check for the presence and emptiness of the install directory
#-- and write permissions.
#-- Arguments:
#--   Install directory to check
#-- Exits:
#--   1 on any error
#-- ----- -------------------- ----- --#
function check_install_directory() {
    local install_dir=$1
    if [ ! -d "$install_dir" ]
    then
        mkdir "$install_dir"
    else
        if [ -f "$install_dir" ]
        then
            echo "ERROR [417]: install directory '$install_dir' is a regular file not a directory!" >&2
            exit 1
        fi
        if [ ! -w "$install_dir" ]
        then
            echo "ERROR [417]: no write permissions in install directory '$install_dir', aborting." >&2
            exit 1
        fi
        REQUIRE_CLEAN_TOOLSDIR="%DWDICONTOOLS.REQUIRE_CLEAN_TOOLSDIR%"
        if [ "${REQUIRE_CLEAN_TOOLSDIR,,}" = "true" ] && [ "$(ls -A "$install_dir")" ]
        then
            echo "ERROR [417]: install directory '$install_dir' is required to be clean, but is not! You can change the behaviour with ICON.REQUIRE_CLEAN_TOOLSDIR or ensure the directory is clean."
            exit 1
        fi
    fi
}

#-- ----- -------------------- ----- --#
#-- Clone and update the icon repository.
#-- Arguments:
#--   Repository directory
#-- Exits:
#--   1 on any (git related) error
#-- ----- -------------------- ----- --#
function pull_repository() {
    local repo_dir=$1
    local prev_dir
    prev_dir=$(pwd)

    local URL="https://gitlab.dkrz.de/dwd-sw/dwd_icon_tools.git"
    local BRANCH=master      #-- branch or tag to be cloned
    local COMMIT=HEAD        #-- commit id/rev/...; 'HEAD' if targeting the latest commit of the branch
    local DEPTH=1            #-- Depth of the cloned repository

    #-- Create or update repository
    if [ ! -d "$repo_dir" ] || [ ! "$(ls -A "$repo_dir")" ]
    then
        #-- Build directory does not yet exist or is empty.
        #-- Create all parent directories if not yet existing.
        mkdir -p "$repo_dir"
        git clone -n --depth "$DEPTH" --single-branch -b "$BRANCH" "$URL" "$repo_dir"
        cd "$repo_dir"
        git checkout "$COMMIT"
        git submodule init
        git submodule update
        cd "$prev_dir"
    else
        #-- Build directory does exist.
        cd "$repo_dir"
        if ! git status > /dev/null 2>&1
        then
            echo "ERROR [417]: repository directory does exist but is not a git repository!" >&2
            exit 1
        fi

        #-- If the branch is HEAD, i.e. if the latest version should be used:
        #-- update repository, otherwise use current state.
        if [ "$COMMIT" = HEAD ]
        then
            #-- Check for correctness of current branch
            if [ ! "$(git rev-parse --abbrev-ref HEAD)" = "$BRANCH" ] && ! git switch "$BRANCH"
            then
                {
                echo "ERROR [416]:"
                echo "  Current branch $(git rev-parse --abbrev-ref HEAD) does not equal desired branch $BRANCH and cannot be switched."
                echo "  This is probably due to a change in the desired branch in the auto-icon config and the checkout of a single branch."
                echo "  Check your repository."
                } >&2
                exit 1
            fi
            git pull --recurse-submodules
        else
            #-- Detached HEAD state is expected.
            #-- Check if current commit is desired commit.
            if [ ! "$(git rev-parse $COMMIT)" = "$(git rev-parse HEAD)" ]
            then
                echo "ERROR [416]: current commit is not the desired commit! Check your repository at '$(pwd)'." >&2
                git status 1>&2
                exit 1
            fi
        fi
        cd "$prev_dir"
    fi
}

#-- ----- -------------------- ----- --#
#-- Run the configuration script
#-- Arguments:
#--   Build directory
#--   Repository directory
#--   Install directory
#-- ----- -------------------- ----- --#
function run_configure() {
    local build_dir=$1
    local prev_dir
    prev_dir=$(pwd)

    #-- If run as a batch job, slurm environment variables
    #-- can confuse the configure script, thus unset slurm variables.
    #-- Store the SLURM environment variables and unset them
    if env | grep SLURM >slurm_env.txt
    then
        unset "${!SLURM@}"
    fi

    #-- Run configure script
    cd "$build_dir"
    # "$config_file" --prefix="$install_dir" "${CONFIG_OPTIONS[@]}"
    sed -i 's#./configure#./configure --prefix="$(pwd)"#' do_configure.sh
    ./do_configure.sh
    cd "$prev_dir"
    
    #-- The SLURM variables can now be restored with the following command:
    #-- (this is currently not done as it is not necessary)
    # . <(sed 's/^/export /' slurm_env.txt)
}

#-- ----- -------------------- ----- --#
#-- Run make.
#-- Arguments:
#--   Build directory
#-- ----- -------------------- ----- --#
function run_make() {
    local build_dir=$1
    local numprocs="%NUMPROC%"
    local prev_dir
    prev_dir=$(pwd)

    cd "$build_dir"
    make -j"$numprocs"
    cd "$prev_dir"
}

#-- ----- -------------------- ----- --#
#-- Install DWD ICON tools to the install directory
#-- Arguments:
#--   Build directory
#--   Repository directory
#-- ----- -------------------- ----- --#
function run_install() {
    local build_dir=$1
    # local repo_dir=$2
    # "${repo_dir}/utils/move_to_prefix.sh" --builddir="$build_dir"
    local prev_dir
    prev_dir=$(pwd)

    cd "$build_dir"
    make install
    cd "$prev_dir"
}



#-- ----- Main function ----- --#
function run_main() {
    #-- Build and installation is all in the repository directory, thus only this is used.
    local REPODIR="%DWDICONTOOLS.REPODIR%"
    local ENSURE_BUILD="%DWDICONTOOLS.ENSURE_BUILD%"
    local REBUILD_FROM_SCRATCH="%DWDICONTOOLS.REBUILD_FROM_SCRATCH%"

    #-- Check if rebuild from scratch is required
    #-- If so, all directories will be deleted, thus everything is done from scratch.
    if [ "${REBUILD_FROM_SCRATCH,,}" = true ]
    then
        #-- Set ENSURE_BUILD and delete all directories
        ENSURE_BUILD=true
        rm -rf "$REPODIR"
    fi

    #-- ----- Main routine ----- --#
    if check_for_binary "$REPODIR" && [ ! "${ENSURE_BUILD,,}" = true ]
    then
        echo "Nothing to be done."
        return
    else
        echo "Binary does not exist or re-running build is required. Do a build."
        check_install_directory "$REPODIR"
        pull_repository "$REPODIR"
        run_configure "$REPODIR"
        run_make "$REPODIR"
        run_install "$REPODIR"
        #-- Check if building was successful.
        check_for_binary "$REPODIR"
    fi
}

run_main
