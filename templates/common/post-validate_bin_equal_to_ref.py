#!/usr/bin/python3

#-- > Run SAMOA post-processing tool. < --#

# Copyright 2023 - 2024 Karlsruhe Institute of Technology (KIT) and Ludwig-Maximilians-Universität München (LMU)
# SPDX-FileContributor: Andreas Baer
#
# SPDX-License-Identifier: BSD-3-Clause

import sys
sys.path.append("%PYTHON_ENVIRONMENT.MODULES_DIR%")

import logging
import os   # Keep as it is necessary for environment modules.
from pathlib import Path
from subprocess import run

from experiment_config import ExperimentConfig
import icon_file_handler as ifh
from environment_modules import module as ml


#-- ----- Initialization ----- --#
logger = logging.getLogger(__name__)
expconf = ExperimentConfig()
logging.basicConfig(level=expconf.LOGLEVEL)
logger.setLevel(expconf.LOGLEVEL)
CHECK_ALL = bool("%MISC.CHECK_ALL_OUTPUT_FILES%")
cdoModule = "%ENVIRONMENT.MODULES.CDO%"
modulesFile = "%ENVIRONMENT.MODULESINITFILE%"
res = ml('load', cdoModule)
if not res: raise RuntimeError(f"[415] Module file {cdoModule!r} could not be loaded.")


#-- ----- Set up files list ----- --#
os.chdir(expconf.OUTDIR)
logger.debug(f"Changed to directory '{os.getcwd()}'")

workflowdir = Path(expconf.WORKFLOWDIR)
workflowdir.mkdir(parents=True, exist_ok=True)
resultFiles = []
for fnResultFiles in workflowdir.rglob("resultFiles.pkl"):
    resultFiles.extend( ifh.createResultFilesList(expconf, fnResultFiles, read=True, write=False) )

filesToCheck = []
filesToCheckStr = []
for file in resultFiles:
    #-- Do not compare reinitialization files or secondary files.
    if (file.getFilename().startswith("auto-icon") or
        not file.primary): continue
    filesToCheck.append(file)
    filesToCheckStr.append(file.getFilepath())


#-- ----- Run checks ----- --#
logger.info(f"Checking {len(filesToCheckStr)} files.")
logger.debug(filesToCheckStr)
if len(filesToCheckStr) == 0:
    raise ValueError("[414] No files found to check! Check FNO_PATTERN in conf/<ICON_CASE>/simulation.yml.")
nfailed = 0
for file in filesToCheckStr:
    cmd = ['cdo', 'diffn',
            file,
            file.replace(expconf.OUTDIR, expconf.REFDIR)]
    res = run(cmd)
    if res.returncode != 0:
        if not CHECK_ALL: res.check_returncode()
        else: nfailed = nfailed + 1

#-- Fail the job if a test failed and all files should have been checked
if nfailed > 0:
    logger.error(f"{nfailed} of {len(filesToCheckStr)} files are not equal!")
    exit(1)
