#!/usr/bin/python3

#-- > Run SAMOA post-processing tool. < --#

# Copyright 2023 - 2024 Karlsruhe Institute of Technology (KIT) and Ludwig-Maximilians-Universität München (LMU)
# SPDX-FileContributor: Andreas Baer
#
# SPDX-License-Identifier: BSD-3-Clause

import sys
sys.path.append("%PYTHON_ENVIRONMENT.MODULES_DIR%")

import logging
import os   # Keep as it is necessary for environment modules.
from pathlib import Path
from subprocess import run

from experiment_config import FullExperimentConfig
import icon_file_handler as ifh
from environment_modules import module as ml


def main():
    #-- ----- Initialization ----- --#
    logger = logging.getLogger(__name__)
    expconf = FullExperimentConfig()
    logging.basicConfig(level=expconf.LOGLEVEL)
    logger.setLevel(expconf.LOGLEVEL)
    expconf.SDATE = "%SDATE%"
    expconf.MEMBER = "%MEMBER%"
    expconf.check(level="member")
    samoa_list = "%SAMOA.LIST%"
    cdoModule = "%ENVIRONMENT.MODULES.CDO%"
    res = ml('load', cdoModule)
    if not res: raise RuntimeError(f"[415] Module file {cdoModule!r} could not be loaded.")


    #-- ----- Set up files list ----- --#
    os.chdir(expconf.OUTDIR)
    logger.debug(f"Changed to directory '{os.getcwd()}'")

    workflowdir = Path(expconf.WORKFLOWDIR, expconf.SDATE, expconf.MEMBER)
    workflowdir.mkdir(parents=True, exist_ok=True)
    fnResultFiles = workflowdir / "resultFiles.pkl"
    resultFiles = ifh.createResultFilesList(expconf, fnResultFiles, initAsPrimary=True)

    filesToCheck = []
    filesToCheckStr = []
    skipped_files = []
    for file in resultFiles:
        if file.samoa == "PASSED" or file.samoa == "FAILED":
            skipped_files.append(file)
        else:
            filesToCheck.append(file)
            filesToCheckStr.append(file.getFilepath())


    #-- ----- Run SAMOA ----- --#
    if len(filesToCheckStr) == 0:
        if len(skipped_files) > 0:
            logger.info("Only already checked files found, nothing to do.")
            return
        else:
            raise ValueError("[414] No files found to check! Check FNO_PATTERN in conf/<ICON_CASE>/simulation.yml.")
    logger.info(f"Checking {len(filesToCheckStr)} files.")
    logger.debug(filesToCheckStr)
    samoa = Path("%AISHAREDIR%", "SAMOA", 'samoa.sh')
    cmd = [str(samoa),
            '-cdf',
            '-mod', 'ICON',
            '-m',
            '--equl_val_test_of',
            '--skull_off']
    if len(samoa_list) > 0:
        cmd.append('-l')
        cmd.append(str(Path("%AISHAREDIR%", "SAMOA", samoa_list)))
    cmd.extend(filesToCheckStr)
    res = run(cmd)


    #-- ----- Check result ----- --#
    if res.returncode == 0:
        for file in filesToCheck:
            file.samoa = "PASSED"
    else:
        # TODO(abaer): do not fail all files here
        for file in filesToCheck:
            file.samoa = "FAILED"
        logger.warning("At least one of the folling files failed the SAMOA test:")
        logger.warning(filesToCheckStr)

    ifh.saveFiles(fnResultFiles, resultFiles)
    #-- Fail the job if a samoa test failed
    res.check_returncode()

if __name__ == "__main__": main()
