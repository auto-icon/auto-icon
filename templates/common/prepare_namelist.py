#!/usr/bin/python3

#-- > Prepare the namelist for an experiment (yaml namelists). < --#

# Copyright 2023 - 2024 Karlsruhe Institute of Technology (KIT) and Ludwig-Maximilians-Universität München (LMU)
# SPDX-FileContributor: Andreas Baer
#
# SPDX-License-Identifier: BSD-3-Clause

from pathlib import Path
import shutil
import sys
sys.path.append("%PYTHON_ENVIRONMENT.MODULES_DIR%")

import logging
from types import SimpleNamespace

from experiment_config import FullExperimentConfig
from namelist_utils import createPatchesChunks, createPatchesGrid, createPatchesNudging, createPatchesOcean, createPatchesOutput, createPatchesReinit, createPatchesTiming, getNamelistPaths, processNamelist
from python_utils import optionalint, readFromAsList

#-----------------------------------------------------------------------------
#-- Debugging the variables provided by AS
#-- SDATE             %SDATE%
#-- MEMBER            %MEMBER%
#-- CHUNK             %CHUNK%
#-- SPLIT             %SPLIT%
#-- DELAY             %DELAY%
#-- DAY_BEFORE        %DAY_BEFORE%
#-- CHUNK_END_IN_DAYS %CHUNK_END_IN_DAYS%
#-- CHUNK_START_DATE  %CHUNK_START_DATE%
#-- CHUNK_START_YEAR  %CHUNK_START_YEAR%
#-- CHUNK_START_MONTH %CHUNK_START_MONTH%
#-- CHUNK_START_DAY   %CHUNK_START_DAY%
#-- CHUNK_START_HOUR  %CHUNK_START_HOUR%
#-- CHUNK_END_DATE    %CHUNK_END_DATE%
#-- CHUNK_END_YEAR    %CHUNK_END_YEAR%
#-- CHUNK_END_MONTH   %CHUNK_END_MONTH%
#-- CHUNK_END_DAY     %CHUNK_END_DAY%
#-- CHUNK_END_HOUR    %CHUNK_END_HOUR%
#-- STARTDATES        %STARTDATES%
#-- PREV              %PREV%
#-- CHUNK_FIRST       %CHUNK_FIRST%
#-- CHUNK_LAST        %CHUNK_LAST%
#-----------------------------------------------------------------------------

#-- ----- Main function ----- --#
def main():
    """
    Main function to process all the namelist patches.
    """
    logger = logging.getLogger(__name__)
    expconf = FullExperimentConfig()
    logging.basicConfig(level=expconf.LOGLEVEL)
    logger.setLevel(expconf.LOGLEVEL)
    #-- Initialize autosubmit variables to expconf namespace
    expconf.JOBNAME = "%JOBNAME%"
    expconf.SDATE = "%SDATE%"
    expconf.CHUNKDATE = "%CHUNK_START_DATE%"
    expconf.CHUNKSIZEUNIT = "%EXPERIMENT.CHUNKSIZEUNIT%".upper()
    expconf.MEMBER = "%MEMBER%"
    expconf.IS_FIRST_CHUNK = ("%CHUNK_FIRST%".upper()  == 'TRUE')
    expconf.CHUNK_STR = "{:04}".format(int("%CHUNK%"))
    expconf.check(level="chunk", check_jobname=True, check_chunk_full=True)

    #-- Additional configuration
    xConf = SimpleNamespace()
    xConf.NUMCHUNKS = int("%EXPERIMENT.NUMCHUNKS%")
    xConf.CHUNKSIZE = int("%EXPERIMENT.CHUNKSIZE%")
    xConf.dtstr = "%EXPERIMENT.TIMESTEP%"
    xConf.dtstrOce = "%EXPERIMENT.TIMESTEP_OCEAN%"
    xConf.CHECKPOINT_TIME_INTERVAL = "%EXPERIMENT.CHECKPOINT_INTERVAL%"
    xConf.nwp_timesteps_mult = {'dt_rad'  : "%EXPERIMENT.TIMESTEP_FACTORS.DT_RAD%",
                                'dt_conv' : "%EXPERIMENT.TIMESTEP_FACTORS.DT_CONV%",
                                'dt_ccov' : "%EXPERIMENT.TIMESTEP_FACTORS.DT_CCOV%",
                                'dt_sso'  : "%EXPERIMENT.TIMESTEP_FACTORS.DT_SSO%",
                                'dt_gwd'  : "%EXPERIMENT.TIMESTEP_FACTORS.DT_GWD%"}
    xConf.phy_timesteps = {'dt_rad'  : "%EXPERIMENT.TIMESTEP_PHY.RAD%",
                           'dt_conv' : "%EXPERIMENT.TIMESTEP_PHY.CONV%",
                           'dt_ccov' : "%EXPERIMENT.TIMESTEP_PHY.CCOV%",
                           'dt_sso'  : "%EXPERIMENT.TIMESTEP_PHY.SSO%",
                           'dt_gwd'  : "%EXPERIMENT.TIMESTEP_PHY.GWD%",
                           'dt_vdf'  : "%EXPERIMENT.TIMESTEP_PHY.VDF%",
                           'dt_mig'  : "%EXPERIMENT.TIMESTEP_PHY.MIG%",
                           'dt_two'  : "%EXPERIMENT.TIMESTEP_PHY.TWO%",
                           'dt_car'  : "%EXPERIMENT.TIMESTEP_PHY.CAR%",
                           'dt_art'  : "%EXPERIMENT.TIMESTEP_PHY.ART%"}
    xoConf = SimpleNamespace()
    xoConf.FILE_INTERVAL = "%EXPERIMENT.FILE_INTERVAL%"
    xoConf.FILE_INTERVAL_OCEAN = "%EXPERIMENT.FILE_INTERVAL_OCEAN%"
    xoConf.STEPS_PER_FILE = optionalint("%EXPERIMENT.STEPS_PER_FILE%", 0)
    xoConf.STEPS_PER_FILE_OCEAN = optionalint("%EXPERIMENT.STEPS_PER_FILE_OCEAN%", 0)

    use_jsbach = expconf.NAMELIST_USE_JSBACH
    use_ocean = expconf.NAMELIST_USE_OCEAN
    use_coupling = expconf.NAMELIST_USE_COUPLING

    logger.info("Autosubmit configuration loaded.")
    logger.debug(expconf)

    #-- nml_patches and nml_patches_sub are namespaces with the following structure:
    #-- (pathes in nml_patches will not overwrite existing values, while nml_patches_sub do)
    #-- master = [
    #--     {'groupa_nml':
    #--         {'parama': 'value',
    #--          'paramb': 'value'}
    #--     },
    #--     {'groupb_nml':
    #--         {'paramc': 'value',
    #--          'paramd': 'value'}
    #--     }
    #-- ]
    #-- atmo = [
    #--     {'groupc_nml':
    #--         {'parame': 'value',
    #--          'paramf': 'value'}
    #--     }
    #--     {'groupd_nml':
    #--         {'paramg': 'value',
    #--          'paramh': 'value'}
    #--     }
    #-- ]
    nml_patches = SimpleNamespace(master=[], atmo=[], jsbach=[], ocean=[])
    nml_patches_sub = SimpleNamespace(master=[], atmo=[], jsbach=[], ocean=[])

    #-- Create Patches
    logger.info("Creating namelist patches.")
    if expconf.REINIT:
        createPatchesReinit(nml_patches, expconf,
                            readFromAsList("%REINITIALIZATION.CONTINUE_VARIABLES.AERO%"),
                            readFromAsList("%REINITIALIZATION.CONTINUE_VARIABLES.CHEM%"),
                            readFromAsList("%REINITIALIZATION.CONTINUE_VARIABLES.MET%"),
                            "%CHUNK_END_DATE%")
    if expconf.NUDGING_INTERVAL > 0:
        createPatchesNudging(nml_patches, expconf)
    output_times = createPatchesTiming(nml_patches, expconf, xConf)
    createPatchesGrid(nml_patches, expconf)
    createPatchesOutput(nml_patches, expconf, xoConf, output_times, ocean=use_ocean)
    createPatchesChunks(nml_patches_sub, expconf, "%EXPERIMENT.NML_SUB_NOT_FIRST%", "%REINITIALIZATION.REINIT_SUBSTITUTES%")
    if use_ocean:
        createPatchesOcean(nml_patches,
                           optionalint("%EXPERIMENT.OCEAN_VERT_COR_TYPE%", 0),
                           "%EXPERIMENT.OCEAN_LEVELS%",
                           "%EXPERIMENT.INIT_OCEAN_FROM%")

    #-- Process namelists
    logger.info("Creating master namelist template.")
    nml_in_path, nml_out_path, inType, outType = getNamelistPaths(expconf.NAMELIST_MASTER_IN, expconf.NAMELIST_MASTER, expconf)
    master_template_path = Path(expconf.LOGDIR, "icon_master.template.namelist")
    shutil.copy(nml_in_path, master_template_path)
    logger.info("Processing atmosphere namelist.")
    nml_in_path, nml_out_path, inType, outType = getNamelistPaths(expconf.NAMELIST_ATMO_IN, expconf.NAMELIST_ATMO, expconf)
    processNamelist(nml_in_path, nml_out_path, inType, outType,
                    nml_patches.atmo, nml_patches_sub.atmo, expconf)
    if use_jsbach:
        logger.info("Processing jsbach (land) namelist.")
        nml_in_path, nml_out_path, inType, outType = getNamelistPaths(expconf.NAMELIST_JSBACH_IN, expconf.NAMELIST_JSBACH, expconf)
        processNamelist(nml_in_path, nml_out_path, inType, outType,
                        nml_patches.jsbach, nml_patches_sub.jsbach, expconf, jsbach=True)
    if use_ocean:
        logger.info("Processing ocean namelist.")
        nml_in_path, nml_out_path, inType, outType = getNamelistPaths(expconf.NAMELIST_OCEAN_IN, expconf.NAMELIST_OCEAN, expconf)
        processNamelist(nml_in_path, nml_out_path, inType, outType,
                        nml_patches.ocean, nml_patches_sub.ocean, expconf)
    if use_coupling:
        logger.info("Processing coupling expconf file.")
        #-- Currently, there are no options to be configured for the coupling namelist,
        #-- i.e. this only writes out the original namelist at the run dicrectory.
        nml_in_path, nml_out_path, inType, outType = getNamelistPaths(expconf.NAMELIST_COUPLING_IN, expconf.NAMELIST_COUPLING, expconf)
        processNamelist(nml_in_path, nml_out_path, inType, outType, [], [], expconf)

if __name__ == "__main__": main()
