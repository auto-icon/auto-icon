#!/usr/bin/bash

#-- > Run the ICON model. < --#

# Copyright 2023 - 2024 Karlsruhe Institute of Technology (KIT) and Ludwig-Maximilians-Universität München (LMU)
# SPDX-FileContributor: Andreas Baer
#
# SPDX-License-Identifier: BSD-3-Clause

set -e  #-- This will be set by autosubmit anyway and allows for proper shell syntax checking.

#-- ----- -------------------- ----- --#
#-- Check if the simulation has terminated successfully:
#-- Icon does not return a reasonable error code,
#-- but a successfull simulation creates a file called
#-- 'finish.status' with the content 'OK'
#-- Upon termination at a restart point, the status file
#-- will contain 'RESTART' instead.
#-- Arguments:
#--   Bool ('true'|'false'), if this is the last chunk
#-- Returns:
#--   1 if the simulation did not terminate successfully
#-- ----- -------------------- ----- --#
function check_status() {
    local STATUS
    STATUS=$(cat finish.status)
    if [ "${STATUS/# /}" = "OK" ] || [ "${STATUS/# /}" = "RESTART" ]; then return; fi
    return 1
}

#-- ----- -------------------- ----- --#
#-- Find the correct output directory.
#-- ----- -------------------- ----- --#
function find_output_directory() {
    local ROOTDIR_KIT="%DIRECTORIES.OUTDIR%"
    local ROOTDIR_LMU="%HPCROOTDIR%"
    local STARTDATE="%SDATE%"
    local MEMBER="%MEMBER%"
    #-- Find correct root outdir
    if   [ -d "$ROOTDIR_KIT" ]; then cd "$ROOTDIR_KIT"
    elif [ -d "$ROOTDIR_LMU" ]; then cd "$ROOTDIR_LMU" 
    else
        echo "ERROR [512]: No output directory exists."
        exit 1
    fi
    #-- Set 'ideal' member
    if [ -z "$MEMBER" ] || [ "$MEMBER" = None ]
    then
        MEMBER=ideal
    fi
    cd "$STARTDATE/$MEMBER"
}

#-- ----- Main script ----- --#
#-- Set up environment specific to the HPC system
INSTALLDIR="%ICON.INSTALLDIR%"
ENVMODULES="%CURRENT_LOGDIR%/envmodules.sh"
REINIT="%REINITIALIZATION.REINIT%"
REINIT="${REINIT,,}"
# shellcheck disable=SC1090
if [ -f "$ENVMODULES" ]; then source "$ENVMODULES"; fi

find_output_directory

#-- Write master namelist
nmlMasterArgs=(
    '--jobname'          '%JOBNAME%'
    '--sdate'            '%SDATE%'
    '--chunk_start_date' '%CHUNK_START_DATE%'
    '--chunksizeunit'    '%EXPERIMENT.CHUNKSIZEUNIT%'
    '--member'           '%MEMBER%'
    '--chunk'            '%CHUNK%'
    '--numchunks'        '%EXPERIMENT.NUMCHUNKS%'
    '--chunksize'        '%EXPERIMENT.CHUNKSIZE%'
    '--cpkintv'          '%EXPERIMENT.CHECKPOINT_INTERVAL%'
    '--ntasks'           "$SLURM_NTASKS"
)
IS_FIRST_CHUNK="%CHUNK_FIRST%"
if [ ! "$REINIT" = true ] && [ "${IS_FIRST_CHUNK,,}" = false ]
then
    nmlMasterArgs+=('--restart')
    jsbach_nml="%NAMELIST.JSBACH%"      # TODO: make a different check whether JSBACH is used or not
    if [ "$jsbach_nml" ]
    then
        nmlMasterArgs+=('--jsbach')
    fi
fi
'%AISHAREDIR%/%PYTHON_ENVIRONMENT.FOLDER_NAME%/bin/python3' '%PYTHON_ENVIRONMENT.MODULES_DIR%/namelist_utils.py' "${nmlMasterArgs[@]}"
if [ "$REINIT" = true ]
then
    NML_CHUNK="$(printf "%04d" "%CHUNK%")_icon_master.namelist"
    if [ -f "$NML_CHUNK" ]
    then
        ln -sf "$(printf "%04d" "%CHUNK%")_icon_master.namelist" icon_master.namelist
    fi
fi

rm -f finish.status

# shellcheck disable=SC2288
%ENVIRONMENT.RUN_CMD% "${INSTALLDIR}/bin/icon"

check_status

if [ "$REINIT" = true ]
then
    "%AISHAREDIR%/%PYTHON_ENVIRONMENT.FOLDER_NAME%/bin/python3" "%PYTHON_ENVIRONMENT.MODULES_DIR%/rename_reinit_files.py" --sdate "%SDATE%" --cdate "%CHUNK_END_DATE%" --member "%MEMBER%"
fi
