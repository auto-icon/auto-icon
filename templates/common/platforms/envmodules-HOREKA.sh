#!/usr/bin/bash

#-- > Set up HPC environment for Levante (prior to model run). < --#

# Copyright 2023 - 2024 Karlsruhe Institute of Technology (KIT) and Ludwig-Maximilians-Universität München (LMU)
# SPDX-FileContributor: Andreas Baer
#
# SPDX-License-Identifier: BSD-3-Clause

set -e  #-- This will be set by autosubmit anyway and allows for proper shell syntax checking.

export OMPI_MCA_coll="^ml,hcoll"
export OMPI_MCA_coll_hcoll_enable="0"
export HCOLL_ENABLE_MCAST_ALL="0"
export HCOLL_MAIN_IB=mlx5_0:1

MODULES="%ENVIRONMENT.MODULES.ECCODES%"
if [ "${MODULES}" ]; then module load ${MODULES}; fi
