#!/usr/bin/bash

#-- > Build the ICON model. < --#

# Copyright 2023 - 2024 Karlsruhe Institute of Technology (KIT) and Ludwig-Maximilians-Universität München (LMU)
# SPDX-FileContributor: Andreas Baer
#
# SPDX-License-Identifier: BSD-3-Clause

set -e  #-- This will be set by autosubmit anyway and allows for proper shell syntax checking.
MODULES="%ENVIRONMENT.MODULES.GIT%"
if [ "${MODULES}" ]; then module load ${MODULES}; fi

#-- ----- -------------------- ----- --#
#-- Check if ICON binary exists.
#-- Arguments:
#--   Prefix directory, i.e. check $1/bin/icon
#-- Outputs:
#--   Writes confirmation if exists to stdout
#-- Returns:
#--   0 if exists
#--   2 otherwise
#-- ----- -------------------- ----- --#
function check_for_binary() {
    local PREFIX=$1
    if [ -f "$PREFIX/bin/icon" ]
    then
        echo "Binary $PREFIX/bin/icon exists."
    else
        return 2
    fi
}

#-- ----- -------------------- ----- --#
#-- Check for the presence and emptiness of the install directory
#-- and write permissions.
#-- Arguments:
#--   Install directory to check
#-- Exits:
#--   1 on any error
#-- ----- -------------------- ----- --#
function check_install_directory() {
    local install_dir=$1
    if [ ! -d "$install_dir" ]
    then
        mkdir "$install_dir"
    else
        if [ -f "$install_dir" ]
        then
            echo "ERROR [417]: install directory '$install_dir' is a regular file not a directory!" >&2
            exit 1
        fi
        if [ ! -w "$install_dir" ]
        then
            echo "ERROR [417]: no write permissions in install directory '$install_dir', aborting." >&2
            exit 1
        fi
        REQUIRE_CLEAN_ICONDIR="%ICON.REQUIRE_CLEAN_ICONDIR%"
        if [ "${REQUIRE_CLEAN_ICONDIR,,}" = "true" ] && [ "$(ls -A "$install_dir")" ]
        then
            echo "ERROR [417]: install directory '$install_dir' is required to be clean, but is not! You can change the behaviour with ICON.REQUIRE_CLEAN_ICONDIR or ensure the directory is clean."
            exit 1
        fi
    fi
}

#-- ----- -------------------- ----- --#
#-- Clone and update the icon repository.
#-- Arguments:
#--   Repository directory
#-- Exits:
#--   1 on any (git related) error
#-- ----- -------------------- ----- --#
function pull_repository() {
    local repo_dir=$1
    local prev_dir
    prev_dir=$(pwd)

    local URL="https://gitlab.dkrz.de/%ICON.GROUP%/%ICON.SLUG%.git"
    local BRANCH="%ICON.BRANCH%"        #-- branch or tag to be cloned
    local COMMIT="%ICON.COMMIT%"        #-- commit id/rev/...; 'HEAD' if targeting the latest commit of the branch
    local DEPTH="%ICON.DEPTH%"          #-- Depth of the cloned repository
    local SUBMODULES_FULL="%ICON.SUBMODULES%"
    mapfile -t SUBMODULES < <(echo "$SUBMODULES_FULL" | sed -e 's/^\[//' -e 's/\]$//' -e 's/, /\n/g' | sed -e "s/^'//" -e"s/'$//")

    #-- Create or update repository
    if [ ! -d "$repo_dir" ] || [ ! "$(ls -A "$repo_dir")" ]
    then
        #-- Build directory does not yet exist or is empty.
        #-- Create all parent directories if not yet existing.
        mkdir -p "$repo_dir"
        git clone -n --depth "$DEPTH" --single-branch -b "$BRANCH" "$URL" "$repo_dir"
        cd "$repo_dir"
        if [ "$COMMIT" = CURRENT ]; then COMMIT=HEAD; fi
        git checkout "$COMMIT"
        git submodule init
        git submodule update
    else
        #-- Build directory does exist.
        cd "$repo_dir"
        if ! git status > /dev/null 2>&1
        then
            echo "ERROR [417]: repository directory does exist but is not a git repository!" >&2
            exit 1
        fi

        #-- If the branch is HEAD, i.e. if the latest version should be used:
        #-- update repository, otherwise use current state.
        if [ "$COMMIT" = HEAD ]
        then
            #-- Check for correctness of current branch
            if [ ! "$(git rev-parse --abbrev-ref HEAD)" = "$BRANCH" ] && ! git switch "$BRANCH"
            then
                {
                echo "ERROR [416]:"
                echo "  Current branch $(git rev-parse --abbrev-ref HEAD) does not equal desired branch $BRANCH and cannot be switched."
                echo "  This is probably due to a change in the desired branch in the auto-icon config and the checkout of a single branch."
                echo "  Check your repository."
                } >&2
                exit 1
            fi
            git pull --recurse-submodules
        elif [ "$COMMIT" = CURRENT ]
        then
            echo "Using the current state:"
            git status
        else
            #-- Detached HEAD state is expected.
            #-- Check if current commit is desired commit.
            if [ ! "$(git rev-parse $COMMIT)" = "$(git rev-parse HEAD)" ]
            then
                echo "ERROR [416]: current commit is not the desired commit! Check your repository at '$(pwd)'." >&2
                echo "             Alternatively, use option ICON.COMMIT: CURRENT to use the current state." >&2
                git status 1>&2
                exit 1
            fi
        fi
    fi
    if [ ! "$COMMIT" = CURRENT ]
    then
        #-- Update the submodules to the desired branches
        for SUBMODULE in "${SUBMODULES[@]}"
        do
            if [ -z "$SUBMODULE" ]; then continue; fi
            mapfile -d':' -t FIELDS < <(echo -n "$SUBMODULE")
            if [ "${#FIELDS[@]}" -ne 3 ]
            then
                echo "ERROR: malformed submodule description: '$SUBMODULE'." >&2
                return 1
            fi
            git submodule set-branch --branch "${FIELDS[1]}" "externals/${FIELDS[0]}"
            git submodule update --remote "externals/${FIELDS[0]}"
            if [ ! "${FIELDS[2]}" = CURRENT ] && [ ! "${FIELDS[2]}" = HEAD ]
            then
                cd "externals/${FIELDS[0]}"
                git checkout "${FIELDS[2]}"
                cd ../..
            fi
        done
    fi
    #-- Update submodules to the desired branch
    cd "$prev_dir"
}

#-- ----- -------------------- ----- --#
#-- Run the configuration script
#-- Arguments:
#--   Build directory
#--   Repository directory
#--   Install directory
#-- ----- -------------------- ----- --#
function run_configure() {
    local build_dir=$1
    local repo_dir=$2
    local install_dir=$3
    local config_file="$repo_dir/config/%ICON.CONFIG_FILE%"
    local CONFIG_OPTIONS=(%ICON.OPTIONS%)
    local prev_dir
    prev_dir=$(pwd)

    #-- If run as a batch job, slurm environment variables
    #-- can confuse the configure script, thus unset slurm variables.
    #-- Store the SLURM environment variables and unset them
    if env | grep SLURM >slurm_env.txt
    then
        unset "${!SLURM@}"
    fi

    #-- Run configure script
    cd "$build_dir"
    "$config_file" --prefix="$install_dir" "${CONFIG_OPTIONS[@]}"
    cd "$prev_dir"
    
    #-- The SLURM variables can now be restored with the following command:
    #-- (this is currently not done as it is not necessary)
    # . <(sed 's/^/export /' slurm_env.txt)
}

#-- ----- -------------------- ----- --#
#-- Run make.
#-- Arguments:
#--   Build directory
#-- ----- -------------------- ----- --#
function run_make() {
    local build_dir=$1
    local numprocs="%NUMPROC%"
    local prev_dir
    prev_dir=$(pwd)

    cd "$build_dir"
    make -j"$numprocs"
    cd "$prev_dir"
}

#-- ----- -------------------- ----- --#
#-- Install ICON to the install directory
#-- Arguments:
#--   Build directory
#--   Repository directory
#-- ----- -------------------- ----- --#
function run_install() {
    local build_dir=$1
    local repo_dir=$2
    "${repo_dir}/utils/move_to_prefix.sh" --builddir="$build_dir"
}


#-- ----- Main function ----- --#
function run_main() {
    #-- There are three different directories:
    #--   - repository        (%HPCROOTDIR%/icon-kit)
    #--   - build directory   (%HPCROOTDIR%/icon-build)
    #--   - install directory (%HPCROOTDIR%/icon)
    #-- All are in a permanent location. This allows incremental builds as in the ICON build process
    #-- absolute path names are used that break as soon as any of the three directories is changed or removed.
    local WORKDIR="%AISHAREDIR%"
    local INSTALLDIR="%ICON.INSTALLDIR%"
    local BUILDDIR="%ICON.BUILDDIR%"
    local REPODIR="%ICON.REPODIR%"

    local ENSURE_BUILD="%ICON.ENSURE_BUILD%"
    local REBUILD_FROM_SCRATCH="%ICON.REBUILD_FROM_SCRATCH%"

    mkdir -p "${WORKDIR}"
    #-- Check if the required directories are set.
    if [ -z "$INSTALLDIR" ] || [ -z "$BUILDDIR" ] || [ -z "$REPODIR" ]
    then
        echo "ERROR [411]: at least one of installdir 'ICON.INSTALLDIR=$INSTALLDIR', builddir 'ICON.BUILDDIR=$BUILDDIR' or repository directory 'ICON.REPODIR=$REPODIR' are empty!"
        exit 1
    fi

    #-- Check if rebuild from scratch is required
    #-- If so, all directories will be deleted, thus everything is done from scratch.
    if [ "${REBUILD_FROM_SCRATCH,,}" = true ]
    then
        #-- Set ENSURE_BUILD and delete all directories
        ENSURE_BUILD=true
        rm -rf "$INSTALLDIR" "$BUILDDIR" "$REPODIR"
    fi

    #-- ----- Main routine ----- --#
    if check_for_binary "$INSTALLDIR" && [ ! "${ENSURE_BUILD,,}" = true ]
    then
        echo "Nothing to be done."
        return
    else
        echo "Binary does not exist or re-running build is required. Do a build."
        check_install_directory "$INSTALLDIR"
        pull_repository "$REPODIR"
        mkdir -p "$BUILDDIR"
        if [ ! -f "$BUILDDIR/Makefile" ]
        then
            run_configure "$BUILDDIR" "$REPODIR" "$INSTALLDIR"
        fi

        run_make "$BUILDDIR"
        run_install "$BUILDDIR" "$REPODIR"
        #-- Check if building was successful.
        check_for_binary "$INSTALLDIR"
    fi
}

run_main
