#!/usr/bin/python3

#-- > Preparation of a member: working directory and input files < --#

# Copyright 2023 - 2024 Karlsruhe Institute of Technology (KIT) and Ludwig-Maximilians-Universität München (LMU)
# SPDX-FileContributor: Andreas Baer
#
# SPDX-License-Identifier: BSD-3-Clause

import sys
sys.path.append("%PYTHON_ENVIRONMENT.MODULES_DIR%")

import logging
from pathlib import Path
from experiment_config import FullExperimentConfig
from icon_file import ArtFile, IconFile, JsbachFile, MiscFile

import icon_file_handler as ifh
from python_utils import readFromAsList, sdate2date


def createDomains(expconf: FullExperimentConfig):
    """
    Create the list of file to be linked to the working directory.
    :param expconf: full experiment configuration
    """
    logger = logging.getLogger(__name__)
    logger.setLevel(expconf.LOGLEVEL)
    expconf.check(level="date")
    #-- Domain files
    fn_domains = Path(expconf.WORKFLOWDIR, expconf.SDATE, "domains.pkl")
    grid_info_file = (expconf.LOGDIR + "/expconf.yml")
    logger.info(f"Reading grid info file {grid_info_file!r}.")
    domains = ifh.createDomainList(grid_info_file, date_str=expconf.SDATE, loglvl=expconf.LOGLEVEL)
    #-- Evaluate patterns for domain files
    domainFiles = []
    for domain in domains:
        domain.evalPatterns()
        for file in domain.getFilesList():
            foundFile = file.findFile()
            if isinstance(foundFile, list):
                for newFile in foundFile[1:]:
                    domain.addFileHandle(newFile)
                domainFiles.extend(foundFile)
            else:
                if foundFile is not file:
                    logger.warning("Processed file is not the same as the triggering file. Probably, something went wrong!")
                domainFiles.append(foundFile)
    ifh.saveDomains(fn_domains, domains)
    logger.info(f"Using {len(domainFiles)} domain input files.")

def createExtraFiles(expconf: FullExperimentConfig):
    """
    Create the list of ExtraFiles to be linked to the working directory.
    :param expconf: full experiment configuration
    """
    logger = logging.getLogger(__name__)
    logger.setLevel(expconf.LOGLEVEL)
    expconf.check(level="date")
    date = sdate2date(expconf.SDATE)
    #-- Other files
    extraFiles = []
    for fn in expconf.MISC_FILES:
        extraFiles.append(MiscFile(fullstr=fn, date=date))
    for fn in expconf.ICON_FILES:
        extraFiles.append(IconFile(fullstr=fn, date=date))
    for fn in expconf.ART_FILES:
        extraFiles.append(ArtFile(fullstr=fn, date=date))
    #-- Add additional files required for some cases.
    if expconf.NUDGING_INTERVAL > 0:
        extraFiles.append(IconFile(fullstr='dict.latbc', date=date))
    #-- Set sources and names and find files
    for f in extraFiles:
        f.evalPattern2Source()
        f.evalPattern2Name()
    extraFiles = findFiles(extraFiles)
    logger.info(f"Using {len(extraFiles)} other input files.")
    fn_files = Path(expconf.WORKFLOWDIR, expconf.SDATE, "extraFiles.pkl")
    ifh.saveFiles(fn_files, extraFiles)

def createJsbachFiles(expconf: FullExperimentConfig):
    """
    Create the list of JsbachFiles to be linked to the working directory.
    :param expconf: full experiment configuration
    """
    logger = logging.getLogger(__name__)
    logger.setLevel(expconf.LOGLEVEL)
    if len("%JSBACH.YEAR%") == 0: return
    jsbYear = int("%JSBACH.YEAR%")
    jsbSpinup = "%JSBACH.SOIL_SPINUP%"
    jsbFilesIc = readFromAsList("%JSBACH.FILES.IC%")
    jsbFilesBc = readFromAsList("%JSBACH.FILES.BC%")
    #-- Jsbach files
    jsbachFiles = []
    for kind in jsbFilesBc:
        jsbachFiles.append(JsbachFile(kind=kind, ct='bc', year=jsbYear))
    for kind in jsbFilesIc:
        if kind == 'soil':
            jsbachFiles.append(JsbachFile(kind=kind, ct='ic', year=jsbYear, jsbss=jsbSpinup))
        else:
            jsbachFiles.append(JsbachFile(kind=kind, ct='ic', year=jsbYear))
    for f in jsbachFiles:
        f.evalPattern2Source()
        f.evalPattern2Name()
    jsbachFiles = findFiles(jsbachFiles)
    logger.info(f"Using {len(jsbachFiles)} JSBACH input files.")
    fn_files = Path(expconf.WORKFLOWDIR, "jsbachFiles.pkl")
    ifh.saveFiles(fn_files, jsbachFiles)

def findFiles(filesList: list) -> list:
    """ Simple wrapper to loop over all files, call findFile and return the updated list. """
    updatedFiles = []
    for file in filesList:
        foundFile = file.findFile()
        if isinstance(foundFile, list):
            updatedFiles.extend(foundFile)
        else:
            if foundFile is not file:
                logger.warning("Processed file is not the same as the triggering file. Probably, something went wrong!")
            updatedFiles.append(foundFile)
    return updatedFiles

if __name__ == "__main__":
    logger = logging.getLogger(__name__)
    expconf = FullExperimentConfig()
    logging.basicConfig(level=expconf.LOGLEVEL)
    logger.setLevel(expconf.LOGLEVEL)
    expconf.SDATE = "%SDATE%"
    expconf.check(level="date")

    workflowdir = Path(expconf.WORKFLOWDIR, expconf.SDATE)
    workflowdir.mkdir(parents=True, exist_ok=True)
    
    #-- Create domain/file lists and save them.
    createDomains(expconf)
    createExtraFiles(expconf)
    createJsbachFiles(expconf)
