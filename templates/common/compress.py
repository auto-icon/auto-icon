# Copyright 2023 - 2024 Karlsruhe Institute of Technology (KIT) and Ludwig-Maximilians-Universität München (LMU)
# SPDX-FileContributor: Oriol Tinto Prims, Fabian Jakub
#
# SPDX-License-Identifier: BSD-3-Clause

import glob
import logging

# Set logging level to info.
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger("compress")
logger.setLevel(logging.INFO)


def compress_outputs():
    import os
    from pathlib import Path
    import enstools.compression.api

    # Move the outputs to the proper destination

    # Get some variables provided by autosubmit.
    WORKDIR = Path("%HPCROOTDIR%")
    STARTDATE = "%SDATE%"
    MEMBER = "%MEMBER%"
    output_file_names = "%SIMULATION.OUTPUT_FILE_NAMES%"

    # Define rundir
    RUNDIR = WORKDIR / STARTDATE / MEMBER

    # Get a list of file names:
    output_file_names = [RUNDIR / f for f in output_file_names.split(" ") if f.strip()]

    logger.info(f"File patterns: {output_file_names}")
    output_files = []
    for file_pattern in output_file_names:
        output_files.extend(glob.glob(file_pattern.as_posix()))


    output_files = [Path(f) for f in output_files]
    logger.info(f"Output files: {output_files}")

    if not output_files:
        logger.warning("The list of files is empty!")

    # Define output dir and create it in case it doesn't exist
    OUTPUT_DIR = WORKDIR / "output" / STARTDATE / MEMBER

    if not OUTPUT_DIR.exists():
        os.makedirs(OUTPUT_DIR.as_posix())

    # Copy the output files
    for source_file_path in output_files:
        destination_file_path = OUTPUT_DIR / source_file_path.name

        logger.info(f"Copying {source_file_path.name!r}")
        enstools.compression.api.compress(source_file_path, output=destination_file_path, compression="lossless")

        # Remove source files
        if destination_file_path.exists():
            logger.info(f"{source_file_path.name!r} copied to {destination_file_path.as_posix()!r}.")
            logger.info(f"Removing {source_file_path.as_posix()!r}")
            source_file_path.unlink()


if __name__ == "__main__":
    compress_outputs()
