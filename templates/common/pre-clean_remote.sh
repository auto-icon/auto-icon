#!/usr/bin/bash

#-- > Clean up all experiment data. < --#

# Copyright 2023 - 2024 Karlsruhe Institute of Technology (KIT) and Ludwig-Maximilians-Universität München (LMU)
# SPDX-FileContributor: Andreas Baer
#
# SPDX-License-Identifier: BSD-3-Clause

set -e  #-- This will be set by autosubmit anyway and allows for proper shell syntax checking.

LOGDIR="%CURRENT_LOGDIR%"
LOG="%JOBNAME%.cmd"
shopt -s extglob
if [ -d "$LOGDIR" ]
then
    cd "$LOGDIR"
    rm -vr !("$LOG"*)
else
    echo "LOGDIR is already empty."
fi
