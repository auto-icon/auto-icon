#!/usr/bin/bash

#-- > Build a python (v)env. < --#

# Copyright 2023 - 2024 Karlsruhe Institute of Technology (KIT) and Ludwig-Maximilians-Universität München (LMU)
# SPDX-FileContributor: Andreas Baer
#
# SPDX-License-Identifier: BSD-3-Clause

set -e  #-- This will be set by autosubmit anyway and allows for proper shell syntax checking.
MODULES="%ENVIRONMENT.MODULES.PYTHON%"
if [ "${MODULES}" ]; then module load ${MODULES}; fi

#-- ----- -------------------- ----- --#
#-- Compare two version numbers
#-- Arguments:
#--   Version number v1
#--   Version number v2
#-- Outputs:
#--   'equal'   if v1 == v2
#--   'smaller' if v1 <  v2
#--   'larger'  if v1 >  v2
#-- ----- -------------------- ----- --#
function compare_versions() {
    local v1 v2
    v1=$1
    v2=$2
    #-- Short cut everything on equality
    if [ "$v1" = "$v2" ]
    then
        echo -n equal
        return
    fi

    #-- Generate the arrays of version numbers now
    mapfile -d"." -t v1a < <(echo -n "$v1")
    mapfile -d"." -t v2a < <(echo -n "$v2")
    #-- Loop over all array entries of the first array
    for i in "${!v1a[@]}"
    do
        local c1 c2
        c1=${v1a[$i]}
        c2=${v2a[$i]}
        #-- Check if second version number is empty
        #-- (treat this as smaller than any non-zero entry)
        if [ -z "$c2" ]
        then
            echo -n larger
            return
        fi
        if [ "$c1" -lt "$c2" ]
        then
            echo -n smaller
            return
        elif [ "$c1" -gt "$c2" ]
        then
            echo -n larger
            return
        fi
    done
    #-- All array entries of the first array are present in the second one.
    #-- They are not equal, i.e. second has a further subversion number,
    #-- i.e. first entry is smaller than the second one.
    echo -n smaller
}

#-- ----- -------------------- ----- --#
#-- Install a specific python version and create a virtual env.
#-- Arguments:
#--   required python version (minimal)
#-- Exits:
#--   1 if the required version is not available
#-- ----- -------------------- ----- --#
function install_python() {
    local REQUIRED_VERSION=$1
    local PYTHONBIN="python3"
    local PYTHON_ENVIRONMENT_FOLDER="%PYTHON_ENVIRONMENT.FOLDER_NAME%"
    local CUSTOM_MODULES_DIR="%PYTHON_ENVIRONMENT.MODULES_DIR%"
    local MODULESINITFILE="%ENVIRONMENT.MODULESINITFILE%"
    local INSTALLED_VERSION
    INSTALLED_VERSION=$("$PYTHONBIN" --version)
    INSTALLED_VERSION=${INSTALLED_VERSION#Python }
    #-- Find required version
    if [ "$(compare_versions "$INSTALLED_VERSION" "$REQUIRED_VERSION")" = "smaller" ]
    then
        #-- Only do something if the installed version is smaller than the required one. Otherwise everything is fine.
        MAJOR_MINOR=$(echo "$REQUIRED_VERSION" | awk -F. '{ print $1"."$2 }' )
        #-- Try out 'python3.x' for the required python version.
        TEST_VERSION=$("python${MAJOR_MINOR}" --version)
        if [ -z "$TEST_VERSION" ]
        then
            echo "ERROR [414]: python version $REQUIRED_VERSION not available. Available is $INSTALLED_VERSION and python${MAJOR_MINOR} not available."
            exit 1
        fi
        TEST_VERSION=${TEST_VERSION#Python }

        #-- Check that both versions are the same or the newly loaded one is larger
        if [ "$(compare_versions "$TEST_VERSION" "$REQUIRED_VERSION")" = "smaller" ]
        then
            echo "ERROR [414]: python version $REQUIRED_VERSION not available. Available with python${MAJOR_MINOR} is only $TEST_VERSION."
            exit 1
        else
            PYTHONBIN="python${MAJOR_MINOR}"
        fi
    fi

    #-- Create, load and link virtual environment
    if [ ! -d "${PYTHON_ENVIRONMENT_FOLDER}" ]; then
        "$PYTHONBIN" -m venv --prompt AS "${PYTHON_ENVIRONMENT_FOLDER}"
    fi
    source "${PYTHON_ENVIRONMENT_FOLDER}/bin/activate"

    mkdir -p "$CUSTOM_MODULES_DIR"

    #-- Provide environment modules for python.
    if [ "$MODULESINITFILE" ] && [ -f "$MODULESINITFILE" ]
    then
        ln -sf "$MODULESINITFILE" "$CUSTOM_MODULES_DIR/environment_modules.py"
    fi
}

#-- ----- -------------------- ----- --#
#-- Install specific modules in the venv.
#-- Outputs:
#--   Info on the requirements to be installed
#-- ----- -------------------- ----- --#
function install_modules() {
    #-- Install the requirements via pip
    #-- Extract all requirements to a bash list and sort with unique to overcome a repetition of the requirements.
    REQUIREMENTS="%PYTHON_ENVIRONMENT.REQUIREMENTS%"
    mapfile -t UNIQUE_REQUIREMENTS < <(echo "$REQUIREMENTS" | grep -o "'[^']*'" | tr -d "'" | sort -u)
    #-- Print requirements for info.
    echo "${UNIQUE_REQUIREMENTS[@]}"

    #-- Install requirements.
    python3 -m pip install "${UNIQUE_REQUIREMENTS[@]}"
}


#-- ----- Main script ----- --#
#-- Get some variables provided by autosubmit.
WORKDIR="%AISHAREDIR%"
PYTHON_VERSION="%PYTHON_ENVIRONMENT.PYTHON_VERSION%"
PYTHON_VERSION=${PYTHON_VERSION%:}

mkdir -p "${WORKDIR}"
cd "${WORKDIR}"
install_python "$PYTHON_VERSION"
install_modules
