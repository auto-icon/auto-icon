#!/usr/bin/bash

#-- > Get the SAMOA post-processing tool. < --#

# Copyright 2023 - 2024 Karlsruhe Institute of Technology (KIT) and Ludwig-Maximilians-Universität München (LMU)
# SPDX-FileContributor: Andreas Baer
#
# SPDX-License-Identifier: BSD-3-Clause

set -e  #-- This will be set by autosubmit anyway and allows for proper shell syntax checking.
MODULES="%ENVIRONMENT.MODULES.GIT%"
if [ "${MODULES}" ]; then module load ${MODULES}; fi

URL="%SAMOA.URL%"
BRANCH="%SAMOA.BRANCH%"
WORKDIR="%AISHAREDIR%"

mkdir -p "${WORKDIR}"
cd "$WORKDIR"

#-- If the directory does not yet exist, clone the repo first
if [ ! -d SAMOA ]
then
    git clone "$URL"
fi

#-- Check if SAMOA is properly pulled
#-- Check out the correct branch
cd SAMOA
if ! git switch "$BRANCH"
then
    echo "ERROR [416]: branch '$BRANCH' cannot be checked out!" >&2
    exit 2
fi
cd ..
