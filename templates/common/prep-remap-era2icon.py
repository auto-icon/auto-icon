#!/usr/bin/python3

#-- > Remap ERA5 input data to the ICON grid using DWD_ICON_TOOLS. < --#

# Copyright 2023 - 2024 Karlsruhe Institute of Technology (KIT) and Ludwig-Maximilians-Universität München (LMU)
# SPDX-FileContributor: Andreas Baer
#
# SPDX-License-Identifier: BSD-3-Clause

import sys
sys.path.append("%PYTHON_ENVIRONMENT.MODULES_DIR%")

import logging
import os
from subprocess import run
from typing import Union
import f90nml
from pathlib import Path
from datetime import datetime, timedelta

from icon_file import GridFile, Era5File, IntermediateFile
import icon_file_handler as ifh
from experiment_config import FullExperimentConfig
from icon_variable import initVariables
import envmodules_dwd_icon_tools as envmodules
from environment_modules import module as ml
from python_utils import sdate2date


def collectVars(cFile: IntermediateFile, ndims: int, inputVarlist: list, iconVariables: dict, date: datetime):
    """
    Collect all (single variable) raw files and merge them into one raw file
    for atmosphere or surface data.
    :param cFile: IntermediateFile where to collect the data
    :param ndims: number of dimensions to collect the data for (2: surface, 3: atmosphere)
    :param inputVarlist: variable list to get
    :param iconVariables: dict with the IconVariables
    :param date: datetime at which to get the variables
    """
    logger = logging.getLogger(__name__)
    logger.warning("This routine is currently specific to the Levante HPC system!")
    #-- Initialization
    DATE = f"{date.year:04d}-{date.month:02d}-{date.day:02d}"
    HOUR = f"{date.hour:02d}"
    # TODO: make flexible
    PREFIX = "/pool/data/ERA5"
    FAMILY = "E5"
    TYPE   = "an"
    TYPEID = "00"
    TRES   = "1H"
    if ndims == 2: LEVEL = "sf"
    else:          LEVEL = "ml"
    #-- Variables
    #-- Varlists contain all variable codes (GRIB1) to be collected.
    varlist = []
    varlistInv = []
    for key in inputVarlist:
        if key not in iconVariables:
            logger.error("Available variables:")
            logger.error(iconVariables.keys())
            raise ValueError(f"[413] No description available for variable {key!r}.")
        var = iconVariables[key]
        if var.NDIMS == ndims:
            codes = var.GRIB1
            if var.INVARIANT: l = varlistInv
            else:             l = varlist
            if isinstance(codes, list):
                for code in codes:
                    if code not in l: l.append(code)
            else:
                if codes not in l: l.append(codes)
    logger.debug("Time varying codes:")
    logger.debug(varlist)
    logger.debug("Time invariant codes:")
    logger.debug(varlistInv)
    if len(varlist) == 0 and len(varlistInv) == 0:
        logger.info("No variables required for this file, skipping!")
        return
    
    #-- Paths to single-variable raw files
    pathlist = []
    pathlistInv = []
    for code in varlist:
        PARAM = f"{code:03d}"
        pathlist.append(Path(
            PREFIX, FAMILY, LEVEL, TYPE, TRES, PARAM,
            f"{FAMILY}{LEVEL}{TYPEID}_{TRES}_{DATE}_{PARAM}.grb"
        ))
    DATE = "INVARIANT"
    TRES = "IV"
    for code in varlistInv:
        PARAM = f"{code:03d}"
        pathlistInv.append(Path(
            PREFIX, FAMILY, LEVEL, TYPE, TRES, PARAM,
            f"{FAMILY}{LEVEL}{TYPEID}_{TRES}_{DATE}_{PARAM}.grb"
        ))
    #-- Merge and convert
    if ndims == 2:
        cmd = ['cdo', '-O', 'setgridtype,regular', '-merge']
        for path in pathlist:
            cmd.extend([f'-selhour,{HOUR}', str(path)])
        cmd.extend([ str(path) for path in pathlistInv ])
        cmd.append(cFile.getFilepath())
    else:
        cmd = ['cdo', '-O', 'sp2gpl', '-setgridtype,regular', '-merge']
        if len(pathlistInv) > 0:
            raise NotImplementedError("[900] Invariant atmospheric quantities are not supported.")
        for path in pathlist:
            cmd.extend([f'-selhour,{HOUR}', str(path)])
        cmd.append(cFile.getFilepath())
    logger.info(' '.join(cmd))
    run(cmd, check=True)

def createFieldsNamelist(fnFields: Union[str, Path], ndims: int, inputVarlist: list, iconVariables: dict, intp_method: int):
    """ Create the fields namelist for remapping. """
    logger = logging.getLogger(__name__)
    #-- Creating the namelist
    fields = []     # list with the contents of each 'input_field_nml' group
    for key in inputVarlist:
        if key not in iconVariables:
            logger.error("Available variables:")
            logger.error(iconVariables.keys())
            raise ValueError(f"[413] Variable {key!r} not found in list of variables.")
        var = iconVariables[key]
        if var.NDIMS != ndims: continue
        if ndims == 2: inputname = f"var{var.GRIB1:d}"
        else:          inputname = var.ERA5
        f = {"inputname"   : inputname,
             "outputname"  : var.NAME,
             "code"        : var.GRIB1,
             "intp_method" : intp_method
             }
        if var.USE_LSM:
            f["var_in_mask"] = "LSM"
            f["code_in_mask"] = 172
            f["in_mask_threshold"] = 0.5
            f["in_mask_below"] = True
        fields.append(f)
    #-- Write the namelist
    nml = f90nml.Namelist()
    for field in fields:
        nml.add_cogroup("input_field_nml", field)
    f90nml.write(nml, nml_path=fnFields, force=True)
    logger.info(f"Fields namelist '{fnFields}' written.")

def remap(gridFile: GridFile,
          eraFile: IntermediateFile,
          rawFile: IntermediateFile,
          ndims: int,
          inputVarlist: list,
          iconVariables: dict,
          expconf: FullExperimentConfig):
    """ Create namelists and do the remapping. """
    logger = logging.getLogger(__name__)
    expconf.check(level="exp", check_jobname=True)
    nmlMain   = ("prep-remap_"
                    + expconf.JOBNAME.removeprefix(expconf.EXPID + "_") )
    nmlFields = ("prep-remap-fields_"
                    + expconf.JOBNAME.removeprefix(expconf.EXPID + "_") )
    #-- Main namelist
    patches = {"remap_nml" :
               {"in_grid_filename"  : str(rawFile.filepath),
                "in_filename"       : str(rawFile.filepath),
                "out_grid_filename" : str(gridFile.filepath),
                "out_filename"      : eraFile.getFilename()}}
    f90nml.patch(Path(expconf.LOGDIR, nmlMain), patches, nmlMain)
    logger.debug(f"Main namelist: {nmlMain!r}")
    #-- Fields namelist
    createFieldsNamelist(nmlFields, ndims, inputVarlist, iconVariables, expconf.INPUT_INTP_METHOD)
    logger.debug(f"Fields namelist: {nmlFields!r}")

    #-- Prepare environment and command
    cmd = envmodules.cmd.copy()
    os.environ.update(envmodules.env)
    logger.info("Running command:")
    cmd.extend([f"{expconf.ICONTOOLS_DIR}/bin/iconremap",
                "--remap_nml", nmlMain,
                "--input_field_nml", nmlFields])
    logger.info(cmd)
    run(cmd, check=True)


def main():
    logger = logging.getLogger(__name__)
    expconf = FullExperimentConfig()
    logging.basicConfig(level=expconf.LOGLEVEL)
    logger.setLevel(expconf.LOGLEVEL)
    expconf.JOBNAME = "%JOBNAME%"
    expconf.SDATE = "%SDATE%"
    expconf.CHUNKDATE = "%CHUNK_START_DATE%"
    expconf.check(level="chunk")
    REQUIRE_REMAPPING = ("%SIMULATION.REQUIRE_REMAPPING%".upper() == 'TRUE')

    workdir = Path(expconf.INDIR, "prep-remap-era2icon")
    workdir.mkdir(parents=True, exist_ok=True)
    os.chdir(workdir)
    logger.debug(f"Changed to directory '{os.getcwd()}'")

    #-- Prepare variables
    fnVars = Path(expconf.LOGDIR, "variables.yml")
    iconVariables = initVariables(fnVars=fnVars)
    inputVarlist = expconf.INPUT_VARIABLES
    logger.info(f"{len(iconVariables)} variable descriptions read from file '{fnVars}'.")

    #-- Prepare files
    datelist = [sdate2date(expconf.CHUNKDATE)]
    if expconf.NUDGING_INTERVAL > 0:
        interval = timedelta(seconds=expconf.NUDGING_INTERVAL)
        d0 = timedelta()
        if (interval % timedelta(hours=1)) != d0:
            #-- Interval is no multiple of 1 hour.
            raise RuntimeError("[411] For automatic ERA5 remapping with nudging, only full hours are supported.")
        if interval <= d0:
            raise RuntimeError("[411] Time interval for nudging must be positive!")
        cdate = datelist[0]
        enddate = sdate2date("%CHUNK_END_DATE%")
        while cdate <= enddate:
            datelist.append(cdate)
            cdate = cdate + interval
    for n, date in enumerate(datelist):
        fn_remap = Path(expconf.WORKFLOWDIR, expconf.SDATE, f"remapFiles_{date.strftime(expconf.DATEFORMAT_ICON)}.pkl")
        gridFile, eraFile, atmEraFile, sfcEraFile, atmRawFile, sfcRawFile = ifh.loadFiles(fn_remap)
        logger.debug(f"Grid file: {gridFile!r}")
        logger.debug(f"Era5 file: {eraFile!r}")
        logger.debug(f"Atm remapped file: {atmEraFile!r}")
        logger.debug(f"Sfc remapped file: {sfcEraFile!r}")
        logger.debug(f"Atm raw file: {atmRawFile!r}")
        logger.debug(f"Sfc raw file: {sfcRawFile!r}")
        if (not isinstance(gridFile, GridFile) or
            not isinstance(eraFile, Era5File) or
            not isinstance(atmEraFile, IntermediateFile) or
            not isinstance(sfcEraFile, IntermediateFile) or
            not isinstance(atmRawFile, IntermediateFile) or
            not isinstance(sfcRawFile, IntermediateFile)):
            raise TypeError("[510] File types are incorrect.")
        
        #-- Remapping
        if not eraFile.exists() or REQUIRE_REMAPPING:
            collectVars(cFile=atmRawFile, ndims=3, inputVarlist=inputVarlist, iconVariables=iconVariables, date=date)
            collectVars(cFile=sfcRawFile, ndims=2, inputVarlist=inputVarlist, iconVariables=iconVariables, date=date)
            if atmRawFile.exists():
                remap(gridFile=gridFile, eraFile=atmEraFile, rawFile=atmRawFile, ndims=3, inputVarlist=inputVarlist, iconVariables=iconVariables, expconf=expconf)
            if sfcRawFile.exists():
                remap(gridFile=gridFile, eraFile=sfcEraFile, rawFile=sfcRawFile, ndims=2, inputVarlist=inputVarlist, iconVariables=iconVariables, expconf=expconf)
            if atmEraFile.exists() and sfcEraFile.exists():
                #-- Do the merge if both files exist.
                cmd = ['cdo', '-O', 'merge',
                    atmEraFile.getFilepath(),
                    sfcEraFile.getFilepath(),
                    eraFile.getTarget()]
                run(cmd, check=True)
            elif atmEraFile.exists():
                if atmEraFile.filepath == None:
                    raise RuntimeError("[510] File exists but path is not set!")
                atmEraFile.filepath.replace(eraFile.getTarget())
            elif sfcEraFile.exists():
                if sfcEraFile.filepath == None:
                    raise RuntimeError("[510] File exists but path is not set!")
                sfcEraFile.filepath.replace(eraFile.getTarget())
            else:
                raise ValueError(f"[413] No input or nudging variables required for file {eraFile.getFilepath()!r}, check simulation.yml.")
        else:
            logger.info("Remapped ERA5 file is present and remapping not required. Nothing to be done.")

if __name__ == "__main__":
    cdoModule = "%ENVIRONMENT.MODULES.CDO%"
    eccodesModule = "%ENVIRONMENT.MODULES.ECCODES%"
    res = ml('load', cdoModule, eccodesModule)
    if not res: raise RuntimeError(f"[415] Module file {cdoModule!r} or {eccodesModule!r} could not be loaded.")
    main()
