# Copyright 2023 - 2024 Karlsruhe Institute of Technology (KIT) and Ludwig-Maximilians-Universität München (LMU)
# SPDX-FileContributor: Oriol Tinto Prims, Fabian Jakub
#
# SPDX-License-Identifier: BSD-3-Clause

# Synchronize the local namelists with the remote directory

# Get some variables provided by autosubmit.
WORKDIR=%HPCROOTDIR%
HPCUSER=%HPCUSER%
HPCHOST=%HPCHOST%

# Define local and remote namelists folders
REMOTE_WORKDIR=${WORKDIR}/
PROJ_FOLDER="%PROJDIR%"

# Transfer the project
if [[ "$HPCHOST" == "localhost" ]]; then
  mkdir -p ${REMOTE_WORKDIR}/proj
  rsync -v -u -r --no-relative ${PROJ_FOLDER}/ ${REMOTE_WORKDIR}/proj
else
  ssh ${HPCUSER}@${HPCHOST} mkdir -p ${REMOTE_WORKDIR}/proj
  rsync -v -u -r --no-relative ${PROJ_FOLDER}/ ${HPCUSER}@${HPCHOST}:${REMOTE_WORKDIR}/proj
fi
