#!/usr/bin/python3

#-- > Check if remapping of ERA2ICON is necessary. < --#

# Copyright 2023 - 2024 Karlsruhe Institute of Technology (KIT) and Ludwig-Maximilians-Universität München (LMU)
# SPDX-FileContributor: Andreas Baer
#
# SPDX-License-Identifier: BSD-3-Clause

import sys
sys.path.append("%PYTHON_ENVIRONMENT.MODULES_DIR%")

import logging
import os
from pathlib import Path
from datetime import datetime, timedelta

from icon_file import GridFile, Era5File, IntermediateFile, MiscFile
import icon_file_handler as ifh
from experiment_config import FullExperimentConfig
from python_utils import sdate2date


def prepareFiles(multiple_era_files: bool, date: datetime, expconf: FullExperimentConfig):
    """ Get the file names / objects for all required files. """
    logger = logging.getLogger(__name__)
    expconf.check(level="date")
    # TODO: generalize names here
    atmRawFile = IntermediateFile(filetype=2, date=date)
    atmRawFile.setPattern("ERA5_{SDATE}-ml.{FMT:s}")
    atmRawFile.evalPattern2Name()
    sfcRawFile = IntermediateFile(filetype=2, date=date)
    sfcRawFile.setPattern("ERA5_{SDATE}-sf.{FMT:s}")
    sfcRawFile.evalPattern2Name()
    atmEraFile = IntermediateFile(filetype=expconf.FILETYPE, date=date)
    atmEraFile.setPattern("ERA5_{SDATE}-ml-remapped.{FMT:s}")
    atmEraFile.evalPattern2Name()
    sfcEraFile = IntermediateFile(filetype=expconf.FILETYPE, date=date)
    sfcEraFile.setPattern("ERA5_{SDATE}-sf-remapped.{FMT:s}")
    sfcEraFile.evalPattern2Name()

    #-- Load domains and corresponding files
    fn_domains = Path(expconf.WORKFLOWDIR, expconf.SDATE, "domains.pkl")
    domains = ifh.loadDomains(fn_domains)
    #-- Select correct domain
    if len(domains) == 0:
        raise RuntimeError("[412] No domains were initialized. Check the PRE_FIND_FILES job!")
    if domains[0].type() == 'R':
        domain = domains[1]
    else:
        domain = domains[0]
    #-- Get grid and ERA5 file
    if "ERA5" not in domain.fileTypes:
        raise RuntimeError(f"[414] Domain #{domain.index()} should contain an ERA5 file but does not.")
    gridFile = domain.getGrid()
    if multiple_era_files:
        eraFile = domain.getFile("ERA5", date=date)
        if eraFile == None:
            logger.info(f"Creating a new ERA5 file for date {date}.")
            eraFile = domain.addFile("ERA5")
            if not isinstance(eraFile, Era5File):
                raise TypeError(f"[510] ERA5 file is not of type Era5File but {type(eraFile)}.")
            eraFile.date = date
            eraFile.evalPattern2Source()
            eraFile.evalPattern2Name()
            eraFile.skipLink = True
    else:
        eraFile = domain.getFile("ERA5")
    if not isinstance(eraFile, Era5File): raise TypeError("[511] ERA5 file is not properly associated!")
    # TODO: what about multiple domains that all use ERA5 files?
    #-- Find files and create links.
    for file in [gridFile, eraFile]:
        if file.targetIsSet:
            foundFile = file.findFile()
            if (isinstance(foundFile, list) and len(foundFile) == 2
                and foundFile[0].kind == 'GRID' and foundFile[1].kind == 'GRFINFO'):
                foundFile = foundFile[0]
            if isinstance(foundFile, list) or foundFile is not file:
                logger.error("Triggering file")
                logger.error(file)
                logger.error("Processed file")
                logger.error(foundFile)
                raise RuntimeError("[510] Processed file is not the same as the triggering file. Something went wrong!")
        else:
            raise RuntimeError(f"[512] Target for file {file!r} is not yet set!")
        logger.info(file)
    if gridFile.exists(): gridFile.createLink(replace=False)
    else: raise FileNotFoundError(f"[414] {type(gridFile).__name__} not found!")
    if multiple_era_files:
        #-- Running on chunk level, i.e. one job per chunk.
        #-- Files will all be collected in intermediate directory and this will be linked.
        addIntermediatePath(eraFile=eraFile, expconf=expconf)
    ifh.saveDomains(fno=fn_domains, domains=domains)
    return (gridFile, eraFile, atmEraFile, sfcEraFile, atmRawFile, sfcRawFile)

def addIntermediatePath(eraFile: Era5File, expconf: FullExperimentConfig):
    """
    Add an intermediate directory path to the era file and update the file and domain lists.
    :param eraFile: Era5File to update
    :param domains: already loaded list of domains
    :param expconf: full experiment configuration
    """
    logger = logging.getLogger(__name__)
    logger.info("Adding an intermediate directory to the Era5File.")
    expconf.check(level="date")
    #-- Update era file
    intermediatePath = "ERA5-input"
    if eraFile.getTarget().parent.name != intermediatePath:
        eraFile.setTarget(eraFile.getTarget().parent / intermediatePath / eraFile.getTarget().name)
    filepath = eraFile.filepath
    if filepath == None: raise ValueError(f"[512] Name of Era5File {eraFile!r} is not yet set!")
    if filepath.parent.name != intermediatePath:
        eraFile.filepath = Path(intermediatePath, eraFile.getFilename())
    eraFile.skipLink = True
    logger.info(f"Updated file: {eraFile!r}")
    #-- Add the intermediate directory to the files list
    fn_files = Path(expconf.WORKFLOWDIR, expconf.SDATE, "extraFiles.pkl")
    files = ifh.loadFiles(fni=fn_files)
    intDirPresent = False
    for file in files:
        if isinstance(file, MiscFile) and file.getFilename() == intermediatePath:
            logger.debug(f"Intermediate path already in files list: {file!r}")
            intDirPresent = True
            break
    if not intDirPresent:
        intDir = MiscFile(fullstr=intermediatePath, date=sdate2date(expconf.SDATE))
        intDir.evalPattern2Source()
        intDir.evalPattern2Name()
        foundFile = intDir.findFile()
        if isinstance(foundFile, list):
            files.extend(foundFile)
        else:
            if foundFile != intDir:
                logger.warning("Processed file is not the same as the triggering file. Probably, something went wrong!")
            files.append(foundFile)
            logger.debug(f"Added file to files list: {foundFile!r}")
        intDir.getTarget().mkdir(exist_ok=True)
        #-- Save updated files list.
        ifh.saveFiles(fno=fn_files, files=files)


def main():
    logger = logging.getLogger(__name__)
    expconf = FullExperimentConfig()
    logging.basicConfig(level=expconf.LOGLEVEL)
    logger.setLevel(expconf.LOGLEVEL)
    expconf.JOBNAME = "%JOBNAME%"
    expconf.SDATE = "%SDATE%"
    expconf.CHUNKDATE = "%CHUNK_START_DATE%"
    expconf.check(level="chunk")
    REQUIRE_REMAPPING = ("%SIMULATION.REQUIRE_REMAPPING%".upper() == 'TRUE')
    # TODO: profile this script (maybe the find takes most of the time, restricting the pools could help)

    workdir = Path(expconf.INDIR, "prep-remap-era2icon")
    workdir.mkdir(parents=True, exist_ok=True)
    os.chdir(workdir)
    logger.debug(f"Changed to directory '{os.getcwd()}'")

    #-- Prepare files
    datelist = [sdate2date(expconf.CHUNKDATE)]
    if expconf.NUDGING_INTERVAL > 0:
        interval = timedelta(seconds=expconf.NUDGING_INTERVAL)
        d0 = timedelta()
        if (interval % timedelta(hours=1)) != d0:
            #-- Interval is no multiple of 1 hour.
            raise RuntimeError("[411] For automatic ERA5 remapping with nudging, only full hours are supported.")
        if interval <= d0:
            raise RuntimeError("[411] Time interval for nudging must be positive!")
        cdate = datelist[0]
        enddate = sdate2date("%CHUNK_END_DATE%")
        while cdate <= enddate:
            datelist.append(cdate)
            cdate = cdate + interval
    multiple_era_files = ( (expconf.NUDGING_INTERVAL > 0) or (expconf.REINIT and not expconf.CONTINUE_MET) )
    for date in datelist:
        fn_remap = Path(expconf.WORKFLOWDIR, expconf.SDATE, f"remapFiles_{date.strftime(expconf.DATEFORMAT_ICON)}.pkl")
        gridFile, eraFile, atmEraFile, sfcEraFile, atmRawFile, sfcRawFile = prepareFiles(multiple_era_files, date, expconf)
        logger.debug(f"Grid file: {gridFile!r}")
        logger.debug(f"Era5 file: {eraFile!r}")
        logger.debug(f"Atm remapped file: {atmEraFile!r}")
        logger.debug(f"Sfc remapped file: {sfcEraFile!r}")
        logger.debug(f"Atm raw file: {atmRawFile!r}")
        logger.debug(f"Sfc raw file: {sfcRawFile!r}")
        if (not isinstance(gridFile, GridFile) or
            not isinstance(eraFile, Era5File) or
            not isinstance(atmEraFile, IntermediateFile) or
            not isinstance(sfcEraFile, IntermediateFile) or
            not isinstance(atmRawFile, IntermediateFile) or
            not isinstance(sfcRawFile, IntermediateFile)):
            raise TypeError("[510] File types are incorrect.")
        ifh.saveFiles(fn_remap, [gridFile, eraFile, atmEraFile, sfcEraFile, atmRawFile, sfcRawFile])
        logger.info(f"Remapping file info saved to '{fn_remap}'.")
        
        #-- Remapping
        if eraFile.exists() and not REQUIRE_REMAPPING:
            logger.info("Remapped ERA5 file is present and remapping not required. Preparation can be skipped.")
            # TODO: pass -> remap is startet
        else:
            # TODO: fail -> remap is skipped
            pass

if __name__ == "__main__": main()
