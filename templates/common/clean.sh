# Copyright 2023 - 2024 Karlsruhe Institute of Technology (KIT) and Ludwig-Maximilians-Universität München (LMU)
# SPDX-FileContributor: Oriol Tinto Prims, Fabian Jakub
#
# SPDX-License-Identifier: BSD-3-Clause

# Remove heavy files from run-dir

# Get some variables provided by autosubmit.
WORKDIR=%HPCROOTDIR%
STARTDATE=%SDATE%
MEMBER=%MEMBER%
FILES_TO_CLEAN='%simulation.FILES_TO_CLEAN%'
echo "${FILES_TO_CLEAN}"

# Define run directory
RUNDIR=${WORKDIR}/${STARTDATE}/${MEMBER}

cd ${RUNDIR} || exit
# Remove the files in the run directory
rm -f ${FILES_TO_CLEAN}


# Remove the files in the remote output directory
OUTPUT_DIR=${WORKDIR}/output/${STARTDATE}/${MEMBER}

cd ${OUTPUT_DIR} || exit
rm -f ${FILES_TO_CLEAN}
