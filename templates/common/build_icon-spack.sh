# Copyright 2023 - 2024 Karlsruhe Institute of Technology (KIT) and Ludwig-Maximilians-Universität München (LMU)
# SPDX-FileContributor: Oriol Tinto Prims, Fabian Jakub
#
# SPDX-License-Identifier: BSD-3-Clause

# Get some variables provided by autosubmit.
# TODO: What do we do to ensure that these variables are defined in the proj file?
WORKDIR=%HPCROOTDIR%
ICON_VERSION=%ICON.VERSION%
COMPILER=%SPACK.COMPILER%

MODULES="%ENVIRONMENT.MODULES.PYTHON%"
if [ ! -z "${MODULES}" ]; then module load ${MODULES}; fi

# If the workdir directory does not exist create it
if [ ! -d ${WORKDIR} ]; then
  mkdir -p ${WORKDIR}
fi

# Go to the working directory
cd ${WORKDIR} || exit

. ${WORKDIR}/proj/platforms/common/spack_utils.sh
load_spack "%spack.init%" "%spack.root%" "%spack.url%" "%spack.branch%" "%spack.externals%" "%spack.compiler%" "%spack.disable_local_config%" "%spack.user_cache_path%" "%spack.user_config_path%" "%spack.upstreams%"

if [ ! $(rpm -qa | grep bzip2) ]; then
  spack install --reuse bzip2
  spack load --first bzip2
fi

SPACK_BUILD_ICON="%ICON.BUILD_CMD%"
SPACK_LOAD_ICON="%ICON.LOAD_CMD%"

if [ "${SPACK_LOAD_ICON}" == "build_cmd" ]; then
  SPACK_LOAD_ICON=${SPACK_BUILD_ICON}
fi

if [ ! -z "$SPACK_BUILD_ICON" ]; then
  echo "Installing ICON with spack!"
  echo "cmd=$SPACK_BUILD_ICON"
  # In case the autosubmit repository with the icon-nwp receipt doesn't exist, add it
  if [[ $(spack repo list | grep "${WORKDIR}/proj/spack_repo") ]]; then
    echo "icon spack repo was already added to repo list"
  else
    spack repo add ${WORKDIR}/proj/spack_repo
  fi
  spack spec $SPACK_BUILD_ICON
  spack install --reuse $SPACK_BUILD_ICON
  #TODO: had some problems with spack load when more than one version is available, adding --first to overcome that
  # although in principle we should not install the model if its already installed.
  spack load --first ${SPACK_LOAD_ICON}
else
  echo "\%icon.build_cmd\% is not defined. If you want to compile icon with spack, please provide a spack compile instruction string in your build.yml"
fi

# Build dwd-icon-tools
SPACK_BUILD_DWDICONTOOLS="%DWDICONTOOLS.BUILD_CMD%"
SPACK_LOAD_DWDICONTOOLS="%DWDICONTOOLS.LOAD_CMD%"
if [ "${SPACK_LOAD_DWDICONTOOLS}" == "build_cmd" ]; then SPACK_LOAD_DWDICONTOOLS=${SPACK_BUILD_DWDICONTOOLS}; fi
if [ ! -z "$SPACK_BUILD_DWDICONTOOLS" ]; then
  spack install --reuse $SPACK_BUILD_DWDICONTOOLS
  spack load --first ${SPACK_LOAD_DWDICONTOOLS}
fi

# Need to get ECCODES DWD definitions:
eccodes_version=$(spack spec $SPACK_BUILD_ICON | grep eccodes | grep -o "@.*%" | grep -o "[0-9\.]*")

definitions_tar_file=eccodes_definitions.edzw-${eccodes_version}-1.tar.bz2
if [ ! -f "${definitions_tar_file}" ]; then
  defs_url=https://opendata.dwd.de/weather/lib/grib/${definitions_tar_file}
  wget "${defs_url}"

  # Decompress definitions file
  tar -xf "${definitions_tar_file}"
  # Create a file containing the environment variable that needs to be set in order to use DWD's definitions:
  echo "export ECCODES_DEFINITION_PATH=${WORKDIR}/definitions.edzw-${eccodes_version}-1" >eccodes_defs.env
fi


# Create a file to set the proper environment to run ICON:

cat >icon_environment.env <<EOF
 # This file loads the proper environment to run icon.
# Activate spack
. ${WORKDIR}/proj/platforms/common/spack_utils.sh
load_spack "%spack.init%" "%spack.root%" "%spack.url%" "%spack.branch%" "%spack.externals%" "%spack.compiler%" "%spack.disable_local_config%" "%spack.user_cache_path%" "%spack.user_config_path%" "%spack.upstreams%"


# Get proper load command.
SPACK_BUILD_ICON="%ICON.BUILD_CMD%"
SPACK_LOAD_ICON="%ICON.LOAD_CMD%"

if [ "${SPACK_LOAD_ICON}" == "build_cmd" ]; then
  SPACK_LOAD_ICON=${SPACK_BUILD_ICON}
fi
# Load icon module
spack load --first ${SPACK_LOAD_ICON}

# Set environment variable for eccodes-dwd definitions:
source ${WORKDIR}/eccodes_defs.env

# Increase stack size limit
ulimit -s unlimited

EOF
