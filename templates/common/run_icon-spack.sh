# Copyright 2023 - 2024 Karlsruhe Institute of Technology (KIT) and Ludwig-Maximilians-Universität München (LMU)
# SPDX-FileContributor: Oriol Tinto Prims, Fabian Jakub
#
# SPDX-License-Identifier: BSD-3-Clause

# Get some variables provided by autosubmit.
WORKDIR=%HPCROOTDIR%
ICON_VERSION=%ICON_VERSION%

STARTDATE=%SDATE%
MEMBER=%MEMBER%

# Define rundir
RUNDIR=${WORKDIR}/${STARTDATE}/${MEMBER}

# Go to the member rundir
cd ${RUNDIR}

# Activate spack
. ${WORKDIR}/proj/platforms/common/spack_utils.sh
load_spack "%spack.init%" "%spack.root%" "%spack.url%" "%spack.branch%" "%spack.externals%" "%spack.compiler%" "%spack.disable_local_config%" "%spack.user_cache_path%" "%spack.user_config_path%" "%spack.upstreams%"


# Get proper load command.
SPACK_BUILD_ICON="%ICON.BUILD_CMD%"
SPACK_LOAD_ICON="%ICON.LOAD_CMD%"

if [ "${SPACK_LOAD_ICON}" == "build_cmd" ]; then
  SPACK_LOAD_ICON=${SPACK_BUILD_ICON}
fi
# Load icon module
spack load --first ${SPACK_LOAD_ICON}

# Set environment variable for eccodes-dwd definitions:
source ${WORKDIR}/eccodes_defs.env

# Increase stack size limit
ulimit -s unlimited

# Run icon
srun icon
