#!/usr/bin/python3

#-- > Remap IFS input data to the ICON grid using DWD_ICON_TOOLS. < --#

# Copyright 2023 - 2024 Karlsruhe Institute of Technology (KIT) and Ludwig-Maximilians-Universität München (LMU)
# SPDX-FileContributor: Andreas Baer
#
# SPDX-License-Identifier: BSD-3-Clause

import sys
sys.path.append("%PYTHON_ENVIRONMENT.MODULES_DIR%")

import logging
import os
from subprocess import run
from typing import Union
import f90nml
from pathlib import Path

from icon_file import GridFile, IfsFile, RawFile
import icon_file_handler as ifh
from experiment_config import FullExperimentConfig
from icon_variable import initVariables
from python_utils import sdate2date
import envmodules_dwd_icon_tools as envmodules


def getFiles(expconf: FullExperimentConfig):
    """ Get the file names for all required files. """
    logger = logging.getLogger(__name__)
    expconf.check(level="date")
    date = sdate2date(expconf.SDATE)
    ifsFile = None
    gridFile = None
    rawFile = RawFile(date=date)
    # TODO: generalize name here
    rawFile.setAllNames("ifs_r1279+O_{SDATE}.{FMT:s}")
    rawFile.iconFiletype = 2

    fn_domains = Path(expconf.WORKFLOWDIR, expconf.SDATE, "domains.pkl")
    domains = ifh.loadDomains(fn_domains)
    for domain in domains:
        if domain.type() == 'R': continue
        gridFile = domain.getGrid()
        ifsFile = domain.getFile("IFS")
        if not isinstance(ifsFile, IfsFile):
            raise TypeError("[510] File types are incorrect!")
        if ifsFile.date == date:
            logger.info(f"Domain {domain.index()} used.")
            break
        # TODO: what about multiple domains that all use IFS files?
    if ifsFile == None or gridFile == None:
        raise RuntimeError(f"[414] File object ifsfile {ifsFile!r} or grid file {gridFile!r} not found.")
    return (gridFile, ifsFile, rawFile)

def createFieldsNamelist(fnFields: Union[str, Path], expconf: FullExperimentConfig):
    """ Create the fields namelist for remapping. """
    logger = logging.getLogger(__name__)
    fnVars = Path(expconf.LOGDIR, "variables.yml")
    iconVariables = initVariables(fnVars=fnVars)
    logger.info(f"{len(iconVariables)} variable descriptions read from file '{fnVars}'.")
    #-- Creating the namelist
    fields = []     # list with the contents of each 'input_field_nml' group
    for key in expconf.INPUT_VARIABLES:
        if key not in iconVariables:
            logger.error("Available variables:")
            logger.error(iconVariables.keys())
            raise ValueError(f"[413] No description available for variable {key!r}.")
        var = iconVariables[key]
        f = {"inputname"   : var.GRIB2,
             "outputname"  : var.NAME,
             "code"        : var.GRIB1,
             "intp_method" : expconf.INPUT_INTP_METHOD
             }
        if var.USE_LSM:
            f["var_in_mask"] = "LSM"
            f["code_in_mask"] = 172
            f["in_mask_threshold"] = 0.5
            f["in_mask_below"] = True
        fields.append(f)
    #-- Write the namelist
    nml = f90nml.Namelist()
    for field in fields:
        nml.add_cogroup("input_field_nml", field)
    f90nml.write(nml, nml_path=fnFields, force=True)

def remap(gridFile: GridFile, ifsFile: IfsFile, rawFile: RawFile, expconf: FullExperimentConfig):
    """ Create namelists and do the remapping. """
    logger = logging.getLogger(__name__)
    expconf.check(level="exp", check_jobname=True)
    nmlMain   = ("prep-remap_"
                    + expconf.JOBNAME.removeprefix(expconf.EXPID + "_") )
    nmlFields = ("prep-remap-fields_"
                    + expconf.JOBNAME.removeprefix(expconf.EXPID + "_") )
    #-- Main namelist
    # TODO: catch the case when the ifs file was actually found in a pool
    #       but should still be remapped (because this file should not
    #       be used) and thus REQUIRE_REMAPPING is turned on.
    fnIfs = ifsFile.getTarget().name
    patches = {"remap_nml" :
               {"in_grid_filename"  : rawFile.getFilename(),
                "in_filename"       : rawFile.getFilename(),
                "out_grid_filename" : gridFile.getFilename(),
                "out_filename"      : fnIfs}}
    f90nml.patch(Path(expconf.LOGDIR, nmlMain), patches, nmlMain)
    logger.debug(f"Main namelist: {nmlMain!r}")
    #-- Fields namelist
    createFieldsNamelist(nmlFields, expconf=expconf)
    logger.debug(f"Fields namelist: {nmlFields!r}")

    #-- Prepare environment and command
    cmd = envmodules.cmd
    os.environ.update(envmodules.env)
    logger.info("Running command:")
    logger.info(cmd)
    cmd.extend([f"{expconf.ICONTOOLS_DIR}/bin/iconremap",
                "--remap_nml", nmlMain,
                "--input_field_nml", nmlFields])
    run(cmd, check=True)
    logger.debug(f"Moving {fnIfs!r} to '{ifsFile.getTarget()}'.")
    os.replace(fnIfs, ifsFile.getTarget())


def main():
    logger = logging.getLogger(__name__)
    expconf = FullExperimentConfig()
    logging.basicConfig(level=expconf.LOGLEVEL)
    logger.setLevel(expconf.LOGLEVEL)
    expconf.JOBNAME = "%JOBNAME%"
    expconf.SDATE = "%SDATE%"
    REQUIRE_REMAPPING = "%MISC.REQUIRE_REMAPPING%"

    workdir = Path(expconf.INDIR, "prep-remap-ifs2icon")
    workdir.mkdir(parents=True, exist_ok=True)
    os.chdir(workdir)
    logger.debug(f"Changed to directory '{os.getcwd()}'")

    #-- Prepare files
    gridFile, ifsFile, rawFile = getFiles(expconf)
    logger.debug(gridFile)
    logger.debug(ifsFile)
    logger.debug(rawFile)
    if (not isinstance(gridFile, GridFile) or
        not isinstance(ifsFile, IfsFile) or
        not isinstance(rawFile, RawFile)):
        raise TypeError("[510] File types are incorrect.")
    for file in [gridFile, ifsFile, rawFile]:
        if file.targetIsSet:
            foundFile = file.findFile()
            if isinstance(foundFile, list) or foundFile != file:
                raise RuntimeError("[510] Processed file is not the same as the triggering file. Something went wrong!")
        logger.info(file)
        if not isinstance(file, IfsFile):
            if file.exists(): file.createLink()
            else: raise FileNotFoundError(f"[414] {type(file).__name__} not found!")

    #-- Remapping
    if not ifsFile.exists() or REQUIRE_REMAPPING:
        remap(gridFile=gridFile, ifsFile=ifsFile, rawFile=rawFile, expconf=expconf)
    else:
        logger.info("Remapped IFS file is present and remapping not required. Nothing to be done.")

if __name__ == "__main__": main()
