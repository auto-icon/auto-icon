#!/usr/bin/python3

#-- > Run cdo to postprocess model output. < --#

# Copyright 2023 - 2024 Karlsruhe Institute of Technology (KIT) and Ludwig-Maximilians-Universität München (LMU)
# SPDX-FileContributor: Andreas Baer
#
# SPDX-License-Identifier: BSD-3-Clause

import sys
sys.path.append("%PYTHON_ENVIRONMENT.MODULES_DIR%")

import re
import fnmatch
import yaml
import logging
import os   # Keep as it is necessary for environment modules.
from pathlib import Path
from subprocess import run
from typing import Optional, Tuple, Union

from experiment_config import FullExperimentConfig
from icon_file import ResultFile
import icon_file_handler as ifh
from environment_modules import module as ml


class CdoOperator:
    """ Simple class for handling a cdo operator call. """
    def __init__(self, operator: Optional[str] = None,
                 opArgs: list[str] = [],
                 inputFiles = [],
                 binary = False,
                 outputFile: Optional[Union[str, ResultFile]] = None) -> None:
        self.operator = operator
        self.args = opArgs
        self.outputFile = outputFile
        if (binary and len(inputFiles) > 2):
            self.inputFiles = [
                inputFiles[0],
                CdoOperator(operator=operator,opArgs=opArgs,inputFiles=inputFiles[1:])
            ]
        else: self.inputFiles = inputFiles
    
    def __repr__(self) -> str:
        return f"CDO operator {self.operator!r}, args {self.args!r}: {self.inputFiles!r} -> {self.outputFile!r}"
    
    def __str__(self) -> str:
        return ' '.join(self.asListRec())
    
    def getOperatorWithArgs(self, chained=False) -> str:
        """ Return the operator with args. """
        if not self.operator: raise ValueError(f"[510] Operator is not yet set!")
        if chained: operator = '-' + self.operator
        else:       operator = self.operator
        for arg in self.args:
            operator +=  ',' + arg
        return operator
    
    def asListRec(self, chained=False) -> list:
        """ Return a cmd list. """
        if not self.operator: raise ValueError(f"[510] Operator is not yet set!")
        cmd = []
        cmd.append(self.getOperatorWithArgs(chained=chained))
        for input in self.inputFiles:
            if isinstance(input, ResultFile):
                cmd.append(input.getFilepath())
            elif isinstance(input, CdoOperator):
                cmd.extend(input.asListRec(chained=True))
            elif isinstance(input, str):
                cmd.append(input)
        if self.outputFile: cmd.append(self.outputFile)
        return cmd
    
    def getOrigFilename(self) -> str:
        """ Return the 'most original' file name, i.e. recursivley of the input file. """
        ifile = self.inputFiles[0]
        if   isinstance(ifile, str):         return ifile
        elif isinstance(ifile, ResultFile):  return ifile.getFilename()
        elif isinstance(ifile, CdoOperator): return ifile.getOrigFilename()
        else: raise TypeError(f"[511] Input file is of unsupported type '{type(ifile)}'.")


def getMatchingFiles(
        pattern: str,
        resultFiles: list[ResultFile],
        operatorStore: dict[str, Union[CdoOperator, list[CdoOperator]]] = {}
    ) -> list[Union[str, ResultFile, CdoOperator]]:
    """ Get all files (file name or ResultFile) that match a pattern. """
    logger = logging.getLogger(__name__)
    matchingFiles: list[Union[str, ResultFile, CdoOperator]] = []
    if pattern in operatorStore:
        res = operatorStore[pattern]
        if isinstance(res, CdoOperator): matchingFiles.append(res)
        elif isinstance(res, list):      matchingFiles.extend(res)
        else: raise TypeError(f"[510] Operator {res!r} from store is of unsupported type '{type(res)}'.")
        return matchingFiles
    for file in resultFiles:
        if fnmatch.fnmatchcase(file.getFilename(), pattern): matchingFiles.append(file)
    if len(matchingFiles) == 0:
        if Path(pattern).exists() and Path(pattern).is_file():
            matchingFiles.append(pattern)
            logger.info(f"File {pattern!r} used but is no ResultFile.")
        else:
            raise FileNotFoundError(f"[414] No file found matching name {pattern!r}. Check the filename patterns.")
    return matchingFiles

def createOperatorDefault(operators: list[CdoOperator],
                          config: dict,
                          inputFiles: list[Union[str, ResultFile, CdoOperator]],
                          fnOut: Optional[str],
                          resultFiles: list[ResultFile]):
    """ Create the operator for the default output case. """
    logger = logging.getLogger(__name__)
    operators.append(CdoOperator(operator=config["OPERATOR"],
                                 opArgs=config.get("ARGS", []),
                                 inputFiles=inputFiles,
                                 binary=config.get("BINARY", False),
                                 outputFile=fnOut))
    if fnOut:
        outfile = ResultFile(fn=fnOut)
        if outfile in resultFiles:
            logger.debug(f"Output file {outfile!r} already exists, not adding it to the list.")
        else:
            logger.debug(f"Appending output file {outfile!r}.")
            resultFiles.append(outfile)

def createOperatorPass(operatorStore: dict[str, Union[CdoOperator, list[CdoOperator]]],
                       action: str,
                       config: dict,
                       inputFiles: list[Union[str, ResultFile, CdoOperator]]):
    """ Create the operator for the output case PASS. """
    operatorStore[action] = CdoOperator(operator=config["OPERATOR"],
                                        opArgs=config.get("ARGS", []),
                                        inputFiles=inputFiles,
                                        binary=config.get("BINARY", False))

def createOperatorPassEach(operatorStore: dict[str, Union[CdoOperator, list[CdoOperator]]],
                           action: str,
                           config: dict,
                           inputFiles: list[Union[str, ResultFile, CdoOperator]]):
    """ Create the operator for the output case PASSEACH. """
    if not action in operatorStore: operatorStore[action] = []
    l = operatorStore[action]
    if not isinstance(l, list):
        raise TypeError(f"[512] Operator store entry {action!r} already exists and is no list.")
    l.append(
        CdoOperator(operator=config["OPERATOR"],
                    opArgs=config.get("ARGS", []),
                    inputFiles=inputFiles,
                    binary=config.get("BINARY", False))
    )

def createOperatorSubstitute(operators: list[CdoOperator],
                             config: dict,
                             inputFiles: list[Union[str, ResultFile, CdoOperator]],
                             file: Union[str, ResultFile, CdoOperator],
                             resultFiles: list[ResultFile],
                             m: Optional[re.Match[str]]):
    """ Create the operator for the substitute output case. """
    logger = logging.getLogger(__name__)
    if isinstance(file, str):           inputFile = file
    elif isinstance(file, ResultFile):  inputFile = file.getFilename()
    elif isinstance(file, CdoOperator): inputFile = file.getOrigFilename()
    else: raise ValueError("[510] For substituting, input files need to be ResultFile or str.")
    if not m: raise ValueError("[414] No valid match found! Check input filename and substitute pattern.")
    outputFile = re.sub(m.groups()[0], m.groups()[1], inputFile)
    cc = config.copy()
    cc["OUTPUT"] = outputFile
    operators.append(
        CdoOperator(operator=cc["OPERATOR"],
                    opArgs=cc.get("ARGS", []),
                    inputFiles=inputFiles,
                    binary=cc.get("BINARY", False),
                    outputFile=cc.get("OUTPUT"))
    )
    outfile = ResultFile(fn=outputFile)
    if outfile in resultFiles:
        logger.debug(f"Output file {outfile!r} already exists, not adding it to the list.")
    else:
        logger.debug(f"Appending output file {outfile!r}.")
        resultFiles.append(outfile)

def traverseFileIterable(filesConfig: Union[str, list, dict],
                         resultFiles: list[ResultFile],
                         operatorStore: dict[str, Union[CdoOperator, list[CdoOperator]]],
                         origFile: Optional[Union[str, ResultFile, CdoOperator]] = None
                         ) -> list[Tuple[Union[str, ResultFile, CdoOperator],
                                         list[Union[str, ResultFile, CdoOperator]]]]:
    """ Traverse a FILES/ADDITIONAL_FILES section and compile the list of input files. """
    logger = logging.getLogger(__name__)
    outList: list[Tuple[Union[str, ResultFile, CdoOperator],
                        list[Union[str, ResultFile, CdoOperator]]]] = []
    if   isinstance(filesConfig, str):  filesIterable = [filesConfig]
    elif isinstance(filesConfig, list): filesIterable = filesConfig
    elif isinstance(filesConfig, dict): filesIterable = filesConfig.items()
    else: raise TypeError(f"[411] Files configuration is of unknown type: '{type(filesConfig)}'")
    for entry in filesIterable:
        logger.debug(f"Processing {entry!r}")
        if isinstance(entry, str):
            pattern = entry     #-- filesConfig is str|list
        elif isinstance(entry, tuple):
            pattern = entry[0]  #-- filesConfig is dict -> select from file
        else: raise TypeError(f"[512] Entry in files iterable is of unknown type: {type(entry)}")
        if origFile:
            #-- ADDITIONAL_FILES section: pattern is a substitute pattern
            #-- the original file will be used for input file name retrieval
            regex = '/(\w+)/(\w+)/' # type: ignore
            m = re.fullmatch(regex, pattern)
            if not m: raise ValueError("[414] No valid match found! Check input filename and substitute pattern.")
            if isinstance(origFile, str):           inputFile = origFile
            elif isinstance(origFile, ResultFile):  inputFile = origFile.getFilename()
            elif isinstance(origFile, CdoOperator): inputFile = origFile.getOrigFilename()
            else: raise RuntimeError("[510] Internal error with file specifications!")
            outputFile = re.sub(m.groups()[0], m.groups()[1], inputFile)
            fileList = [outputFile]
        else:   #-- FILES section: pattern is an input pattern
            fileList = getMatchingFiles(pattern, resultFiles, operatorStore)
        for file in fileList:
            inputFiles = []
            if isinstance(entry, str):      #-- Single file
                inputFiles.append(file)
            elif isinstance(entry, tuple):  #-- Selection from file
                if entry[1]:
                    for key, sel in entry[1].items():
                        if isinstance(sel, str): sel = [sel]
                        for s in sel:
                            inputFiles.append(CdoOperator(operator=f"sel{key}",
                                                            opArgs=[s],
                                                            inputFiles=[file]))
                #-- else: Do not select for this file, but dict is required for other files.
                else: inputFiles.append(file)
            outList.append( (file, inputFiles) )
    logger.debug(f"Returning list to be processed: {outList!r}")
    return outList

def parseFilesToOperators(action: str,
                          config: dict,
                          resultFiles: list[ResultFile],
                          operatorStore: dict[str, Union[CdoOperator, list[CdoOperator]]]
                          ) -> list[CdoOperator]:
    """
    Parse the input files sections and create corresponding operators.
    Returns a list of operators which can then be run e.g. with `runCdoCmd`.
    :param action: currently processed action, required for using the operatorStore
    :param config: configuration section of the current action
    :param resultFiles: list of ResultFiles to process
    :param operatorStore: operator store with the currently available operators (for possible input)
    """
    logger = logging.getLogger(__name__)
    filesConfig: Union[str, list, dict] = config["FILES"]
    addFilesConfig: Optional[Union[str, list, dict]] = config.get("ADDITIONAL_FILES")
    #-- Process output section.
    output = config.get("OUTPUT")
    m = None
    if isinstance(output, str):
        regex = '/(\w+)/(\w+)/' # type: ignore
        m = re.fullmatch(regex, output)
        if m: output = "SUBSTITUTE"
    else:
        if output != None: raise TypeError("[411] Unsupported type for the OUTPUT field!")
    singleOperator = True
    if (output == "SUBSTITUTE" or output == "PASSEACH"): singleOperator = False
    #-- Process input files.
    operators: list[CdoOperator] = []
    outList: list[Tuple[Union[str, ResultFile, CdoOperator], list[Union[str, ResultFile, CdoOperator]]]] = []
    logger.debug(f"Looping over: {filesConfig!r}")
    outList = traverseFileIterable(filesConfig=filesConfig,
                                   resultFiles=resultFiles,
                                   operatorStore=operatorStore)
    #-- Create operators or outList items
    if singleOperator:
        logger.debug("Creating a single operator.")
        inputFiles = []
        for item in outList:
            inputFiles.extend(item[1])
        if output == "PASS": createOperatorPass(operatorStore, action, config, inputFiles)
        else: createOperatorDefault(operators, config, inputFiles, output, resultFiles)
    else:
        logger.debug("Creating one operator per input file.")
        for item in outList:
            #-- Create the list
            inputFiles = item[1]
            if addFilesConfig:
                addOutList = traverseFileIterable(filesConfig=addFilesConfig,
                                                  resultFiles=resultFiles,
                                                  operatorStore=operatorStore,
                                                  origFile=item[0])
                for addItem in addOutList:
                    inputFiles.extend(addItem[1])
            #-- Apply the list
            if output == "SUBSTITUTE":
                createOperatorSubstitute(operators, config, inputFiles, item[0], resultFiles, m)
            elif output == "PASSEACH":
                createOperatorPassEach(operatorStore, action, config, inputFiles)
            else: raise RuntimeError("[512] Some weird combination of OUTPUT section made the system crash.")
    return operators

def runCdoCmd(operator: CdoOperator, options: list[str]):
    """ Do the actual operator run. """
    logger = logging.getLogger(__name__)
    cmd = ['cdo']
    #-- General options
    cmd.extend(options)
    #-- Operator and input/output
    cmd.extend(operator.asListRec())
    if ('-O' not in options and
        ((isinstance(operator.outputFile, ResultFile) and operator.outputFile.exists()) or
         (isinstance(operator.outputFile, str) and Path(operator.outputFile).exists()))):
        logger.error("Command to run:")
        logger.error(' '.join(cmd))
        raise FileExistsError(f"[470] Output file {operator.outputFile!r} exists and should not be overwritten.")
    #-- Run
    logger.info(f"Running command:")
    logger.info(' '.join(cmd))
    run(cmd, check=True)

def main():
    #-- ----- Initialization ----- --#
    logger = logging.getLogger(__name__)
    expconf = FullExperimentConfig()
    logging.basicConfig(level=expconf.LOGLEVEL)
    logger.setLevel(expconf.LOGLEVEL)
    expconf.SDATE = "%SDATE%"
    expconf.MEMBER = "%MEMBER%"
    expconf.check(level="member")
    cdoModule = "%ENVIRONMENT.MODULES.CDO%"
    res = ml('load', cdoModule)
    if not res: raise RuntimeError(f"[415] Module file {cdoModule!r} could not be loaded.")
    configPath = Path(expconf.LOGDIR, "expconf.yml")
    with open(configPath, 'r') as stream:
        cdoConfig = yaml.safe_load(stream).get("CDO")
    if cdoConfig == None:
        logger.info("Nothing to be done (config empty)!")
        return

    #-- ----- Set up files list ----- --#
    os.chdir(Path(expconf.OUTDIR, expconf.SDATE, expconf.MEMBER))
    logger.debug(f"Changed to directory '{os.getcwd()}'")

    workflowdir = Path(expconf.WORKFLOWDIR, expconf.SDATE, expconf.MEMBER)
    workflowdir.mkdir(parents=True, exist_ok=True)
    fnResultFiles = workflowdir / "resultFiles.pkl"
    resultFiles = ifh.createResultFilesList(expconf, fnResultFiles, initAsPrimary=True)
    logger.debug("Available result files:")
    logger.debug(resultFiles)

    #-- ----- Create and run command ----- --#
    if "OPTIONS" in cdoConfig:
        options = cdoConfig.pop("OPTIONS")
        if isinstance(options, str): options = [options]
        elif options == None: options = []
    else: options = []
    operatorStore = {}
    for action, config in cdoConfig.items():
        if action == "SPLITVARS":
            logger.info(f"Processing action {action!r}")
            if config == None: config = {}
            config["OPERATOR"] = 'splitname'
            pattern = config.get("FILES", "*")
            for file in resultFiles:
                if not fnmatch.fnmatchcase(file.getFilename(), pattern): continue
                fp = file.filepath
                if fp == None: raise ValueError(f"[411] File name for file {file!r} not provided!")
                config["OUTPUT"] = fp.stem + '-'
                op = CdoOperator(operator=config["OPERATOR"],
                                opArgs=config.get("ARGS",[]),
                                inputFiles=[file],
                                binary=config.get("BINARY", False),
                                outputFile=config.get("OUTPUT"))
                runCdoCmd(operator=op, options=options)
            newFilesList = ifh.createResultFilesList(expconf, fnResultFiles, write=False)
            for file in newFilesList:
                if file not in resultFiles: resultFiles.append(file)
            continue
        logger.info(f"Processing action {action!r}")
        operatorList = parseFilesToOperators(action, config, resultFiles, operatorStore)
        for op in operatorList:
            runCdoCmd(operator=op, options=options)
    ifh.saveFiles(fnResultFiles, resultFiles)

if __name__ == "__main__": main()
