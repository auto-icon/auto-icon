#!/usr/bin/bash

#-- > Prepare output directories for an experiment. < --#

# Copyright 2023 - 2024 Karlsruhe Institute of Technology (KIT) and Ludwig-Maximilians-Universität München (LMU)
# SPDX-FileContributor: Andreas Baer
#
# SPDX-License-Identifier: BSD-3-Clause

set -e  #-- This will be set by autosubmit anyway and allows for proper shell syntax checking.

#-- ----- -------------------- ----- --#
#-- Provide a utility file to be used in further scripts.
#-- Arguments:
#--   Bool ('true'|'false') if the file is required
#--   File name as in the auto-icon repository (with extension)
#--   (optional) File name as it should be provided as utils file (with extension)
#-- ----- -------------------- ----- --#
function provideUtilsFile() {
    local critical=$1
    local fn_source=$2
    local fn_target=${3:-${fn_source}}
    JOBNAME="%JOBNAME%"
    LOGDIR="%CURRENT_LOGDIR%"
    sc="${LOGDIR}/${fn_source%.*}_${JOBNAME#%DEFAULT.EXPID%_}"
    tg="${LOGDIR}/${fn_target}"
    #-- If file is critial: try move, if it fails -> error
    #-- If not critical: only move if the file exists
    if [ "${critical,,}" = true ] || [ -f "$sc" ]
    then
        mv "$sc" "$tg"
    fi
}

#-- ----- -------------------- ----- --#
#-- Provide a python package file to be used in further scripts.
#-- Arguments:
#--   Bool ('true'|'false') if the file is required
#--   File name as in the auto-icon repository (with extension)
#--   (optional) File name as it should be provided as utils file (with extension)
#-- ----- -------------------- ----- --#
function providePythonPackage() {
    local critical=$1
    local fn_source=$2
    local fn_target=${3:-${fn_source}}
    JOBNAME="%JOBNAME%"
    LOGDIR="%CURRENT_LOGDIR%"
    PYTHONDIR="%PYTHON_ENVIRONMENT.MODULES_DIR%"
    mkdir -p "${PYTHONDIR}"
    sc="${LOGDIR}/${fn_source%.*}_${JOBNAME#%DEFAULT.EXPID%_}"
    tg="${PYTHONDIR}/${fn_target}"
    #-- If file is critial: try move, if it fails -> error
    #-- If not critical: only move if the file exists
    if [ "${critical,,}" = true ] || [ -f "$sc" ]
    then
        mv "$sc" "$tg"
    fi
}

#-- ----- -------------------- ----- --#
#-- Fetches an archive, unzips it and places its contents into the input directory.
#-- Arguments:
#--   Archive name/address
#--   Input directory
#-- Outputs:
#--   Writes to stdout info on fetched archive
#-- Returns:
#--   2 if archive could not be found
#-- ----- -------------------- ----- --#
function archiveToIndir () {
    local ARCHIVE=$1
    local INDIR=$2
    local EXPNAME="%EXPNAME%"
    
    echo "Trying to fetch archive '$ARCHIVE'."
    local url_regex='^(https?|ftp|file)://*'
    if [[ ${ARCHIVE} =~ $url_regex ]]
    then
        #-- Download to a teporary file
        TMPFN=$(mktemp)
        if ! curl --fail "$ARCHIVE" -o "$TMPFN"
        then
            rm -f "$TMPFN"
            echo "Downloading archive '$ARCHIVE' failed!"
            return 2
        fi
    else
        if [ ! -f "$ARCHIVE" ]
        then
            echo "Archive is no URL, and not present on local storage!"
            return 2
        fi
        TMPFN="$ARCHIVE"
    fi

    #-- Unzip and move to INDIR
    mkdir -p "$INDIR"
    TMPFNUNIZIP=$(mktemp -d -p "$INDIR")
    unzip "$TMPFN" -d "$TMPFNUNIZIP"
    rm -f "$TMPFN"
    if [ "${INDIR##*/}" = "$EXPNAME" ]
    then
        DESTINATION="$INDIR"
    else
        DESTINATION="$INDIR/$EXPNAME"
    fi

    if [ "$(ls -A "$TMPFNUNIZIP")" = "$EXPNAME" ]
    then
        SOURCE="$TMPFNUNIZIP/$EXPNAME"
    else
        SOURCE="$TMPFNUNIZIP"
    fi
    #-- This workaround allows overwriting directories
    for name in "$SOURCE/"*
    do
        rm -rf "${DESTINATION:?}/${name##*/}"
        mv "$name" "$DESTINATION/"
    done
    rm -r "$TMPFNUNIZIP"
}


#-- ----- Main script ----- --#
provideUtilsFile true  "envmodules-%DEFAULT.HPCARCH%.sh" "envmodules.sh"
provideUtilsFile false "%EXPNAME%.yml" "expconf.yml"
provideUtilsFile false "variables.yml"
providePythonPackage false "python_utils.py"
providePythonPackage false "icon_file.py"
providePythonPackage false "icon_domain.py"
providePythonPackage false "icon_file_handler.py"
providePythonPackage false "experiment_config.py"
providePythonPackage false "envmodules_dwd_icon_tools.py"
providePythonPackage false "icon_variable.py"
providePythonPackage false "rename_reinit_files.py"
providePythonPackage false "visualization_utils.py"
providePythonPackage false "unitlib.py"
providePythonPackage false "namelist_utils.py"

ARCHIVE="%DIRECTORIES.ARCHIVE%"
INDIR="%DIRECTORIES.INDIR%"

if [ "$ARCHIVE" ]
then
    archiveToIndir "$ARCHIVE" "$INDIR"
fi
