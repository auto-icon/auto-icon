#!/usr/bin/python3

#-- > Container class for the full experiment config. < --#

# Copyright 2023 - 2024 Karlsruhe Institute of Technology (KIT) and Ludwig-Maximilians-Universität München (LMU)
# SPDX-FileContributor: Andreas Baer
#
# SPDX-License-Identifier: BSD-3-Clause

import logging
from os import path
from python_utils import optionalbool, optionalfloat, optionalint, optionalstr, readFromAsList


class ExperimentConfig():
    """
    Container class for the experiment configuration, obtained
    from autosubmit.
    """
    def __init__(self):
        # TODO: initialize from the experiment_data.yml file.
        lvl_str          = "%LOGLEVEL%"
        lvl = getattr(logging, lvl_str.upper(), None)
        if not isinstance(lvl, int):
            raise ValueError(f"[411] Invalid log level: {lvl_str!r}. Check the main configuation file <ICON_CASE>.yml.")
        self.LOGLEVEL    = lvl
        
        self.ICONFOLDER  = path.expandvars(path.expanduser("%ICON.INSTALLDIR%"))
        self.INDIR       = path.expandvars(path.expanduser("%DIRECTORIES.INDIR%"))
        self.OUTDIR      = path.expandvars(path.expanduser("%DIRECTORIES.OUTDIR%"))
        self.REFDIR      = path.expandvars(path.expanduser("%DIRECTORIES.REFDIR%"))
        self.LOGDIR      = path.expandvars(path.expanduser("%CURRENT_LOGDIR%"))
        self.WORKFLOWDIR = path.expandvars(path.expanduser("%DIRECTORIES.WORKFLOW%"))
        self.EXPID       = "%DEFAULT.EXPID%"
        self.EXPNAME     = "%EXPNAME%"
        self.FILETYPE    = int("%SIMULATION.FILETYPE%")
        self.REQUIRE_CLEAN_OUTDIR = bool("%SIMULATION.REQUIRE_CLEAN_OUTDIR%")
        self.ICONTOOLS_DIR = "%DWDICONTOOLS.REPODIR%"
        self.FNO_PATTERN = "%SIMULATION.FNO_PATTERN%"
        #-- Experiment
        self.NAMELIST_MASTER       = "%NAMELIST.MASTER%"
        self.NAMELIST_MASTER_IN    = optionalstr("%NAMELIST.MASTER_IN%", self.NAMELIST_MASTER)
        self.NAMELIST_ATMO         = "%NAMELIST.ATMO%"
        self.NAMELIST_ATMO_IN      = optionalstr("%NAMELIST.ATMO_IN%", self.NAMELIST_ATMO)
        self.NAMELIST_JSBACH       = "%NAMELIST.JSBACH%"
        self.NAMELIST_JSBACH_IN    = optionalstr("%NAMELIST.JSBACH_IN%", self.NAMELIST_JSBACH)
        self.NAMELIST_USE_JSBACH   = optionalbool("%NAMELIST.USE_JSBACH%", False)
        self.NAMELIST_OCEAN        = "%NAMELIST.OCEAN%"
        self.NAMELIST_OCEAN_IN     = optionalstr("%NAMELIST.OCEAN_IN%", self.NAMELIST_OCEAN)
        self.NAMELIST_USE_OCEAN    = optionalbool("%NAMELIST.USE_ocean%", False)
        self.NAMELIST_COUPLING     = "%NAMELIST.COUPLING%"
        self.NAMELIST_COUPLING_IN  = optionalstr("%NAMELIST.COUPLING_IN%", self.NAMELIST_COUPLING)
        self.NAMELIST_USE_COUPLING = optionalbool("%NAMELIST.USE_COUPLING%", False)
        self.DATEFORMAT_ICON = "%SIMULATION.DATE_FORMAT%"
        self.OUTPUT_INTERVAL = "%EXPERIMENT.OUTPUT_INTERVAL%"
        self.OUTPUT_INTERVAL_OCEAN = "%EXPERIMENT.OUTPUT_INTERVAL_OCEAN%"
        self.REINIT          = ("%REINITIALIZATION.REINIT%".upper()  == 'TRUE')
        self.CONTINUE_MET    = ("%REINITIALIZATION.CONTINUE_MET%".upper()  == 'TRUE')
        self.CONTINUE_AERO   = ("%REINITIALIZATION.CONTINUE_AERO%".upper() == 'TRUE')
        self.CONTINUE_CHEM   = ("%REINITIALIZATION.CONTINUE_CHEM%".upper() == 'TRUE')
        self.NUDGING_INTERVAL = optionalint("%EXPERIMENT.NUDGING_INTERVAL%", -1)
        self.NUDGING_RANGE = optionalstr("%EXPERIMENT.NUDGING_RANGE%".lower(), "default")
        NML_TYPE = "%NAMELIST_SUFFIX%"
        if NML_TYPE == 'nml':
            self.YAML_NAMELIST = False
        elif NML_TYPE == 'yml' or NML_TYPE == 'yaml':
            self.YAML_NAMELIST = True
        else: raise ValueError(f"[411] Unknown namelist type {NML_TYPE!r}, should be 'nml' or 'yml'. Check the main configuation file <ICON_CASE>.yml.")
        #-- Input files
        self.FILELIST    = readFromAsList("%GRID.FILELIST%")
        self.MISC_FILES  = readFromAsList("%DIRECTORIES.LINK_FILES.FILES%", expandPath=True)
        self.ICON_FILES  = readFromAsList("%DIRECTORIES.LINK_FILES.ICON_DATA%", expandPath=True)
        self.ART_FILES   = readFromAsList("%DIRECTORIES.LINK_FILES.ART%", expandPath=True)
        #-- Pools
        self.POOLS = {}
        self.POOLS["INDIR"]   = [ self.INDIR ]
        self.POOLS["GRID"]    = readFromAsList("%DIRECTORIES.POOL.GRID%", expandPath=True)
        self.POOLS["GENERAL"] = readFromAsList("%DIRECTORIES.POOL.GENERAL%", expandPath=True)
        self.POOLS["EXP"]     = readFromAsList("%DIRECTORIES.POOL.EXP%", expandPath=True)
        for i, item in enumerate(self.POOLS["EXP"]):
            self.POOLS["EXP"][i] = item + "/" + self.EXPNAME
        self.POOLS["ICON"]    = [ self.ICONFOLDER ]
        self.POOLS["ART"]     = [
            self.ICONFOLDER + "/externals/art/runctrl_examples/xml_ctrl/" + self.EXPNAME,
            self.ICONFOLDER + "/externals/art/runctrl_examples/photo_ctrl",
            self.ICONFOLDER + "/externals/art/runctrl_examples/init_ctrl",
            self.ICONFOLDER + "/externals/art"
            ]
        for pool in self.POOLS:
            for pooldir in pool:
                pooldir = path.expandvars(pooldir)
        #-- Misc
        self.INPUT_VARIABLES = readFromAsList("%SIMULATION.INPUT_VARIABLES%")
        self.INPUT_INTP_METHOD = optionalint("%SIMULATION.INTERPOLATION_METHOD%")
        self.ICON_CHECK_STRICT = ("%ICON.ICON_CHECK_STRICT%".upper()  == 'TRUE')
        #-- Resources
        self.OCEAN_TASKS_PERC = optionalfloat("%JOBS.RUN_ICON.OCEAN_TASKS_PERC%", 0.15)
    
    def __repr__(self) -> str:
        return f"ExperimentConfig for expid={self.EXPID}, expname={self.EXPNAME}"
    
class FullExperimentConfig(ExperimentConfig):
    """
    Extension of the experiment config with placeholders for dynamic
    configuration properties.
    """
    def __init__(self):
        super().__init__()
        self.IS_FIRST_CHUNK: bool
        self.CHUNK_STR: str
        self.SDATE: str
        self.CHUNKDATE: str
        self.CHUNKSIZEUNIT: str
        self.MEMBER: str
        self.JOBNAME: str
    
    def __repr__(self) -> str:
        res = super().__repr__()
        if self.SDATE:     res += f", sdate={self.SDATE}"
        if self.CHUNKDATE: res += f", chunk_date={self.CHUNKDATE}"
        if self.MEMBER:    res += f", member={self.MEMBER}"
        if self.JOBNAME:   res += f", jobname={self.JOBNAME}"
        return res
    
    def check(self, level: str, check_jobname=False, check_chunk_full=False):
        """ Check if all parameters for a level are set. """
        if level.lower() == "exp" or level.lower() == "experiment":
            pass
        elif level.lower() == "date" or level.lower() == "sdate":
            if self.SDATE is None or len(self.SDATE) == 0:
                raise ValueError("[510] Start date is not available.")
        elif level.lower() == "member":
            if self.MEMBER is None or len(self.MEMBER) == 0:
                raise ValueError("[510] Member is not available.")
        elif level.lower() == "chunk":
            if (self.CHUNKDATE is None or len(self.CHUNKDATE) == 0):
                raise ValueError("[510] Chunk is not available.")
            if (check_chunk_full and
                (self.CHUNK_STR is None or len(self.CHUNK_STR) == 0 or
                self.CHUNKSIZEUNIT is None or len(self.CHUNKSIZEUNIT) == 0 or
                self.IS_FIRST_CHUNK is None)):
                raise ValueError("[510] CHUNK_FIRST is not available.")
        else:
            raise ValueError(f"[510] Unknown level {level!r} for experiment config validation.")
        if check_jobname and (self.JOBNAME is None or len(self.JOBNAME) == 0):
                raise ValueError("[510] Jobname is not available.")
