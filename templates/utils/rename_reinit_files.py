#!/usr/bin/python3

#-- > Prepare reinitialization data after a chunk of simulation (yaml namelists). < --#

# Copyright 2023 - 2024 Karlsruhe Institute of Technology (KIT) and Ludwig-Maximilians-Universität München (LMU)
# SPDX-FileContributor: Andreas Baer
#
# SPDX-License-Identifier: BSD-3-Clause

import argparse
import logging
import os
from pathlib import Path

from experiment_config import FullExperimentConfig
import icon_file_handler as ifh

parser = argparse.ArgumentParser(prog='ChunkReinit', description='This short porgramm renames the auto-icon reinit files with their corresponding ICON input file names.')
parser.add_argument('--sdate',  help="Job's value for SDATE.")
parser.add_argument('--cdate',  help="Job's value for CHUNK_END_DATE.")
parser.add_argument('--member', help="Job's value for MEMBER.")
args = parser.parse_args()
if (len(args.sdate) == 0 or
    len(args.cdate) == 0 or
    len(args.member) == 0):
    raise ValueError(f"[510] Start date {args.sdate!r}, chunk (end) date {args.cdate!r} or member {args.member!r} not provided.")
#-- Initialization
logger = logging.getLogger(__name__)
expconf = FullExperimentConfig()
logging.basicConfig(level=expconf.LOGLEVEL)
logger.setLevel(expconf.LOGLEVEL)
expconf.SDATE = args.sdate
expconf.MEMBER = args.member
expconf.check(level="member")
datestr = args.cdate
outdir = Path(expconf.OUTDIR, expconf.SDATE, expconf.MEMBER)

#-- Main script
os.chdir( outdir )
logger.info(f"Changed WD to '{outdir}'.")
fn_domains = Path(expconf.WORKFLOWDIR, expconf.SDATE, "domains.pkl")
for domain in ifh.loadDomains(fn_domains):
    if domain.type() == 'R': continue
    g = domain.getGrid()
    MET_TARGET_FN  = f"dwdFG_R{g.r:01d}B{g.b:02d}_DOM{domain.index():02d}.nc"
    AERO_TARGET_FN = f"ART_IAE_iconR{g.r:01d}B{g.b:02d}-grid_{g.number:04d}.nc"
    CHEM_TARGET_FN = f"ART_ICE_iconR{g.r:01d}B{g.b:02d}-grid_{g.number:04d}.nc"
    MET_CURR_FN    = f"auto-icon_icon-art-met-{datestr}_DOM{domain.index():02d}_ML_0001.nc"
    AERO_CURR_FN   = f"auto-icon_icon-art-aero-{datestr}_DOM{domain.index():02d}_ML_0001.nc"
    CHEM_CURR_FN   = f"auto-icon_icon-art-chem-{datestr}_DOM{domain.index():02d}_ML_0001.nc"
    if expconf.CONTINUE_MET:
        logger.info(f"Replacing {MET_CURR_FN!r} with {MET_TARGET_FN!r}")
        os.replace(MET_CURR_FN, MET_TARGET_FN)
    if expconf.CONTINUE_AERO:
        logger.info(f"Replacing {AERO_CURR_FN!r} with {AERO_TARGET_FN!r}")
        os.replace(AERO_CURR_FN, AERO_TARGET_FN)
    if expconf.CONTINUE_CHEM:
        logger.info(f"Replacing {CHEM_CURR_FN!r} with {CHEM_TARGET_FN!r}")
        os.replace(CHEM_CURR_FN, CHEM_TARGET_FN)
