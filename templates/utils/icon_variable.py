#!/usr/bin/python3

#-- > Utilities for all file operations for ICON input and output files. < --#

# Copyright 2023 - 2024 Karlsruhe Institute of Technology (KIT) and Ludwig-Maximilians-Universität München (LMU)
# SPDX-FileContributor: Andreas Baer
#
# SPDX-License-Identifier: BSD-3-Clause

import logging
from pathlib import Path
from typing import Union
import yaml


class IconVariable():
    """
    Icon variable class
    """
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.INFO)
    
    def __init__(self, name="", desc="",
                 grib1=-1, grib2="", gribt="", era5="",
                 ndims=-1, use_lsm=False, invariant=False):
        super().__init__()
        self.NAME  = name      # ICON variable name
        self.DESC  = desc      # Description / long name
        # GRIB values can be lists
        self.GRIB1 = grib1     # GRIB1 code
        self.GRIB2 = grib2     # GRIB2 variable short name
        self.GRIBT = gribt     # GRIB triplet
        self.ERA5  = era5      # ERA5 variable short name
        self.NDIMS = ndims     # Number of dimensions (usually 2 or 3)
        self.USE_LSM = use_lsm # Use land/sea mask for remapping
        self.INVARIANT = invariant # Variable is time invariant
        if len(self.ERA5) == 0 and len(self.GRIB2) > 0:
            self.ERA5 = self.GRIB2
    
    def __repr__(self):
        return f"{self.NAME}: {self.DESC}"

def initVariables(fnVars: Union[str, Path]) -> dict:
    """
    Initialize the set of ICON variables from a yaml file.
    :param fnVars: file name / path to the yaml file
    :returns: dict with variable name and object
    """
    with open(fnVars, 'r') as stream:
        vars = yaml.safe_load(stream).get('VARIABLES', {})
    iconVariables = {}
    for key, params in vars.items():
        iconVariables[key] = IconVariable(
            name=key,
            desc=params.get("DESC", ""),
            grib1=params.get("GRIB1", -1),
            grib2=params.get("GRIB2", ""),
            gribt=params.get("GRIBT", ""),
            era5=params.get("ERA5", ""),
            ndims=params.get("NDIMS", -1),
            use_lsm=params.get("USE_LSM", False),
            invariant=params.get("INVARIANT", False)
        )
    return iconVariables
