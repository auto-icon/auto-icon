#!/usr/bin/python3

# Copyright 2023 - 2024 Karlsruhe Institute of Technology (KIT) and Ludwig-Maximilians-Universität München (LMU)
# SPDX-FileContributor: Andreas Baer
#
# SPDX-License-Identifier: BSD-3-Clause


import fnmatch
import os
import copy
import logging
import yaml
import xarray as xr
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.colors as mcl
import matplotlib.cm as mcm
import matplotlib.figure
from matplotlib.contour import ContourSet
import cartopy.crs as ccrs
import cartopy.feature as cfeature
from cartopy.mpl.geoaxes import GeoAxes
from pathlib import Path
from typing import Callable, Optional, Tuple, Union
from scipy import integrate

from icon_file import ResultFile
import icon_file_handler as ifh
from experiment_config import FullExperimentConfig
from icon_domain import IconDomain
from python_utils import str2slice
from unitlib import Units


DIM_NAMES_LEVELS = ['height', 'height_2', 'height_3', 'plev']
DIM_NAMES_PLANE = ['ncells', 'lat', 'lon']
VAR_NAMES_HEIGHT = ['z_mc', 'z_ifc']
R_EARTH = 6.371229e6 # meters

def baseFigure(cmap: mcl.Colormap) -> matplotlib.figure.Figure:
    """
    This function creates the base figure for the plot.
    :returns: tuple of (figure, axes)
    """
    #-- Create figure and axes.
    fig = plt.figure()
    ax = plt.axes(projection=ccrs.PlateCarree())
    if not isinstance(ax, GeoAxes): raise TypeError("[510] Creating a figure with GeoAxes failed.")

    # TODO: make this flexible with the config
    #-- Add coastlines, country border lines and grid lines.
    ax.coastlines(zorder=1)
    #-- Create states outlines.
    states_provinces = cfeature.NaturalEarthFeature(
        category='cultural',
        name='admin_1_states_provinces_lines',
        scale='50m',
        facecolor='none'
    )
    ax.add_feature(cfeature.BORDERS, linewidth=0.6, edgecolor='gray', zorder=2)
    ax.add_feature(states_provinces, linewidth=0.3, edgecolor='gray', zorder=3)
    # TODO: add output bounds to the grid
    ax.gridlines(draw_labels=True,
                 linewidth=0.5,
                 color='gray',
                 xlocs=range(-180,180,30),
                 ylocs=range(-90,90,30),
                 zorder=4)
    fig.colorbar(mcm.ScalarMappable(norm=mcl.Normalize(), cmap=cmap),
                 ax=ax,
                 orientation='horizontal',
                 shrink=0.7)
    fig.tight_layout(pad=0.5)
    return fig

def addContours(fig: matplotlib.figure.Figure,
                x: xr.DataArray,
                y: xr.DataArray,
                z: xr.DataArray,
                cmap: mcl.Colormap,
                norm: mcl.Normalize,
                triangGrid: bool,
                cconf: dict,
                selection={}):
    """
    Add main content to the figure:
    - title
    - contour plot of the respective variable
    - colorbars
    - labels
    :param fig: figure object
    :param x: x of data
    :param y: y of data
    :param z: data to plot
    :param cmap: colormap to use
    :param triangGrid: grid is triangular, i.e. use tricontourf for plotting
    :param cconf: colorbar configuration (respective visConf subsection)
    :param selection: variable selection to add info to title
    """
    kwargs_cbar = {}
    kwargs_cbar["orientation"] = "horizontal"
    axes = fig.get_axes()
    geoax = axes[0]
    #-- Set colorbar axis.
    for cax in axes:
        if cax.get_label() == "<colorbar>":
            if 'cax' in kwargs_cbar: raise RuntimeError("[510] More colorbar axes than expected.")
            cax.clear()
            kwargs_cbar['cax'] = cax
    #-- Set plot function.
    if triangGrid: plot_func = geoax.tricontourf
    else:          plot_func = geoax.contourf
    #-- Create color scale.
    kwargs_cbar['label'] = z.attrs.get("units", "")
    ext = extendFromCconf(cconf)
    if ext: kwargs_cbar['extend'] = ext
    #-- Set extend parameter for colorbar.
    colorlevels = int(cconf.get("NLEVELS", 15))
    if cconf.get("SCALE") == 'log':
        if norm.vmin: mn = norm.vmin
        else:         mn = z.min()
        if norm.vmax: mx = norm.vmax
        else:         mx = z.max()
        z = z.fillna(mn*1e-1)
        colorlevels = np.logspace(np.log10(mn), np.log10(mx), int(cconf.get("NLEVELS", 15)))
    #-- Plot the data.
    plot_func(x, y, z,
              levels=colorlevels,
              cmap=cmap, norm=norm,
              zorder=0, transform=ccrs.PlateCarree())
    #-- Colorbar
    fig.colorbar(mcm.ScalarMappable(norm=norm, cmap=cmap), **kwargs_cbar)
    #-- Title
    for key, value in selection.items():
        if key == 'time':
            geoax.set_title(f"{key}: {value}", loc='left')
        elif key in DIM_NAMES_LEVELS:
            geoax.set_title(f"{key}: {value}", loc='right')
    geoax.set_title(z.attrs.get("long_name", z.attrs.get("standard_name", "")), fontweight='bold')

def removeContours(fig: matplotlib.figure.Figure):
    """ Remove all contours from the first axis of the figure. """
    for coll in fig.get_axes()[0].collections:
        if isinstance(coll, ContourSet): coll.remove()

def adjustUnits(da: xr.DataArray, multiplier: Union[str, float, int]):
    """ Adjust the DataArrays units inplace. """
    unitstring = da.attrs.get('units',"")
    if unitstring == '' or unitstring == '-':
        if isinstance(multiplier, str): da.attrs['units'] = multiplier
        #-- else: just keep the unit; unitless times number stays unitless
    else: da.attrs['units'] = str(Units(unitstring, multiplier))

def extendFromCconf(cconf: dict) -> Optional[str]:
    """ Get the value for the extend keyword from the configuration. """
    clo = cconf.get("CLO")
    clu = cconf.get("CLU")
    clb = cconf.get("CLB")
    if clo or clu or clb:
        if clo and not clu:   return 'max'
        elif clu and not clo: return 'min'
        else:                 return 'both'
    return None

def selectSpatialCoordinates(ds: xr.Dataset, grid: Optional[xr.Dataset]) -> Tuple[xr.DataArray, xr.DataArray]:
    """ Select spatial coordinates from data or grid file. """
    if isinstance(grid, xr.Dataset):
        if not 'ncells' in ds.dims: raise ValueError("[472] No dimension 'ncells' in grid file.")
        x = grid.clon/np.pi*180
        y = grid.clat/np.pi*180
    else:
        if not ('lat' in ds.dims and 'lon' in ds.dims):
            raise ValueError("[414] No dimension 'lat' or 'lon' in output file.")
        x = ds.lon
        y = ds.lat
    return (x, y)

def treatData(data: xr.Dataset, visConf: dict) -> xr.Dataset:
    """
    Apply some data treatment routines to the data:
    NaNs will be treated and for logscale plots, values <= 0 are masked.
    :param data: data to process
    :param visConf: visualization config
    """
    logger = logging.getLogger(__name__)
    cconf = visConf.get("COLORBAR", {})
    variables = visConf["VARIABLES"]
    if cconf.get("SCALE") == 'log':
        for varname in variables:
            z = data[varname]
            if z.min() <= 0.0:
                #-- Treat all values <= zero in the log-scale.
                logger.debug(f"Masking non-positive values of {varname!r} for logscale.")
                nonzeros = z > 0
                data[varname] = z.where(nonzeros)
    elif data.isnull().any(): data = treatNaNs(data, visConf.get("MISSING_VALUES"))
    return data

def treatNaNs(data: xr.Dataset, nan_treatment: Optional[dict]) -> xr.Dataset:
    """ Treat NaNs according to the config. """
    logger = logging.getLogger(__name__)
    if (nan_treatment == None or not
        ("FILL" in nan_treatment or
         "ERROR" in nan_treatment)):
            raise RuntimeError("[414] NaNs encountered in data but no treatment available.")
    if "ERROR" in nan_treatment:
        logger.error(data)
        raise ValueError("[421] NaNs in the data and not allowed by the NAN treament switch in visualizations/default.yml!")
    fillval = nan_treatment["FILL"]
    logger.info(f"Replacing NaNs with 0 in data set.")
    return data.fillna(fillval)

def addColors2Colormap(color, cmap: mcl.Colormap, ncolorlevels: int, symmetric: bool) -> Tuple[mcl.Colormap, int]:
    """ Add a color to the colormap. """
    colors = [ cmap(l) for l in np.linspace(0,1,ncolorlevels) ]
    newColor = mcl.get_named_colors_mapping()[color]
    if symmetric:
        idx = int(ncolorlevels/2)
        if ncolorlevels%2 == 0:
            colors.insert(idx, newColor)
            colors.insert(idx, newColor)
            ncolorlevels += 2
        else: colors[idx] = newColor
    else:
        colors.insert(0, newColor)
        ncolorlevels += 1
    cmap = mcl.ListedColormap(colors)
    return cmap, ncolorlevels

def createColormap(cconf: dict) -> mcl.Colormap:
    """ Create the colormap corresponding to the colorbar config. """
    ncolorlevels = int(cconf.get("NLEVELS", 15))
    zero2color   = cconf.get("ZERO2COLOR")
    scale        = cconf.get("SCALE", "linear")
    clo = cconf.get("CLO")
    clu = cconf.get("CLU")
    clb = cconf.get("CLB")
    cmap = plt.get_cmap(cconf.get("CMAP"), ncolorlevels)
    if zero2color:
        if scale == 'linear':
            cmap, colorlevels = addColors2Colormap(zero2color, cmap, ncolorlevels, symmetric=False)
        elif scale == 'symmetric' or scale == 'symlog':
            cmap, colorlevels = addColors2Colormap(zero2color, cmap, ncolorlevels, symmetric=True)
        else: colorlevels = ncolorlevels
        if colorlevels != ncolorlevels: cconf["NLEVELS"] = colorlevels
    cmap.set_extremes(bad=clb, under=clu, over=clo)
    return cmap

def setColorScale(z: xr.DataArray, cconf: dict) -> mcl.Normalize:
    """ Set normalization and colormaps for color scaling. """
    scale = cconf.get("SCALE", "linear")
    lim   = cconf.get("LIM")
    kwargs_norm = {}
    if isinstance(lim, list) and len(lim) == 2 and scale != 'symmetric':
        kwargs_norm['vmin'] = lim[0]
        kwargs_norm['vmax'] = lim[1]
    #-- ----- Create Normalize object for the appropriate scale. ----- --#
    if scale == 'linear':
        norm = mcl.Normalize(**kwargs_norm)
    elif scale == 'symmetric':
        if lim != None:
            if isinstance(lim, list): kwargs_norm['halfrange'] = max(lim)
            else: kwargs_norm['halfrange'] = lim
        norm = mcl.CenteredNorm(**kwargs_norm)
    elif scale == 'log':
        norm = mcl.LogNorm(**kwargs_norm)
    elif scale == 'symlog':
        # TODO: set the linear scale to reflect the zero2color range
        # kwargs_norm['linscale'] = ...
        nonzeros = z > 0
        linthresh = float(z.where(nonzeros).min())
        norm = mcl.SymLogNorm(linthresh=linthresh, **kwargs_norm)
    else: raise NotImplementedError(f"[900] Scale {scale!r} is not implemented.")
    norm.autoscale_None(z)
    return norm

def columnBurden(ds: xr.Dataset, variables: list[str]) -> xr.Dataset:
    """
    Calculate the column burden of the respective quantity.
    Reduces the variables along the height level dimension.
    """
    if not 'rho' in ds: raise KeyError("[414] The data set requires the air density ('rho') for calculating column burden.")
    vnHeight = ""
    for heightVarName in VAR_NAMES_HEIGHT:
        if heightVarName in ds:
            vnHeight = heightVarName
            break
    if len(vnHeight) == 0: raise KeyError(f"[414] The data set requires a height {VAR_NAMES_HEIGHT!r} for calculating column burden.")
    levels = ds[vnHeight]
    dimLvl = getSingleLevelDim(levels)
    for varname in variables:
        data = ds[varname]
        dimData = getSingleLevelDim(data)
        if dimLvl not in data.dims:
            if len(levels[dimLvl]) == len(data[dimData]) + 1:
                levels = (levels.shift({dimLvl : -1})[:-1] + levels) / 2
            levels = levels.rename({dimLvl : dimData})
            levels.attrs = ds[dimData].attrs
            dimLvl = dimData
        #-- Now levels are aligned with dimension dimData
        prod = data * ds['rho']
        kwargs = {'even' : 'first'}
        res = -1 * xr.apply_ufunc(integrate.simpson, prod, levels, input_core_dims=[[dimData], [dimData]], kwargs=kwargs)
        #-- Units
        units = Units(levels.attrs.get("units", "m"), data.attrs.get("units", ""), ds['rho'].attrs.get("units", "kg m-3"))
        res.attrs = {
            "standard_name" : "{:s}_column_burden".format(data.attrs.get('standard_name', '')),
            "long_name"     : "Column burden of {:s}".format( data.attrs.get("long_name", data.attrs.get("standard_name", "")) ),
            "units"         : str(units),
            "number_of_grid_in_reference" : data.attrs.get("number_of_grid_in_reference")
        }
        ds[varname] = res
    return ds[variables]

def applyMultiplicators(ds: xr.Dataset, multiply, grid: Optional[xr.Dataset]) -> xr.Dataset:
    """ Apply all terms of the MULTIPLY section. """
    if not isinstance(multiply, list): multiply = [multiply]
    for m in multiply:
        if isinstance(m, int) or isinstance(m, float):
            ds = xr.apply_ufunc(np.multiply, ds, m, keep_attrs=True)
            for var in ds.variables:
                adjustUnits(ds[var], 1./m)
        elif isinstance(m, str):
            if m == 'area' or m == 'cell_area':
                # TODO: what about cases where the grid is in the file itself?
                if not isinstance(grid, xr.Dataset): raise ValueError("[414] Shall multiply with cell area but grid is not given")
                # TODO: check if this needs to be more flexible
                dimNameCells = 'ncells'
                area = grid['cell_area']
                if dimNameCells not in area.dims and 'cell' in area.dims:
                    area = area.rename({'cell' : dimNameCells})
                ds = xr.apply_ufunc(np.multiply, ds, area, keep_attrs=True)
                for var in ds.variables:
                    #-- Units of the area field should be used but these units are wrong!
                    # adjustUnits(ds[var], area.attrs['units'])
                    adjustUnits(ds[var], "m2")
            # TODO: other special cases
            else: raise NotImplementedError(f"[900] Multiplier specified by str {m!r} not supported!")
        else: raise NotImplementedError(f"[900] Multiplier {m!r} not supported!")
    return ds

def applySelections(ds: xr.Dataset, visConf: dict, vars: Optional[list[str]]=None) -> xr.Dataset:
    """
    Select the required dimensions from a dataset according
    to the SELECT and ISELECT sections in the plot config.
    :param ds: dataset to be modified
    :param visConf: current section of the plot config dict
    :param vars: apply only the selections for the specified variables
    :returns: DataArray with selected and squeezed dimensions
    """
    logger = logging.getLogger(__name__)
    for dim in ds.dims:
        #-- Skip dimension if planar dimension or skippable.
        if isinstance(vars, list) and dim not in vars: continue
        if dim in DIM_NAMES_PLANE: continue
        selection = None
        sel_expr = visConf.get("SELECT", {}).get(dim)
        sel_type = 'index'
        #-- ----- Parse selection expressions ----- --#
        if sel_expr == None:
            #-- Default case
            red_expr = visConf.get("REDUCE", {})
            if (('TIME' in red_expr and dim == 'time') or
                ('LEVEL' in red_expr and dim in DIM_NAMES_LEVELS)):
                continue
            #-- If no reduce section is given, select last/lowest.
            selection = -1
        elif isinstance(sel_expr, list):
            selection = sel_expr
            if not isinstance(sel_expr, int): sel_type = 'value'
        elif isinstance(sel_expr, int):
            selection = sel_expr
        elif isinstance(sel_expr, str):
            #-- A few string-expressions are supported.
            if sel_expr == 'all':
                continue
            elif (sel_expr == 'sfc' or
                  sel_expr == 'surface' or
                  sel_expr == 'last'):
                selection = -1
            elif (sel_expr == 'first' or
                  sel_expr == 'toa'):
                selection = 0
            else:
                logger.info(f"Parsing {sel_expr!r} as slice.")
                selection = str2slice(sel_expr)
        else:
            selection = sel_expr
            sel_type = 'value'
        #-- ----- Apply selection expression ----- --#
        if selection == None: raise ValueError("[411] No valid selection expression available.")
        d = { dim : selection }
        logger.debug(f"Selection: {d}")
        if   sel_type == 'index': ds = ds.isel(d)
        elif sel_type == 'value': ds = ds.sel(d)
        if ds.sizes.get(dim, 0) == 1: ds = ds.squeeze(dim, drop=True)
    return ds

def getSingleLevelDim(da: xr.DataArray) -> str:
    """ Get the only variable name corresponding to height levels. """
    dimsLevels = getLevelDims(da)
    if len(dimsLevels) != 1:
        raise ValueError(f"[472] Appropriate dimension cannot be found: {da.dims} -> {dimsLevels}")
    return dimsLevels[0]

def getLevelDims(ds: Union[xr.Dataset, xr.DataArray]) -> list[str]:
    """ Get a list of variable names corresponding to height levels. """
    levelDims = []
    for dim in DIM_NAMES_LEVELS:
        if dim in ds.dims: levelDims.append(dim)
    return levelDims

def getFilesList(visConf: dict, expconf: FullExperimentConfig) -> list[ResultFile]:
    """ Get a list of output files to be plotted. """
    logger = logging.getLogger(__name__)
    PATTERNS = {"DOM" : "[0-9][0-9]",
                "LEV" : "[MHPI]",
                "NR"  : "[0-9][0-9][0-9][0-9]"}
    basename  = visConf["BASENAME"]
    variables = visConf["VARIABLES"]
    #-- Variables is required to be a list object.
    if not isinstance(variables, list):
        if isinstance(variables, str): variables = [variables]
        else: raise ValueError("[411] Variables need to be specified as a list or a single one as string!")
    select_files = visConf.get("WHICH", "last")
    
    #-- ----- Find files ----- --#
    if isinstance(select_files, dict):
        if "DOM" in select_files: PATTERNS["DOM"] = "{0:02d}".format(select_files["DOM"])
        if "LEV" in select_files: PATTERNS["LEV"] = "{0:1s}".format(select_files["LEV"])
        if "NR"  in select_files: PATTERNS["NR"]  = "{0:04d}".format(select_files["NR"])
        select_files = select_files.get("WHICH", "last")
    PATTERN = visConf.get("PATTERN", "_DOM{DOM:s}_{LEV:s}L_{NR:s}.nc")
    PATTERN = PATTERN.format(**PATTERNS)
    logger.info(f"Plotting files matching the pattern '{basename}{PATTERN}'.")
    #-- Get result files list.
    workflowdir = Path(expconf.WORKFLOWDIR, expconf.SDATE, expconf.MEMBER)
    workflowdir.mkdir(parents=True, exist_ok=True)
    fnResultFiles = workflowdir / "resultFiles.pkl"
    resultFiles = ifh.createResultFilesList(expconf, fnResultFiles)
    filesList = []
    for file in resultFiles:
        if fnmatch.fnmatchcase(file.getFilename(), basename + PATTERN): filesList.append(file)
    if len(filesList) == 0:
        logger.warning(f"No files found matching the pattern '{basename}{PATTERN}'.")
        return []
    filesList.sort(key=lambda file: file.getFilename())
    if select_files == 'last':
        files2plot = filesList[-1:]
    elif select_files == 'all':
        files2plot = filesList
    else: raise ValueError(f"[411] Section 'WHICH' currently only allows 'last' and 'all', not {select_files!r}.")
    #-- Sanity check: are all variables to plot in the file.
    for file in files2plot:
        anyVarPresent = False
        for var in variables:
            if var in file.vars: anyVarPresent = True
            else: logger.warning(f"Variable {var!r} not present in file {file!r}.")
        if not anyVarPresent:
            logger.error("Variables:")
            logger.error(variables)
            logger.error("Files:")
            logger.error(files2plot)
            raise RuntimeError("[414] No variable found in the files to plot!")
    return files2plot

def updateConfig(orig: dict, new: dict) -> dict:
    """
    Update the config - a nested dict - with a new
    config - another nested dict.
    :param orig: original dict
    :param new: new dict with updated values
    :returns: dict including the updates
    """
    for key, value in new.items():
        #-- Recurse if the current value is itself a dict.
        if isinstance(value, dict):
            orig[key] = updateConfig(orig.get(key, {}), value)
        else: orig[key] = value
    return orig

def readVisConfig(defConfig: Union[str, Path], specConfig: Union[str, Path], plotType: str) -> dict:
    """
    Read the full visualization configuration from the default and experiment specific
    files and return the fully merged config as a dict.
    :param defConfig: file name or path to default config file
    :param specConfig: file name or path to the specific config file
    """
    with open(defConfig, 'r') as stream:
        defaultConfig = yaml.safe_load(stream)
    with open(specConfig, 'r') as stream:
        specificConfig = yaml.safe_load(stream)
        if "PLOTS" in specificConfig:
            specificConfig = specificConfig[plotType]
    fullConfig = {}
    for varGroup, groupConfig in specificConfig.items():
        fullConfig[varGroup] = updateConfig(copy.deepcopy(defaultConfig), groupConfig)
    return fullConfig

def plotVarGroup(plotDataset: Callable,
                 varGroup: str,
                 visConf: dict,
                 domains: list[IconDomain],
                 expconf: FullExperimentConfig,
                 concatTimes=False):
    """ Plot the variable group with the respective configuration. """
    logger = logging.getLogger(__name__)
    variables = visConf["VARIABLES"]
    files2plot = getFilesList(visConf=visConf, expconf=expconf)
    if len(files2plot) == 0: return
    outputPath = Path(expconf.OUTDIR, expconf.SDATE, expconf.MEMBER)
    grid = None
    ds2plot = None
    dslist = []
    lvl_reduce = visConf.get("REDUCE", {}).get("LEVEL",'')
    time_reduce = visConf.get("REDUCE", {}).get("TIME",'')
    #-- Select appropriate variables.
    for resFile in files2plot:
        logger.info(f"Loading dataset {resFile!r}.")
        ds = xr.load_dataset(resFile.getFilepath())
        #-- If column burden is requested, rho and height need to be selected as well.
        if lvl_reduce.upper() == 'COLUMN_BURDEN':
            selVars = variables.copy()
            for heightVarName in VAR_NAMES_HEIGHT:
                if heightVarName in ds:
                    selVars.append(heightVarName)
                    break
            selVars.append('rho')
        else: selVars = variables
        #-- Drop all variables that should not be plotted.
        ds = ds[selVars]
        #-- Get grid data if not contained in dataset.
        if 'ncells' in ds.dims and not isinstance(grid, xr.Dataset):
            if domains[0].type() == 'R':
                gridFile = domains[1].getGrid()
            else:
                gridFile = domains[0].getGrid()
            grid = xr.load_dataset(gridFile.getFilepath())
        #-- Apply level selection and reduction.
        logger.debug("Applying selections.")
        ds = applySelections(ds, visConf)
        if len(lvl_reduce) > 0:
            logger.debug(f"Applying level reduction {lvl_reduce!r}.")
            if lvl_reduce.upper() == 'COLUMN_BURDEN':
                ds = columnBurden(ds, variables)
            else:
                lvl_reduce_func = getattr(np.ndarray, lvl_reduce)
                for dim in getLevelDims(ds):
                    ds = ds.reduce(lvl_reduce_func, dim=dim, keep_attrs=True)
        #-- Apply multiplication.
        multiply = visConf.get("MULTIPLY")
        if multiply: ds = applyMultiplicators(ds, multiply, grid)
        if len(time_reduce) == 0 and not concatTimes:
            #-- Plot time frame now.
            logger.debug("Plotting dataset.")
            plotDataset(ds=ds, varGroup=varGroup, visConf=visConf, grid=grid,
                        triangGrid=isinstance(grid, xr.Dataset), outputPath=outputPath)
        else:
            #-- Store for plotting after collection of all files.
            dslist.append(ds)
    if len(time_reduce) > 0 or concatTimes:
        #-- Run the actual plotting routine if time was reduced.
        #-- Otherwise the plotting routine is already called on each time frame.
        ds2plot = xr.concat(dslist, dim='time')
        if 'time' in ds2plot.dims and len(time_reduce) > 0:
            logger.debug(f"Applying time reduction on concatenated set {time_reduce!r}.")
            ds2plot = ds2plot.reduce(getattr(np.ndarray, time_reduce), dim='time', keep_attrs=True)
        logger.debug("Plotting concatenated dataset.")
        plotDataset(ds=ds2plot, varGroup=varGroup, visConf=visConf, grid=grid,
                    triangGrid=isinstance(grid, xr.Dataset), outputPath=outputPath)

def processPlots(expconf: FullExperimentConfig, plotType: str, plotDatasetFunc: Callable, concatTimes=False):
    """ Main routine for processing the plots. """
    logger = logging.getLogger(__name__)
    plt.set_loglevel("info")
    expconf.check(level="member", check_jobname=True)
    #-- ----- Plot config ----- --#
    config_path_def  = Path(expconf.LOGDIR, "default_" + expconf.JOBNAME.removeprefix(expconf.EXPID + "_"))
    config_path_spec = Path(expconf.LOGDIR, expconf.EXPNAME + "_" + expconf.JOBNAME.removeprefix(expconf.EXPID + "_"))
    fullConfig = readVisConfig(config_path_def, config_path_spec, plotType)
    #-- Domain info
    fn_domains = Path(expconf.WORKFLOWDIR, expconf.SDATE, "domains.pkl")
    if fn_domains.exists():
        logger.debug("Reading domain info.")
        domains = ifh.loadDomains(fn_domains)
    else: raise FileNotFoundError(f"[412] Domain file '{fn_domains}' not present!")

    #-- All the plotting is done in the working directory of the simulation.
    os.chdir( Path(expconf.OUTDIR, expconf.SDATE, expconf.MEMBER) )
    for varGroup, visConf in fullConfig.items():
        logger.info(f"Processing variable group {varGroup!r}.")
        #-- Update rcParams
        for group, properties in visConf.get("RCPARAMS", {}).items():
            if group == 'figure' and 'figsize_cm' in properties:
                properties['figsize'] = [ v/2.54 for v in properties.pop('figsize_cm')]
            mpl.rc(group, **properties)
        plotVarGroup(plotDatasetFunc, varGroup, visConf, domains, expconf, concatTimes=concatTimes)
        mpl.rcdefaults()
