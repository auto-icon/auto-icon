#!/usr/bin/python3

#-- > Utility functions for ICON specify stuff. < --#

# Copyright 2023 - 2024 Karlsruhe Institute of Technology (KIT) and Ludwig-Maximilians-Universität München (LMU)
# SPDX-FileContributor: Andreas Baer
#
# SPDX-License-Identifier: BSD-3-Clause

from ast import literal_eval
import ast
from datetime import datetime
import logging
from os import path
from pathlib import Path
import re
import shutil
import tempfile
from typing import Union
from urllib.error import URLError
import urllib.request


def optionalstr(s: str, default: str) -> str:
    """ Return a string or, if empty, a default string. """
    if len(s) == 0: return default
    else: return str(s)
    
def optionalint(s: str, default=-1) -> int:
    """ Return an int for an optionally empty string. """
    if len(s) == 0: return default
    else: return int(s)
    
def optionalbool(s: str, default=False) -> bool:
    """ Return an int for an optionally empty string. """
    if len(s) == 0: return default
    else: return bool(ast.literal_eval(s))
    
def optionalfloat(s: str, default=-1.0) -> float:
    """ Return a float for an optionally empty string. """
    if len(s) == 0: return default
    else:
        try:
            res = float(s)
        except ValueError:
            res = eval(s)
    return res

def timestepStr2timestep(dtstr: str) -> Union[str, int, float]:
    """ Evaluate input string to a time step in seconds (int|float) or ISO time period. """
    try:
        timestep = int(dtstr)
    except ValueError:
        try:
            timestep = float(dtstr)
        except ValueError:
            try:
                timestep = eval(dtstr)
            except NameError:
                timestep = str(dtstr)
    return timestep

def sdate2date(date: str) -> datetime:
    """
    Simple wrapper to savely format an autosubmit date to a datetime date.
    :param date: autosubmit date
    :returns: datetime object
    """
    dateformat = "%Y" + "%m" + "%d"
    if len(date) > 8: dateformat += "%H"
    return datetime.strptime(date, dateformat)

def idate2date(date: int) -> datetime:
    """
    Simple wrapper to savely format a date from an 8 or 10 digit integer.
    :param date: 8 or 10 digit integer
    :returns: datetime object
    """
    return sdate2date(f"{date:d}")

def filetype2format(ft: int) -> str:
    """
    Simple wrapper to obtain the file format string from the ICON filetype.
    :param ft: filetype integer
    :returns: extension string
    """
    if ft == 2: return "grb"
    elif ft == 4 or ft == 5: return "nc"
    else: return ""

def str2slice(strrep: str) -> slice:
    """ Convert a string representation (e.g. '3:17:2') to a slice. """
    l = []
    for val in strrep.split(':'):
        if len(val) == 0: l.append(None)
        else: l.append(int(val))
    return slice(*l)

def checkCleanDir(p: Path) -> bool:
    """
    Check if a given Path is clean, i.e. does not exist or is
    an empty directory.
    """
    if not p.exists(): return True
    elif not p.is_dir(): return False
    l = list(p.glob("*"))
    return (len(l) == 0)

def downloadFile(url: str, target_dir, overwrite=True, raiseOnFailure=True, loglvl=logging.INFO) -> str:
    """
    Download the file at a given URL to a temporary file and move it to the
    target directory afterwards. The file name is given by the last part of
    the URL.
    :param url: URL to the file to be downloaded
    :param target_dir: target directory for the file
    :param overwrite: silently overwrite an existing file in the target dir
    :param raiseOnFailure: raise an exception if the download fails
    :returns
    """
    logger = logging.getLogger(__name__)
    logger.setLevel(loglvl)

    fn = Path(url).name
    if isinstance(target_dir, str): target_dir = Path(target_dir)
    # TODO: generalize to allow file names for the target
    if not (target_dir.exists() and target_dir.is_dir()):
        raise FileNotFoundError(f"[512] Target directory {target_dir!r} does not exist!")
    target = target_dir / fn
    if target.exists() and not overwrite:
        raise FileExistsError(f"[414] File {target!r} already exists and should not be overwritten!")
    #-- Download file
    logger.debug(f"Trying to download {url!r} to '{target}'.")
    try:
        with urllib.request.urlopen(url) as response:
            with tempfile.NamedTemporaryFile(delete=False) as tmpFile:
                shutil.copyfileobj(response, tmpFile)
    except URLError as e:
        if raiseOnFailure: raise e
        else: return ""
    logger.info(f"Downloaded {url!r} as {target}.")
    return shutil.move(tmpFile.name, str(target))

def substitutePlaceholders(s: str, sdate: Union[str, datetime], loglvl=logging.INFO) -> str:
    """
    Substitute placeholders in a string:
    Placeholders are of the form <NAME> and will be replaced
    by associated details of the member. Supported are
        YEAR, MONTH, DAY, HOUR -> relating to sdate
    :param s: string with placeholders
    :param sdate: start date of the simulation (str or datetime)
    :returns: string with substituted placeholders
    """
    logger = logging.getLogger(__name__)
    logger.setLevel(loglvl)

    if isinstance(sdate, str): d = sdate2date(sdate)
    elif isinstance(sdate, datetime): d = sdate
    else: raise TypeError(f"[512] Date {sdate!r} of type {type(sdate)} is not supported.")
    def date_lookup(matchobj):
        p = matchobj.group(1)
        if   p == "YYYYMMDDHH":
            return f"{d.year:04d}{d.month:02d}{d.day:02d}{d.hour:02d}"
        elif p == "DATE"  or p == "YYYYMMDD":
            return f"{d.year:04d}{d.month:02d}{d.day:02d}"
        elif p == "YEAR"  or p == "YYYY":     return f"{d.year:04d}"
        elif p == "MONTH" or p == "MM":       return f"{d.month:02d}"
        elif p == "DAY"   or p == "DD":       return f"{d.day:02d}"
        elif p == "HOUR"  or p == "HH":       return f"{d.hour:02d}"
        else: raise ValueError(f"[411] Unknown placeholder {p!r}.")
    res = re.sub('<([A-Z]+)>', date_lookup, s)
    logger.debug("Substituting placeholders:")
    logger.debug(f"{s!r} -> {res!r}")
    return res

def readFromAsList(source: str, expandPath=False) -> list:
    """
    Convert an autosubmit list (python print of a list) to a python list.
    :param source: String with the content, usually an autosubmit placeholder.
    """
    if len(source) == 0:
        return []
    else:
        return [ path.expandvars(path.expanduser(obj)) for obj in ast.literal_eval(source) ]

#-- ----- Testing ----- --#
if __name__ == "__main__":
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)
    
    datestr = "2014112703"
    date = sdate2date(datestr)
    templatestrings = ["ABC", "A<>B", "A<YEAR>B<YYYYMMDD>C<HH>D"]
    for templatestr in templatestrings:
        finalstr = substitutePlaceholders(templatestr, date)
