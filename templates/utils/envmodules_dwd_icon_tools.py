#!/usr/bin/python3

#-- > Set up HPC environment for DWD icon tools. < --#

# Copyright 2023 - 2024 Karlsruhe Institute of Technology (KIT) and Ludwig-Maximilians-Universität München (LMU)
# SPDX-FileContributor: Andreas Baer
#
# SPDX-License-Identifier: BSD-3-Clause

import os
import resource
from types import SimpleNamespace


envNamespace = SimpleNamespace()
SLURM_NTASKS = int(os.environ["SLURM_NTASKS"])
SLURM_JOB_NUM_NODES = int(os.environ["SLURM_JOB_NUM_NODES"])
SLURM_CPUS_ON_NODE = int(os.environ["SLURM_CPUS_ON_NODE"])
mpi_procs_pernode = int(os.environ.get("SLURM_NTASKS_PER_NODE",
                                       SLURM_NTASKS / SLURM_JOB_NUM_NODES) )
OMP_STACKSIZE = 2000

#-- Openmp environment variables
envNamespace.OMP_NUM_THREADS = int(SLURM_CPUS_ON_NODE / mpi_procs_pernode)
envNamespace.ICON_THREADS = envNamespace.OMP_NUM_THREADS
envNamespace.OMP_SCHEDULE = "dynamic,1"
envNamespace.OMP_DYNAMIC = "false"
envNamespace.OMP_STACKSIZE = f"{OMP_STACKSIZE}M"
envNamespace.OMPI_MCA_pml = "ucx"
envNamespace.OMPI_MCA_btl = "self"
envNamespace.OMPI_MCA_osc = "pt2pt"

envNamespace.KMP_AFFINITY = "verbose,granularity=core,compact,1,1"
envNamespace.KMP_LIBRARY = "turnaround"
envNamespace.KMP_KMP_SETTINGS = 1
envNamespace.OMP_WAIT_POLICY = "active"
envNamespace.I_MPI_FABRICS = "shm:dapl"
envNamespace.I_MPI_DAPL_UD = "enable"
envNamespace.I_MPI_DAPL_UD_PROVIDER = "ofa-v2-mlx5_0-1u"
envNamespace.DAPL_UCM_REP_TIME = 8000
envNamespace.DAPL_UCM_RTU_TIME = 4000
envNamespace.DAPL_UCM_CQ_SIZE = 1000
envNamespace.DAPL_UCM_QP_SIZE = 1000
envNamespace.DAPL_UCM_RETRY = 10
envNamespace.DAPL_ACK_RETRY = 10
envNamespace.DAPL_ACK_TIMER = 20
envNamespace.DAPL_UCM_TX_BURST = 100
envNamespace.DAPL_WR_MAX = 500

# Set stack limit
resource.setrlimit(resource.RLIMIT_STACK, (OMP_STACKSIZE * 1000000,-1))

env = envNamespace.__dict__
for key in env.keys():
    if not isinstance(env[key], str):
        env[key] = str(env[key])

cmd = ["srun",
       "--kill-on-bad-exit=1",
       f"--nodes={SLURM_JOB_NUM_NODES}",
       "--cpu_bind=verbose,cores",
       "--distribution=block:block",
       f"--ntasks={SLURM_NTASKS}",
       f"--ntasks-per-node={mpi_procs_pernode}",
       f"--cpus-per-task={env['OMP_NUM_THREADS']}",
       "--propagate=STACK"]
