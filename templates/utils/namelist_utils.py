#!/usr/bin/python3

#-- > Prepare the namelist for an experiment (yaml namelists). < --#

# Copyright 2023 - 2024 Karlsruhe Institute of Technology (KIT) and Ludwig-Maximilians-Universität München (LMU)
# SPDX-FileContributor: Andreas Baer
#
# SPDX-License-Identifier: BSD-3-Clause

import argparse
import shutil
import re
import yaml
import f90nml
import logging
from pathlib import Path
from typing import Union
from dateutil.relativedelta import relativedelta
from ast import literal_eval
from types import SimpleNamespace

from experiment_config import ExperimentConfig, FullExperimentConfig
from icon_file import GridFile, JsbachFile
from python_utils import sdate2date, timestepStr2timestep
import icon_file_handler as ifh


def loadYamlSection(fn_yaml: Union[str, Path], section) -> dict:
    """
    Simple wrapper to load a yaml file and retreive a
    specified section.
    :param fn_yaml: file name of the yaml file to read
    :param section: section of the namelist yaml file to be processed.
    :return: dictionary of parameters
    """
    if section == None: raise ValueError("[510]: Empty section name to be retrieved.")
    with open(fn_yaml, 'r') as stream:
        return yaml.safe_load(stream).get(section, {})

def nmlGroupSanityCheck(group: str, content: dict, expconf: FullExperimentConfig):
    """
    Perform sanity checks of some namelist groups that cause ICON to abort or
    result in undefined states. The list of checks can be extended but should
    be kept to strikt checks only.
    All tests are performed consecutively, if a test fails, the job is aborted.
    Non-strict checks should only issue warnings.
    :param group: namelist group
    :param content: dict with the key-value pairs
    :param expconf: namespace with configuration entries from autosubmit
    """
    logger = logging.getLogger(__name__)
    expconf.check(level="chunk")
    #-- Select the test by group.
    if group == 'output_nml':
        #-- Check for uniquness of 'file_interval' and 'steps_per_file'
        if ('file_interval' in content.keys() and content['file_interval'] != ""
            and 'steps_per_file' in content.keys() and content['steps_per_file'] >= 0):
            raise ValueError("[420] 'steps_per_file' and 'file_interval' are both provided! Check your experiment config and namelist files.")
        #-- Check if output date is included in filenames for REINIT runs.
        if expconf.REINIT:
            hasdate = False
            fmt = content.get('filename_format')
            if fmt != None:
                if ('datetime' in fmt or 'ddhhmmss' in fmt): hasdate = True
            fno = content.get('output_filename')
            if fno != None:
                if expconf.SDATE in fno: hasdate = True
                else:
                    pattern = "([0-9]{4})"
                    #-- Calculate ENDDATE
                    if expconf.CHUNKSIZEUNIT == "HOUR":
                        pattern = pattern + "[-_]?([0-9]{2})[-_]?([0-9]{2})[-_]?([0-9]{2})"
                    elif expconf.CHUNKSIZEUNIT == "DAY":
                        pattern = pattern + "[-_]?([0-9]{2})[-_]?([0-9]{2})"
                    elif expconf.CHUNKSIZEUNIT == "MONTH":
                        pattern = pattern + "[-_]?([0-9]{2})"
                    if re.search(pattern, fno): hasdate = True
            if not hasdate:
                logger.warning("Output file does not seem to have a date!")
                if fmt != None: logger.warning(f"filename_format = {fmt!r}")
                if fno != None: logger.warning(f"filename_format = {fno!r}")
    if group == 'grid_nml':
        #-- Check if a grid filename is provided
        if not 'dynamics_grid_filename' in content.keys():
            raise ValueError("[420] No grid filename present! Check the namelist and GRID section of your experiment config file.")
    #-- No test failed, everything went fine.
    return
            
def params2nml(parameters: dict, expconf: FullExperimentConfig) -> f90nml.Namelist:
    """
    Convert a dictionary of namelist groups into a f90nml Namelist
    object. Some scrutinizations are implemented:
    output_nml[*] will be converted to an output_nml group
    master_model_nml[*] will be converted to an master_model_nml group
    :param parameters: dictionary with all namelist groups
    :param expconf: namespace with configuration entries from autosubmit
    :return: f90nml Namelist object
    """
    nml = f90nml.Namelist()
    for group, content in parameters.items():
        if group.startswith('output_nml'): group = 'output_nml'
        elif group.startswith('master_model_nml'): group = 'master_model_nml'
        nmlGroupSanityCheck(group, content, expconf)
        nml.add_cogroup(group, content)
    return nml

def applyPatches(nml: dict, patches: list, overwrite=False):
    """
    Simple wrapper function to apply a list of patches.
    :param nml: namelist (python dict) to apply the patch to
    :param patch: list of dicts with the namelist patch in the form {'group_nml' : {'param' : 'value'}} or single patch
    :param logger: parent logger instance
    :param overwrite: bool whether to overwrite values if they exist.
    """
    logger = logging.getLogger(__name__)
    if len(patches) == 0: return
    if isinstance(patches, dict):
        #-- If patches is a dict with the patch(es), it can be passed on directly.
        logger.debug("Applying patch:")
        logger.debug(patches)
        applyNmlPatch(nml, patches, overwrite)
    else:
        for patch in patches:
            logger.debug("Applying patch:")
            logger.debug(patch)
            applyNmlPatch(nml, patch, overwrite)

def applyNmlPatch(nml: dict, patch: dict, overwrite=False):
    """
    Apply a namelist patch to a namelist while both are only dictionaries.
    Pre-filled values take precedence unless 'overwrite=True'.
    This allows to specify explicitly values in the yaml definition that
    will not be overwritten by the built-in functionality.
    output_nml[*] will apply all patches from output_nml
    :param nml: namelist (python dict) to apply the patch to
    :param patch: dict with the namelist patch in the form {'group_nml' : {'param' : 'value'}}
    :param logger: parent logger instance
    :param overwrite: bool whether to overwrite values if they exist.
    """
    logger = logging.getLogger(__name__)
    for patch_grp, patch_content in patch.items():
        existing = False
        for nml_grp in nml.keys():
            if nml_grp.startswith(patch_grp):
                existing = True
                #-- Update the nml group dictionary with the patch
                for nkey, nval in patch_content.items():
                    if nkey not in nml[nml_grp] or overwrite:
                        nml[nml_grp][nkey] = nval
                    else:
                        logger.info(f"In namelist group {nml_grp!r}, not overwriting key {nkey!r}'s value {nml[nml_grp][nkey]!r} with new value {nval!r}.")
        if not existing: nml[patch_grp] = patch_content

def str2Type(input: str):
    """
    Evaluate the type of an input string and returns the typed value
    using ast.literal_eval. If no appropriate type is found, returns
    the original string. Credit goes to this stack overflow answer:
    https://stackoverflow.com/a/60213649
    In addition, Fortran boolean types .false. and .true. (case insensitive)
    will be converted to False and True also
    :param input: string to be converted
    :return: returns typed object
    """
    try:
        return literal_eval(input)
    except (ValueError, TypeError, SyntaxError):
        if input.lower() == ".false.": return False
        if input.lower() == ".true.":  return True
        else: return input

def patchFromStr(patch_str: str) -> dict:
    """
    Parse a namelist patch in string syntax (<namelist group>:<parameter>:<value>)
    to obtain the corresponding namelist patch in dict format.
    :param patch_str: namelist patch in string format
    :return: namelist patch in dictionary format
    """
    l = patch_str.split(':')
    return {l[0] : {l[1] : str2Type(l[2])}}

def patchesFromASlist(as_list: str) -> list:
    """
    Take an autosubmit list and convert it 
    to a namelist patch in dict format.
    :param as_list: namelist patch in string format
    :return: namelist patches in dictionary format
    """
    if len(as_list) == 0: return []
    reformat_list = literal_eval( as_list )
    if len(reformat_list) == 0: return []
    patches_list = []
    for itm in reformat_list:
        patch = patchFromStr( itm )
        patches_list.append(patch)
    return patches_list

def createPatchesNudging(p: SimpleNamespace,
                         expconf: FullExperimentConfig):
    """
    Create and add the patches related to global nugding.
    :param p:  patches dictionary (not substituting)
    :param expconf: namespace with configuration entries from autosubmit
    """
    logger = logging.getLogger(__name__)
    expconf.check(level="chunk")
    logger.info("Creating patches for (global) nudging.")
    #-- Static patches for reinit: create output files
    nudging_patches = {
        'nudging_nml' : {
            'nudge_type' : 2,               #-- Global nudging, required for the moment
        },
        'limarea_nml' : {
            'itype_latbc' : 1,
            'dtime_latbc' : expconf.NUDGING_INTERVAL,
            'latbc_boundary_grid' : "",
            'latbc_path' : "ERA5-input",
            'latbc_varnames_map_file' : 'dict.latbc'
        }
    }
    if expconf.NUDGING_RANGE == "full":
        nudging_patches['nudging_nml']['nudge_start_height'] = 0
        nudging_patches['nudging_nml']['nudge_end_height'] = 1000000
    #-- Grid files
    fn_domains = Path(expconf.WORKFLOWDIR, expconf.SDATE, "domains.pkl")
    if fn_domains.exists():
        domains = ifh.loadDomains(fn_domains)
    else: raise FileNotFoundError(f"[412] Domain file '{fn_domains}' not present!")
    if expconf.REINIT and not expconf.CONTINUE_MET:
        datestr = expconf.CHUNKDATE
    else:
        datestr = expconf.SDATE
    if len(datestr) == 8:
        hourstr = "00"
    else:
        hourstr = ""
    for domain in domains:
        f = None
        for eraFile in ["ERAI", "ERA5"]:
            tmpfile = domain.getFile(eraFile, sdate2date(datestr))
            if tmpfile != None and f == None: f = tmpfile
        if f != None:
            nudging_patches['limarea_nml']['latbc_filename'] = f.getFilename().replace(datestr + hourstr, '<y><m><d><h>')
    p.atmo.append( nudging_patches )

def createPatchesReinit(p: SimpleNamespace,
                        expconf: FullExperimentConfig,
                        varlistAero: list[str],
                        varlistChem: list[str],
                        varlistMet: list[str],
                        enddate: str):
    """
    Create and add the patches related to reinitialization runs.
    :param p:  patches dictionary (not substituting)
    :param expconf: namespace with configuration entries from autosubmit
    :param varlistAero: variable list to be continued for aerosols
    :param varlistChem: variable list to be continued for chemistry
    :param varlistMet: variable list to be continued for meteorology
    :param enddate: enddate of the simulation
    """
    logger = logging.getLogger(__name__)
    logger.info("Creating patches for reinitializations.")
    #-- Static patches for reinit: create output files
    reinit_patches = {}
    if expconf.CONTINUE_AERO:
        reinit_patches['output_nml-reinit-aero-auto-icon'] = {
            'filetype' : expconf.FILETYPE,
            'output_filename' : f'auto-icon_icon-art-aero-{enddate}',
            'output_interval' : 'PT01H',
            'file_interval' : '',
            'steps_per_file' : 1,
            'ml_varlist' : varlistAero
            }
    if expconf.CONTINUE_CHEM:
        reinit_patches['output_nml-reinit-chem-auto-icon'] = {
            'filetype' : expconf.FILETYPE,
            'output_filename' : f'auto-icon_icon-art-chem-{enddate}',
            'output_interval' : 'PT01H',
            'file_interval' : '',
            'steps_per_file' : 1,
            'ml_varlist' : varlistChem
            }
    if expconf.CONTINUE_MET:
        reinit_patches['output_nml-reinit-met-auto-icon'] = {
            'filetype' : expconf.FILETYPE,
            'output_filename' : f'auto-icon_icon-art-met-{enddate}',
            'output_interval' : 'PT01H',
            'file_interval' : '',
            'steps_per_file' : 1,
            'ml_varlist' : varlistMet
            }
    #-- Dynamic patches
    CHUNKENDDATE = sdate2date(enddate)
    for key in reinit_patches.keys():
        reinit_patches[key]['output_start'] = CHUNKENDDATE.strftime(expconf.DATEFORMAT_ICON)
        reinit_patches[key]['output_end']   = CHUNKENDDATE.strftime(expconf.DATEFORMAT_ICON)
    p.atmo.append( reinit_patches )

def createPatchesTiming(p: SimpleNamespace,
                        expconf: FullExperimentConfig,
                        xConf: SimpleNamespace) -> SimpleNamespace:
    """
    Create and add the patches related to reinitialization runs.
    :param p:  patches dictionary (not substituting)
    :param expconf: namespace with configuration entries from autosubmit
    :param xConf: namespace with additional configuration arguments
    :returns: SimpleNamespace with output time steps: STARTDATE, ENDDATE
    """
    logger = logging.getLogger(__name__)
    expconf.check(level="chunk")
    logger.info("Creating patches with timing info.")
    #-- Getting autosubmit variables
    NUNITS = xConf.NUMCHUNKS * xConf.CHUNKSIZE

    STARTDATE = sdate2date(expconf.SDATE)
    #-- Calculate ENDDATE
    if expconf.CHUNKSIZEUNIT == "HOUR":
        ENDDATE = STARTDATE + relativedelta(hours=NUNITS)
    elif expconf.CHUNKSIZEUNIT == "DAY":
        ENDDATE = STARTDATE + relativedelta(days=NUNITS)
    elif expconf.CHUNKSIZEUNIT == "MONTH":
        ENDDATE = STARTDATE + relativedelta(months=NUNITS)
    elif expconf.CHUNKSIZEUNIT == "YEAR":
        ENDDATE = STARTDATE + relativedelta(years=NUNITS)
    else:
        raise ValueError(f"[411] Chunk size unit {expconf.CHUNKSIZEUNIT!r} is invalid!")
    output_times = SimpleNamespace(STARTDATE=STARTDATE, ENDDATE=ENDDATE)
    if expconf.REINIT: STARTDATE = sdate2date(expconf.CHUNKDATE)

    #-- Create patches for start and end date
    p.master.append({'master_time_control_nml' :
                     {'experimentStartDate' : STARTDATE.strftime(expconf.DATEFORMAT_ICON),
                      'experimentStopDate'  : ENDDATE.strftime(expconf.DATEFORMAT_ICON)}})

    #-- Create patch for timestep
    #-- Atmosphere
    if len(xConf.dtstr) > 0:
        timestep = timestepStr2timestep(xConf.dtstr)
        if isinstance(timestep, int) or isinstance(timestep, float):
            p.atmo.append({'run_nml' : {'dtime' : timestep}})
        else: p.atmo.append({'run_nml' : {'modelTimeStep' : timestep}})
        #-- NWP physics time steps
        nwp_ts_dict = {}
        for nml_param, mult in xConf.nwp_timesteps_mult.items():
            if len(mult) > 0:
                if not (isinstance(timestep, int) or isinstance(timestep, float)):
                    raise ValueError("[411] Time step was not set but time step multipliers are provided.")
                try:
                    nwp_ts_dict[nml_param] = int(mult) * timestep
                except:
                    logger.warning(f"NWP time step multiplier {nml_param!r} was not specified correctly.")
        p.atmo.append({'nwp_phy_nml' : nwp_ts_dict})
    #-- Ocean
    if len(xConf.dtstrOce) > 0:
        timestep = timestepStr2timestep(xConf.dtstrOce)
        if isinstance(timestep, int) or isinstance(timestep, float):
            p.ocean.append({'run_nml' : {'dtime' : timestep}})
        else: p.ocean.append({'run_nml' : {'modelTimeStep' : timestep}})
    #-- NWP physics time steps
    phy_ts_dict = {}
    for nml_param, val in xConf.phy_timesteps.items():
        if len(val) > 0:
            timestep = timestepStr2timestep(val)
            phy_ts_dict[nml_param] = timestep
    phy_section = 'nwp_phy_nml'
    if ('dt_vdf' in phy_ts_dict or 'dt_mig' in phy_ts_dict or 'dt_two' in phy_ts_dict or
        'dt_car' in phy_ts_dict or 'dt_art' in phy_ts_dict): phy_section = 'aes_phy_nml'
    p.atmo.append({phy_section : phy_ts_dict})
    
    #-- Restart and checkpoint intervals
    RESTART_TIME_INTERVAL = "P%d%s"%(xConf.CHUNKSIZE, expconf.CHUNKSIZEUNIT[0])
    if expconf.CHUNKSIZEUNIT == "HOUR": RESTART_TIME_INTERVAL = "PT" + RESTART_TIME_INTERVAL[1:]
    
    p.master.append({'master_time_control_nml' :
                     {'restartTimeIntVal' : RESTART_TIME_INTERVAL,
                      'checkpointTimeIntVal' : xConf.CHECKPOINT_TIME_INTERVAL}})
    return output_times

def createPatchesGrid(p: SimpleNamespace, expconf: FullExperimentConfig):
    """
    Create and add the patches related to the computation grids.
    :param p:  patches dictionary (not substituting)
    :param expconf: namespace with configuration entries from autosubmit
    """
    logger = logging.getLogger(__name__)
    expconf.check(level="chunk")
    logger.info("Creating patches with grid info.")
    #-- Grid files
    fn_domains = Path(expconf.WORKFLOWDIR, expconf.SDATE, "domains.pkl")
    if fn_domains.exists():
        domains = ifh.loadDomains(fn_domains)
    else: raise FileNotFoundError(f"[412] Domain file '{fn_domains}' not present!")
    #-- A list will properly converted to a nml list.
    fnRadgrid = ""
    fnGridList = []
    fnGridOceList = []
    if expconf.REINIT and not expconf.CONTINUE_MET:
        datestr = expconf.CHUNKDATE
    else:
        datestr = expconf.SDATE
    for domain in domains:
        gridFile = domain.getFile("GRID")
        if gridFile == None: continue
        if not isinstance(gridFile, GridFile): raise ValueError(f"[511] Grid file not valid for domain {domain!r}")
        if gridFile.type == 'R':
            fnRadgrid = gridFile.getFilepath()
        elif gridFile.type == 'O':
            fnGridOceList.append(gridFile.getFilepath())
        else:
            fnGridList.append(gridFile.getFilepath())
        f = None
        for eraFile in ["ERAI", "ERA5"]:
            tmpfile = domain.getFile(eraFile, sdate2date(datestr))
            if tmpfile != None and f == None: f = tmpfile
        if f != None:
            p.atmo.append({'initicon_nml' : {'ifs2icon_filename' : f.getFilepath()}})
    
    p.atmo.append({'grid_nml' :
                  {'dynamics_grid_filename' : fnGridList}})
    if len(fnGridOceList) > 0:
        p.ocean.append({'grid_nml' :
                     {'dynamics_grid_filename' : fnGridOceList}})
    if len(fnRadgrid) > 0:
        p.atmo.append({'grid_nml' :
                      {'radiation_grid_filename' : fnRadgrid,
                       'lredgrid_phys' : True}})

def createPatchesOutput(p: SimpleNamespace,
                        expconf: ExperimentConfig,
                        xConf: SimpleNamespace,
                        timing: SimpleNamespace,
                        ocean=False):
    """
    Create and add the patches related to model output and the filetype.
    :param p:  patches dictionary (not substituting)
    :param xConf: namespace with additional configuration arguments
    :param expconf: namespace with configuration entries from autosubmit
    :param timing: SimpleNamespace with the timing configuration
    :param ocean: ocean is used
    """
    logger = logging.getLogger(__name__)
    logger.info("Creating patches with filetypes and output.")
    #-- Filetype patches
    p.atmo.append({'initicon_nml' : {'filetype' : expconf.FILETYPE},
                  'io_nml'       : {'restart_file_type' : expconf.FILETYPE},
                  'output_nml'   : {'filetype' : expconf.FILETYPE} })
    #-- Create patches for output
    output_patch = {'output_nml' : {'output_start' : timing.STARTDATE.strftime(expconf.DATEFORMAT_ICON),
                                    'output_end'   : timing.ENDDATE.strftime(expconf.DATEFORMAT_ICON),
                                    'output_interval' : expconf.OUTPUT_INTERVAL
                                    } }
    #-- Check that only one of FILE_INTERVAL and STEPS_PER_FILE is provided
    if len(xConf.FILE_INTERVAL) > 0 and xConf.STEPS_PER_FILE != 0:
        raise ValueError("[420] FILE_INTERVAL and STEPS_PER_FILE both provided, but only one should be!")
    if len(xConf.FILE_INTERVAL) > 0:
        output_patch['output_nml']['file_interval'] = xConf.FILE_INTERVAL
    if xConf.STEPS_PER_FILE != 0:
        output_patch['output_nml']['steps_per_file'] = xConf.STEPS_PER_FILE
    p.atmo.append(output_patch)
    if ocean:
        output_patch = {'output_nml' : {'output_start' : timing.STARTDATE.strftime(expconf.DATEFORMAT_ICON),
                                        'output_end'   : timing.ENDDATE.strftime(expconf.DATEFORMAT_ICON),
                                        'output_interval' : expconf.OUTPUT_INTERVAL_OCEAN
                                        } }
        #-- Check that only one of FILE_INTERVAL and STEPS_PER_FILE is provided
        if len(xConf.FILE_INTERVAL_OCEAN) > 0 and xConf.STEPS_PER_FILE_OCEAN != 0:
            raise ValueError('FILE_INTERVAL_OCEAN and STEPS_PER_FILE_OCEAN both provided, but only one should be!')
        if len(xConf.FILE_INTERVAL_OCEAN) > 0:
            output_patch['output_nml']['file_interval'] = xConf.FILE_INTERVAL_OCEAN
        if xConf.STEPS_PER_FILE_OCEAN != 0:
            output_patch['output_nml']['steps_per_file'] = xConf.STEPS_PER_FILE_OCEAN
        p.ocean.append(output_patch)

def createPatchesChunks(ps: SimpleNamespace, expconf: FullExperimentConfig,
                        nmlSnfPatches: str, nmlReinitSubPatches: str):
    """
    Create and add the patches applying not to all chunks.
    :param ps: patches dictionary (substituting)
    :param expconf: namespace with configuration entries from autosubmit
    :param nmlSnfPatches: namelist patches to be substituted for all chunks except the first
    :param nmlReinitSubPatches: namelist patches to be substituted for all reinit chunks except the first
    """
    logger = logging.getLogger(__name__)
    expconf.check(level="chunk", check_chunk_full=True)
    logger.info("Creating patches for specific chunks.")
    #-- Add additional patches in case of reinitializations or for the first chunk
    if not expconf.IS_FIRST_CHUNK:
        ps.atmo.extend( patchesFromASlist(nmlSnfPatches) )
        if expconf.REINIT:
            ps.atmo.extend( patchesFromASlist(nmlReinitSubPatches) )

def createPatchesMaster(ps: SimpleNamespace, expconf: FullExperimentConfig,
                        ntasks: int, ocean=False, jsbach=False, restart=False):
    """
    Create and add the patches related to changes in the master namelist (except timing).
    :param ps: patches dictionary (substituting)
    :param expconf: namespace with configuration entries from autosubmit
    :param ocean: bool whether to use the ocean model
    """
    logger = logging.getLogger(__name__)
    expconf.check(level="chunk")
    logger.info("Creating patches for the master namelist.")
    models = {"atmo"  : {"type" : 1, "nml"  : expconf.NAMELIST_ATMO},
              "ocean" : {"type" : 2, "nml"  : expconf.NAMELIST_OCEAN}}
    modelList = ["atmo"]
    ntasks_atm = ntasks
    if ocean:
        modelList.append("ocean")
        if expconf.OCEAN_TASKS_PERC < 0 or expconf.OCEAN_TASKS_PERC > 1:
            raise ValueError(f"[411] Percentage of ocean tasks {expconf.OCEAN_TASKS_PERC} is out of range (0,1).")
        ntasks_oce = int(ntasks * expconf.OCEAN_TASKS_PERC)
        ntasks_atm = ntasks - ntasks_oce
    for model in modelList:
        fn_nml = models[model]["nml"]
        if expconf.REINIT: fn_nml = expconf.CHUNK_STR + "_" + fn_nml
        items = {'model_type' : models[model]["type"],
                 'model_name' : model,
                 'model_namelist_filename' : fn_nml}
        if ocean:
            if model == "atmo":
                minRank = 0
                maxRank = ntasks_atm
            elif model == "ocean":
                minRank = ntasks_atm + 1
                maxRank = ntasks
            else: raise RuntimeError("Oooops, something went wrong in MPI task assignment.")
            items["model_min_rank"] = minRank
            items["model_max_rank"] = maxRank
        ps.master.append({f"master_model_nml-{model}" : items})
    ps.master.append({'master_nml':{'lrestart':restart}})
    if jsbach:
        ps.master.append({'jsb_control_nml':{'restart_jsbach':True}})

def createPatchesOcean(p: SimpleNamespace, vert_cor_type: int, levels: str, initOceanFrom: str):
    """
    Create and add the patches related to model initialization.
    :param p:  patches dictionary (not substituting)
    :param vert_cor_type: type of vertica coordinate for the ocean
    :param levels: level string indicating the levels type
    :param initOceanFrom: type of initialization to use for the ocean
    """
    logger = logging.getLogger(__name__)
    logger.info("Creating patches for model initialization.")
    #-- Vertical coordinate
    oce_patch: dict[str, dict] = {}
    ml_varlist_stretch_c = ['draftave','hi','hs','conc','verticallyTotal_mass_flux_e','ice_u','ice_v',
                            'to','so', 'mlotst', 'zos', 'Qtop', 'Qbot', 'u', 'v', 'condep']
    if vert_cor_type == 0:
        oce_patch['ocean_dynamics_nml'] = {'vert_cor_type' : vert_cor_type,
                                           'select_lhs' : 2,
                                           'l_lhs_direct' : True}
        oce_patch['ocean_forcing_nml'] = {'seaice_limit' : 0.8}
    elif vert_cor_type == 1:
        oce_patch['ocean_dynamics_nml'] = {'vert_cor_type' : vert_cor_type,
                                           'select_lhs' : 1,
                                           'l_lhs_direct' : False}
        oce_patch['ocean_forcing_nml'] = {'seaice_limit' : 5.0}
        ml_varlist_stretch_c.append('stretch_c')
    else:
        raise ValueError(f"[411] OCEAN_VERT_COR_TYPE can only be 0 or 1.")
    #-- Ocean levels
    if levels == "L40":
        nlev = 40
        levidx_100m  = 9
        levidx_200m  = 12
        levidx_2000m = 30
        lvl_list = [
            12.,10.,10.,10.,10.,10.,13.,15.,20.,25.,30.,35.,40.,45.,50.,55.,60.,70.,80.,90.,
            100.,110.,120.,130.,140.,150.,170.,180.,190.,200.,220.,250.,270.,300.,350.,400.,
            450.,500.,500.,600.
        ]
    elif levels == "L64":
        nlev = 64
        levidx_100m  = 10
        levidx_200m  = 17
        levidx_2000m = 46
        lvl_list = [
            12.,10.,10.,10.,10.,10.,10.,10.,10.,10.,11.,12.,13.,14.,15.,16.,17.,18.,20.,22.,
            24.,26.,28.,30.,32.,35.,38.,41.,45.,49.,53.,58.,62.,66.,71.,75.,80.,85.,91.,97.,
            104.,111.,118.,125.,132.,138.,145.,152.,160.,167.,175.,182.,188.,195.,201.,208.,
            213.,219.,224.,230.,235.,241.,250.,260.
        ]
    elif levels == "L72":
        nlev = 72
        levidx_100m  = 18
        levidx_200m  = 25
        levidx_2000m = 55
        lvl_list = [
            2.0,2.2,2.5,2.8,3.1,3.5,3.9,4.4,4.9,5.4,5.9,6.4,7.1,7.7,8.4,9.2,10.1,11.0,
           12.0,13.2,14.4,15.7,17.1,18.7,20.4,22.3,24.3,26.5,28.9,31.5,34.3,37.3,40.6,
           43.1,45.3,46.8,48.4,50.0,51.7,53.4,55.2,57.0,58.9,60.8,62.9,66.6,72.6,80.6,
           90.6,100.2,110.0,120.3,128.7,137.4,146.4,155.7,165.2,174.8,184.4,194.1,203.6,
           212.9,221.9,230.5,238.5,245.9,252.4,258.1,262.8,266.4,268.9,270.1
        ]
    elif levels == "L128":
        nlev = 128
        levidx_100m  = 14
        levidx_200m  = 24
        levidx_2000m = 92
        lvl_list = [
             11.0,    9.0,    8.0,    8.0,    8.0,    8.0,    8.0,    8.0,    8.0,    8.0,
              8.0,    8.0,    8.0,    8.25,   8.5,    8.75,   9.0,   9.25,    9.5,   9.75,
             10.0,   10.0,   10.0,   10.0,   10.0,   10.0,   10.0,   10.0,   10.0,   10.0,
             10.5,   11.0,   11.5,   12.0,   12.5,   13.0,   13.5,   14.0,   14.5,   15.0,
             15.5,   16.0,   16.5,   17.0,   17.5,   18.0,   18.5,   19.0,   19.5,   20.0,
             20.5,   21.0,   21.5,   22.0,   22.5,   23.0,   23.5,   24.0,   24.5,   25.0,
             25.5,   26.0,   26.5,   27.0,   28.5,   29.0,   29.5,   30.0,   30.5,   31.0,
             31.0,   32.0,   33.0,   34.0,   35.0,   36.0,   37.0,   38.0,   39.0,   40.0,
             42.0,   44.0,   46.0,   48.0,   50.0,   52.0,   54.0,   56.0,   58.0,   60.0,
             62.0,   64.0,   66.0,   68.0,   70.0,   72.0,   74.0,   76.0,   78.0,   80.0,
             82.0,   84.0,   86.0,   88.0,   90.0,   92.0,   94.0,   96.0,   98.0,  100.0,
            102.0,  104.0,  106.0,  108.0,  110.0,  112.0,  114.0,  116.0,  118.0,  200.0,
            200.0,  200.0,  200.0,  200.0,  200.0,  200.0,  200.0,  200.0
        ]
    elif levels == "L128SMT":
        nlev = 128
        levidx_100m  = 27
        levidx_200m  = 37
        levidx_2000m = 96
        lvl_list = [
              2.0, 2.1, 2.2, 2.3, 2.4, 2.5, 2.6, 2.7, 2.8, 3.0, 3.1, 3.2,
              3.4, 3.5, 3.7, 3.9, 4.0, 4.2, 4.4, 4.6, 4.8, 5.0, 5.3, 5.5,
              5.8, 6.0, 6.3, 6.6, 6.9, 7.2, 7.5, 7.8, 8.2, 8.5, 8.9, 9.3,
              9.8, 10.2, 10.7, 11.1, 11.5, 11.9, 12.3, 12.7, 13.1, 13.5,
             14.0, 14.5, 14.9, 15.4, 15.9, 16.5, 17.0, 17.6, 18.2, 18.8,
             19.4, 20.0, 20.7, 21.4, 22.1, 22.8, 23.6, 24.4, 25.2, 26.0,
             26.9, 27.8, 28.7, 29.7, 30.6, 31.7, 32.7, 33.8, 34.9, 36.1,
             37.3, 38.5, 39.8, 41.1, 42.5, 43.9, 45.3, 46.8, 48.4, 50.0,
             51.7, 53.4, 55.2, 57.0, 58.9, 60.8, 62.9, 64.9, 67.1, 69.3,
             71.6, 74.0, 76.5, 79.0, 81.6, 84.3, 87.1, 90.0, 93.0, 96.1,
             99.3, 102.6, 106.0, 109.5, 113.2, 116.9, 120.8, 124.8, 128.9,
            133.2, 137.6, 142.2, 146.9, 151.8, 156.9, 162.1, 167.4, 173.0,
            178.7, 184.7, 190.8, 197.1
        ]
    else:
        raise ValueError(f"[411] Unknown ocean levels {levels!r}.")
    oce_patch['ocean_dynamics_nml']['n_zlev'] = nlev
    oce_patch['ocean_dynamics_nml']['minVerticalLevels'] = 2
    oce_patch['ocean_dynamics_nml'][f'dzlev_m(1:{nlev})'] = lvl_list
    oce_patch['output_nml-oce_ice'] = {'m_levels' : f"1...10,{levidx_100m},{levidx_200m},{levidx_2000m}",
                                       'ml_varlist' : ml_varlist_stretch_c}
    p.ocean.append(oce_patch)

    #-- Initialization
    init_patch = {'initial_salinity_type' : 0,
                 'initial_temperature_type' : 0,
                #  'sea_surface_height_type' : 0,
                #  'initial_velocity_type' : 0,
                 'initialize_fromRestart' : False}
    if initOceanFrom == 'restart' or initOceanFrom == 'climatology' or initOceanFrom == 'experiment':
        if initOceanFrom == 'restart':
            # oce_patch['initial_velocity_type']    = 0
            init_patch['initialize_fromRestart']   = True
        elif initOceanFrom == 'climatology':
            init_patch['initial_salinity_type']    = 1
            init_patch['initial_temperature_type'] = 1
            # oce_patch['sea_surface_height_type']  = 1
            # oce_patch['initial_velocity_type']    = 1
        elif initOceanFrom == 'experiment':
            init_patch['sea_surface_height_type']  = 0
            init_patch['initial_velocity_type']    = 0
        p.ocean.append({'ocean_initialConditions_nml' : init_patch})
    elif len(initOceanFrom) > 0:
        raise ValueError(f"[411] Unknown ocean initialization from {initOceanFrom!r}.")

def applyJsbachFilenames(nml: dict, expconf: ExperimentConfig):
    """
    Apply jsbach filenames to the jsbach namelist.
    :param nml: namelist (dict) to modify
    :param expconf: namespace with configuration entries from autosubmit
    """
    logger = logging.getLogger(__name__)
    logger.info("Creating patches for the jsbach model.")
    fn_jsbach = Path(expconf.WORKFLOWDIR, "jsbachFiles.pkl")
    if not fn_jsbach.exists():
        logger.warning("No workflow file with Jsbach Files is found.")
        return
    files = ifh.loadFiles(fn_jsbach)
    jsbachFiles = [ f for f in files if isinstance(f, JsbachFile)]
    reqFiles = {'seb' : {'bc' : 'phys',
                         'ic' : 'soil'},
                'rad' : {'bc' : 'phys',
                         'ic' : 'soil'},
                'turb' : {'bc' : 'phys',
                          'ic' : 'soil'},
                'sse' : {'bc' : 'soil',
                         'ic' : 'soil'},
                'hydro' : {'bc' : 'soil',
                           'ic' : 'soil',
                           'bc_sso' : 'sso'},
                'pheno' : {'bc' : 'phys',
                           'ic' : 'soil'},
                'disturb' : {'bc' : 'phys',
                             'ic' : 'soil'}}
    for group, filesDict in reqFiles.items():
        nmlGroup = nml.get(f"jsb_{group}_nml")
        if not isinstance(nmlGroup, dict): continue
        for fnPrefix, kind in filesDict.items():
            ct = fnPrefix[:2]
            jsbFile = None
            for jf in jsbachFiles:
                if jf.getKind() == kind and jf.ct == ct:
                    jsbFile = jf
                    break
            if jsbFile == None: raise FileNotFoundError(f"No jsbach file for {ct=} and {kind=} found.")
            nmlGroup[f"{fnPrefix}_filename"] = jsbFile.getFilename()

def getNamelistPaths(nmlinstring: str, nmloutstring: str, expconf: FullExperimentConfig, skipPrefix=False):
    """
    Extract the file name created by autosubmit from a namelist string
    as provided to auto-icon. Then, read the corresponding yaml file,
    apply all patches, and write it as a f90 namelist file.
    :param nmlstring: namelist name string for input namelist (as provided to auto-icon)
    :param nmloutstring: namelist name string for output namelist (as provided to auto-icon)
    :param expconf: namespace with configuration entries from autosubmit
    """
    logger = logging.getLogger(__name__)
    expconf.check(level="chunk", check_jobname=True)
    #-- Extract input and output file names.
    if len(nmlinstring) == 0: raise ValueError("[413] Namelist name empty! Check the NAMELIST setting in conf/<ICON_CASE>/simulation.yml file.")
    nml_in_fn = (nmlinstring[:nmlinstring.rfind(".")]
                 + "_"
                 + expconf.JOBNAME.removeprefix(expconf.EXPID + "_") )
    nml_in_path = Path(expconf.LOGDIR, nml_in_fn)
    fn_nml_out = nmloutstring
    if expconf.REINIT and not skipPrefix: fn_nml_out = expconf.CHUNK_STR + "_" + fn_nml_out
    nml_out_path = Path(expconf.OUTDIR, expconf.SDATE, expconf.MEMBER, fn_nml_out)
    logger.info(f"Path of input namelist file: '{nml_in_path}'")
    logger.info(f"Path of output namelist file: '{nml_out_path}'")
    sf = Path(nmlinstring).suffix
    if sf == ".nml" or sf == ".namelist": inType = 'nml'
    elif sf == ".yml" or sf == ".yaml":   inType = 'yml'
    else: raise ValueError(f"Suffix of input namelist unknown: {sf}")
    sf = nml_out_path.suffix
    if sf == ".nml" or sf == ".namelist": outType = 'nml'
    elif sf == ".yml" or sf == ".yaml":   outType = 'yml'
    else: raise ValueError(f"Suffix of output namelist unknown: {sf}")
    return (nml_in_path, nml_out_path, inType, outType)

def processNamelist(nml_in_path: Path, nml_out_path: Path, inType: str, outType: str,
                    patches: list, substitutes: list, expconf: FullExperimentConfig, jsbach=False):
    """
    Extract the file name created by autosubmit from a namelist string
    as provided to auto-icon. Then, read the corresponding yaml file,
    apply all patches, and write it as a f90 namelist file.
    :param nml_in_path: namelist path of namelist template
    :param nml_out_path: namelist path of output namelist
    :param inType: file type of input namelist ['nml', 'yml']
    :param outType: file type of output namelist ['nml', 'yml']
    :param patches: list of namelist patches *not* overwriting existing values
    :param substitutes: list of namelist patches overwriting existing values
    :param expconf: namespace with configuration entries from autosubmit
    :param jsbach: add jsbach files to the namelist
    """
    logger = logging.getLogger(__name__)
    expconf.check(level="member")
    #-- Just rename if no patches available.
    if (len(patches) == 0 and len(substitutes) == 0 and inType == outType and not jsbach):
        logger.info("No patches to apply, only copying namelist.")
        shutil.copy(nml_in_path, nml_out_path)
        return

    #-- Read parameters from yaml file
    if expconf.YAML_NAMELIST:
        parameters = loadYamlSection(nml_in_path, 'NAMELIST')
    else:
        with open(nml_in_path, 'r') as stream:
            parameters = f90nml.read(stream).todict()
            subs = []
            for grp in parameters.keys():
                if grp.startswith('_grp_'): subs.append(grp)
            for grp in subs:
                parameters[grp[5:]] = parameters.pop(grp)

    #-- ----- Substitute patches ----- --#
    #-- Member patches (only supported by yaml namelists)
    if expconf.YAML_NAMELIST:
        params_all_members = loadYamlSection(nml_in_path, 'MEMBERS')
        if expconf.MEMBER in params_all_members:
            applyPatches(parameters, params_all_members[expconf.MEMBER], overwrite=True)
    #-- Basic patches
    applyPatches(parameters, patches)
    #-- Overwriting patches, e.g. for reinitialization
    applyPatches(parameters, substitutes, overwrite=True)
    #-- Add jsbach files to the jsbach namelist
    if jsbach: applyJsbachFilenames(parameters, expconf)
    
    #-- ----- Write f90 namelist ----- --#
    logger.debug("Full set of namelist parameters:")
    logger.debug(parameters)
    nml = params2nml(parameters, expconf)
    f90nml.write(nml, nml_path=nml_out_path, force=True)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(prog='NmlUtils', description='This program will write an icon master namelist.')
    parser.add_argument('--jobname', type=str, required=True)
    parser.add_argument('--sdate', type=str, required=True)
    parser.add_argument('--chunk_start_date', type=str, required=True)
    parser.add_argument('--chunksizeunit', type=str, required=True)
    parser.add_argument('--member', type=str, required=True)
    parser.add_argument('--chunk', type=str, required=True)
    parser.add_argument('--numchunks', type=str, required=True)
    parser.add_argument('--chunksize', type=str, required=True)
    parser.add_argument('--cpkintv', type=str, required=True)
    parser.add_argument('--ntasks', type=int, required=True)
    parser.add_argument('--restart', action='store_true')
    parser.add_argument('--jsbach', action='store_true')
    args = parser.parse_args()
    expconf = FullExperimentConfig()
    expconf.JOBNAME = args.jobname
    expconf.SDATE = args.sdate
    expconf.CHUNKDATE = args.chunk_start_date
    expconf.CHUNKSIZEUNIT = args.chunksizeunit.upper()
    expconf.MEMBER = args.member
    expconf.CHUNK_STR = "{:04}".format(int(args.chunk))
    expconf.check(level="chunk")

    logging.basicConfig(level=expconf.LOGLEVEL)
    logger = logging.getLogger(__name__)
    logger.setLevel(expconf.LOGLEVEL)
    #-- Initialize autosubmit variables to expconf namespace
    use_ocean = (len(expconf.NAMELIST_OCEAN) > 0)

    #-- Additional configuration
    xConf = SimpleNamespace()
    xConf.NUMCHUNKS = int(args.numchunks)
    xConf.CHUNKSIZE = int(args.chunksize)
    xConf.CHECKPOINT_TIME_INTERVAL = args.cpkintv
    xConf.dtstr = ""
    xConf.dtstrOce = ""
    xConf.nwp_timesteps_mult = {}
    xConf.phy_timesteps = {}

    #-- Create patches for master namelist
    nml_patches = SimpleNamespace(master=[], atmo=[], jsbach=[], ocean=[])
    nml_patches_sub = SimpleNamespace(master=[], atmo=[], jsbach=[], ocean=[])

    output_times = createPatchesTiming(nml_patches, expconf, xConf)
    createPatchesMaster(nml_patches_sub, expconf, args.ntasks, ocean=use_ocean, jsbach=args.jsbach, restart=args.restart)
    #-- Process namelists
    logger.info("Processing master namelist.")
    nml_in_path, nml_out_path, inType, outType = getNamelistPaths(expconf.NAMELIST_MASTER_IN, expconf.NAMELIST_MASTER, expconf, skipPrefix=True)
    master_template_path = Path(expconf.LOGDIR, "icon_master.template.namelist")
    processNamelist(master_template_path, nml_out_path, inType, outType,
                    nml_patches.master, nml_patches_sub.master, expconf)
