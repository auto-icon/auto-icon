#!/usr/bin/python3

# Copyright 2023 - 2024 Karlsruhe Institute of Technology (KIT) and Ludwig-Maximilians-Universität München (LMU)
# SPDX-FileContributor: Andreas Baer
#
# SPDX-License-Identifier: BSD-3-Clause


import logging
import re
import numpy as np


PREFIXES = {
    24 : "Y",
    21 : "Z",
    18 : "E",
    15 : "P",
    12 : "T",
    9 : "G",
    6 : "M",
    3 : "k",
    2 : "h",
    -1 : "d",
    -2 : "c",
    -3 : "m",
    -6 : "u",
    # -6 : "mu",
    -9 : "n",
    -12 : "p",
    -15 : "f",
    -18 : "a",
    -21 : "z",
    -24 : "y"
}

class Units():
    """
    Representation of a set of units for a quantity.
    """
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.WARNING)
    unit_expr = re.compile("([a-zA-Z]+)(?:(-?[0-9]+)?)")

    def __init__(self, *units) -> None:
        self._units: dict[str, int] = {}
        self._power = 0
        self.addMultipleUnits(units)
    
    def __repr__(self):
        self.sort()
        return f"<Units {10**self._power:.1e} {self._units!r}>"
    
    def __str__(self):
        self.sort()
        n2print = self._power
        u2print = self._units
        if n2print == 0:
            if len(u2print) == 0: return "-"
            res = ""
        else:
            if 'g' in list(u2print.keys())[1:]:
                n2print += -3 * u2print['g']
                u2print = {}
                for key, val in self._units.items():
                    if key == 'g': u2print['kg'] = val
                    else: u2print[key] = val
            if (n2print in PREFIXES and
                self._getPowerFirstUnit() == 1):
                res = PREFIXES[n2print]
            else:
                res = f"{10**n2print:.0e} "
        for unit, power in u2print.items():
            if power == 1: res += f"{unit:s} "
            else:          res += f"{unit:s}{power:d} "
        return res.rstrip()
    
    def _getPowerFirstUnit(self):
        """ Get the power of the first unit. """
        if len(self._units) == 0: return 0
        else: return list(self._units.values())[0]
    
    def sort(self):
        """ Sort the units dict: ascending positive, then descending negative powers. """
        if len(self._units) == 0: return
        offset = max(self._units.values())
        keyfunc = lambda item: item[1] if item[1]>0 else -item[1]+offset
        self._units = dict( sorted(self._units.items(), key=keyfunc) )

    def addMultipleUnits(self, units):
        """ Add multiple units. """
        if units == None: return
        elif isinstance(units, Units):
            self._power += units._power
            for unit, power in units._units.items():
                if unit in self._units: self._units[unit] += power
                else:                   self._units[unit]  = power
                if self._units[unit] == 0: self._units.pop(unit)
        elif isinstance(units, str):
            #-- Float
            try: n = float(units)
            except ValueError: pass
            else:
                self._power += int(np.log10(n))
                return
            #-- Float
            try: n = int(units)
            except ValueError: pass
            else:
                self._power += int(np.log10(n))
                return
            #-- List of units
            for entry in units.split():
                self.addUnit(entry)
        elif isinstance(units, list) or isinstance(units, tuple):
            for entry in units:
                self.addMultipleUnits(entry)
        elif isinstance(units, int) or isinstance(units, float):
            self._power += int(np.log10(units))
        else: raise TypeError(f"[510] Type of unit input {type(units)} not suppported!")

    def addUnit(self, unitstring: str):
        """ Parse a string with a single unit. """
        matches = self.unit_expr.fullmatch(unitstring)
        if matches == None:
            self.logger.warning(f"Unitstring {unitstring!r} could not be parsed.")
            return
        unit, powerstr = matches.groups()
        if powerstr == None: power = 1
        else: power = int(powerstr)
        #-- Parse prefixes.
        for npower, prefix in PREFIXES.items():
            #-- Special case 'mol'
            if unit == 'mol' or len(unit) == 1: continue
            #-- Special case 'mu' as prefix
            if unit.startswith("mu"): unit = unit.removeprefix("m")
            #-- Default case
            if unit.startswith(prefix):
                self._power += npower * power
                unit = unit.removeprefix(prefix)
        if unit in self._units: self._units[unit] += power
        else:                   self._units[unit]  = power
        if self._units[unit] == 0: self._units.pop(unit)

if __name__ == "__main__":
    unitstrings = ["mm2", "A-2", "kg-1", "s5", 1e-6, "mol-2", "s", "m", "EN", "kg2"]
    nonunitstrings = ["a+2", "s2.5", "kg+m"]
    print("All units individually:")
    allUnits = Units()
    for ustr in unitstrings:
        myUnit = Units(ustr)
        print(f"{myUnit!r}: {myUnit}")
        allUnits.addMultipleUnits(myUnit)
    print(allUnits)
    #-- All units at once
    print("All units at once:")
    print(Units(unitstrings))
    print(Units(*unitstrings))
    #-- Tracer test
    print("Tracer test:")
    u = Units("mug")
    print(u)
    u.addMultipleUnits("kg-1")
    print(u)
    #-- Non-units
    print("Non-units:")
    for ustr in nonunitstrings:
        myUnit = Units(ustr)
        print(f"{myUnit!r}: {myUnit}")
