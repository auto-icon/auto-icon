#!/usr/bin/python3

#-- > Utilities for all file operations for ICON input and output files. < --#

# Copyright 2023 - 2024 Karlsruhe Institute of Technology (KIT) and Ludwig-Maximilians-Universität München (LMU)
# SPDX-FileContributor: Andreas Baer
#
# SPDX-License-Identifier: BSD-3-Clause

import copy
import filecmp
import logging
from os import PathLike
import validators
from datetime import datetime
from pathlib import Path
from abc import ABC
from typing import Optional, TypeVar, Union
import xarray as xr

from experiment_config import ExperimentConfig
from python_utils import downloadFile, filetype2format, substitutePlaceholders



class AutoIconFile(ABC):
    """
    Base class for all files in ICON.
    """
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.INFO)
    
    def __init__(self, fn: Optional[str] = None):
        super().__init__()
        self._patternName = ""              # Pattern for the file name.
        self.iconFiletype = -1
        self.filepath: Optional[Path] = None    # Name (path) of the file in the working directory.
        if fn: self.filepath = Path(fn)
    
    def __repr__(self) -> str:
        if self.filepath != None:
            return f"AI-file '{self.filepath}'"
        elif self._patternName != None:
            return f"AI-file {self._patternName!r}"
        else:
            return "<unknown icon file>"
    
    def __eq__(self, value: object) -> bool:
        """ Check if two files are the same. """
        if isinstance(value, self.__class__):
            if self.filepath == None and value.filepath == None:
                return (self._patternName == value._patternName and
                        self.iconFiletype == value.iconFiletype)
            else: return self.filepath == value.filepath
        return False

    #-- ----- Getters ----- --#
    def getFilename(self) -> str:
        """ Get the file name of the file. """
        if self.filepath == None: raise RuntimeError("[510] File name not yet set!")
        return self.filepath.name

    def getFilepath(self) -> str:
        """ Get the file path and name of the file. """
        if self.filepath == None: raise RuntimeError("[510] File name not yet set!")
        return str(self.filepath)

    #-- ----- Setters ----- --#
    def setPattern(self, pattern: str):
        """ Set the pattern for the file. """
        self._patternName = pattern

    #-- ----- Methods ----- --#
    def evalPattern(self, params={}) -> str:
        """
        Evaluate the pattern with provided parameters and return the evaluated string.
        The file format is further evaluated in the pattern.
        :param kwargs: further keywords to be repalced in the pattern
        :returns: evaluated pattern
        """
        if len(self._patternName) == 0:
            raise ValueError(f"[510] No file name or pattern specified for input file {type(self)}!")
        fmt = filetype2format(self.iconFiletype)
        res = self._patternName.format(FMT=fmt, **params)
        return res

    def evalPattern2Name(self, params={}):
        """
        Evaluate the name pattern using provided parameters
        and populate the name field with it.
        :param params: dict with parameters to evaluate the pattern
        """
        self.filepath = Path(self.evalPattern(params=params))
    
    def exists(self) -> bool:
        """ Return wether the file exists on disk. """
        if self.filepath != None:
            return self.filepath.exists()
        return False


#-- ----- INPUT FILES ----- --#
class InputFile(AutoIconFile, ABC):
    """
    Base class for all input files in ICON.
    """
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self._pool_directories = []             # List of Pool keys to be used for the file.
        self._target: Optional[Path] = None     # Path to the file on disk.
        self._patternSource = ""                # Pattern for the file target.
        self._patternName = "R{R:01d}B{B:02d}_DOM{DOM:02d}.{FMT:s}"
        self._source = ""                       # String rep. of the source (e.g. file name, URL)
        self._sourceUrl = ""                    # URL at which the file was actually found.
        self.skipLink = False                   # Do not link the file if createLink is called.
        self.kind = ''
    
    def __repr__(self):
        #-- Get name
        if self.filepath != None:
            n = self.filepath
        elif self._patternName != None:
            n = self._patternName
        else:
            n = "<unknown icon file>"
        #-- Get target
        if self._target != None:
            t = self._target
        elif self._patternSource != "":
            t = self._patternSource
        else:
            t = "<unknown icon soruce>"
        return f"'{t}' -> '{n}'"
    
    def __eq__(self, value: object) -> bool:
        if isinstance(value, self.__class__):
            return super().__eq__(value) and self.kind == value.kind
        return False

    #-- ----- Getters ----- --#
    def getTarget(self) -> Path:
        """ Get the target path, raise if none. """
        if self._target == None:
            raise ValueError("[510] Target is not yet set!")
        return self._target
    
    def targetIsSet(self) -> bool:
        """ Get if the target is already set. """
        return (self._target != None)
    
    def getKind(self) -> str:
        """ Get the kind of the file (inverse of CLASS_OF_KIND dict). """
        if len(self.kind) == 0: raise RuntimeError("[510] No type is assigned to this class!")
        return self.kind

    #-- ----- Setters ----- --#
    def setPattern(self, pattern: str, target=True):
        """
        Set the pattern for the file.
        :param pattern: pattern to be set
        :param target: set the target pattern or the file name pattern
        """
        if target: self._patternSource = pattern
        else:      super().setPattern(pattern)
    
    def setSource(self, src: str):
        """ Set the source. """
        self._source = src

    def setTarget(self, fn: PathLike):
        """
        Simple wrapper to set the target using the provided file name and converting
        to a Path object or the Path directly.
        :param fn: file name or path usually create by calling 'evalPattern' with some parameters
        """
        if isinstance(fn, Path): self._target = fn
        else:                    self._target = Path(fn)
    
    def setSourceNamePatterns(self, full_str: str):
        """ Split input to source and name. """
        if len(full_str) == 0: raise ValueError("[411] File name required. Check the file names of the LINK_FILES section and in the GRID section.")
        parts = full_str.split("|")
        self._patternSource = parts[0]
        if len(parts) == 2:
            self._patternName = parts[1]
        elif len(parts) > 2:
            raise ValueError(f"[411] Malformed expression {full_str!r}. Check the file names of the LINK_FILES section and in the GRID section.")

    #-- ----- Methods ----- --#
    def evalPattern(self, params={}, target=True) -> str:
        """
        Evaluate the pattern with provided parameters and return the evaluated string.
        The file format is further evaluated in the pattern.
        :param target: Evaluate the pattern for the target, otherwise for file name
        :param kwargs: further keywords to be repalced in the pattern
        :returns: evaluated pattern
        """
        if target:
            if len(self._patternSource) == 0:
                raise ValueError(f"[510] No file name or pattern specified for input file {type(self)}!")
            fmt = filetype2format(self.iconFiletype)
            res = self._patternSource.format(FMT=fmt, **params)
            return res
        else:
            if len(self._patternName) == 0 and len(self._patternSource) > 0:
                self._patternName = Path(self._patternSource).name
            return super().evalPattern(params)

    def evalPattern2Source(self, params={}):
        """
        Evaluate the target pattern using provided parameters
        and populate the source field with it.
        :param params: dict with parameters to evaluate the pattern
        """
        self._source = self.evalPattern(params=params)

    def evalPattern2Name(self, params={}):
        """
        Evaluate the name pattern using provided parameters
        and populate the name field with it.
        :param params: dict with parameters to evaluate the pattern
        """
        self.filepath = Path(self.evalPattern(params=params, target=False))

    def checkAndSetTarget(self, file2check: Union[str, Path]) -> bool:
        """
        Check if the desired file exists and is a regular file or directory.
        If yes, set the target to the provided file name / path and return true.
        Otherwise return false.
        :param f: file name or path
        """
        if isinstance(file2check, str):
            if file2check == "": return False
            f = Path(file2check)
        elif isinstance(file2check, Path):
            f = file2check
        else:
            raise TypeError(f"[510] File {file2check!r} with type {type(file2check)} is incompatible to str or Path.")
        if f.exists() and (f.is_file() or f.is_dir()):
            self._target = f
            return True
        else: return False

    def findFileWithWildcard(self, src: str, relpath: str):
        """
        Find all files matching a source string containing wildcards (*, ?, []).
        For each file found, returs a deep copy of self with the appropriate
        target, name and name pattern set.
        :param src: source string
        :param relpath: relative path where to look for the files
        :returns: list of ExtraFile instances
        """
        if not isinstance(relpath, str):
            raise TypeError(f"[510] 'relpath' is no str but {type(relpath)}!")
        s = src
        p = Path(src)
        while ( '*' in s or '?' in s or '[' in s or ']' in s ):
            p = p.parent
            s = str(p)
        pattern = Path(src).relative_to(p)
        foundFiles = []
        for file in Path(relpath, p).rglob(pattern.name):
            if file.exists():
                foundFiles.append(self.duplicate(file))
        return foundFiles
    
    def duplicate(self, newName: Union[str, Path]):
        """
        Duplicates the current file instance with a new file name.
        Used for e.g. creating files from patterns and grfinfo files.
        :param newName: target of the new file
        """
        newFile = copy.deepcopy(self)
        if newFile.checkAndSetTarget(newName):
            if not isinstance(newName, Path):
                newName = Path(newName)
            newFile.setPattern(newName.name, target=False)
            newFile.evalPattern2Name()
            return newFile
        else:
            raise FileNotFoundError(f"[513] File '{newName}' could not be found!")

    def findFile(self):
        """
        Find this file on disk, at a pool directory or in an online repository.
        This sets the path for the link target.
        Raises a FileNotFoundError if file source
        - is a URL and could not be donwloaded
        - is an absolute path and could not be found on disk
        - is a pattern matching no files on disk
        - matches multiple different files in a pool.
        Sets the target to INDIR/<filename> if none is the case and the file could
        still not be found on disk.
        :param src: source for the file (overwriting the internal source parameter)
        :returns: self or list of files found
        """
        expconf = ExperimentConfig()
        src = self._source
        self.logger.debug(f"Checking source {src!r}.")
        wildcard = False
        if len(src) == 0: raise ValueError(f"[510] Source for the file {self!r} is not set.")
        #-- Check for patterns.
        if src[0] == "!":
            if not isinstance(self, ExtraFile):
                raise RuntimeError(f"[411] Patterns in file sources are not allowed for {type(self)}! Check your linked file names.")
            src = src.lstrip('!')
            if src.startswith(("https://","http://","ftp://","file://")):
                raise RuntimeError(f"[411] Source {src!r} uses wildcards but looks like a URL, which is not supported! Check your linked file names.")
            wildcard = True
        #-- Check for a URL
        if validators.url(src):
            self.logger.debug(f"Trying to download from URL {src!r}.")
            f = downloadFile(src, expconf.INDIR, loglvl=self.logger.level)
            if self.checkAndSetTarget(f):
                self._sourceUrl = src
                self.logger.debug(f"Downloaded file from URL: {src!r} -> {f!r}")
                return self
            else:
                raise FileNotFoundError(f"[411] File {src!r} seems to be a URL but could not be successfully downloaded. Check your linked file names.")
        #-- Check if absolute path
        if src[0] == "/":
            self.logger.debug(f"Source {src!r} is an absolute path.")
            if wildcard:
                fl = self.findFileWithWildcard(src, "/")
                if len(fl) > 0: return fl
                else: raise FileNotFoundError(f"[411] File {src!r} seems to be an absolute path but could not be found. Check your linked file names.")
            f = Path(src)
            if self.checkAndSetTarget(f):
                self.logger.debug(f"Found with absolute path '{f}'.")
                return self
            else:
                raise FileNotFoundError(f"[411] File {src!r} seems to be an absolute path but could not be found. Check your linked file names.")
        #-- ----- Pool directories (incl. INDIR) ----- --#
        #-- Loop over pool directories to find the file locally
        for poolname in self._pool_directories:
            pools = expconf.POOLS.get(poolname, [])
            if len(pools) == 0:
                raise KeyError(f"[413] Pool {poolname!r} is not set! Check the POOLS section in conf/common/platforms/<platform>.yml.")
            for poolstr in pools:
                self.logger.debug(f"Checking pool {poolstr!r}.")
                #-- URL pool
                if validators.url(poolstr):
                    url = poolstr.rstrip('/') + '/' + Path(src).name.lstrip('/')
                    f = downloadFile(url,
                                     expconf.INDIR,
                                     raiseOnFailure=False,
                                     loglvl=self.logger.level)
                    if len(f) > 0:
                        self._sourceUrl = url
                        self.logger.debug(f"Found at URL pool {f!r}.")
                        if self.checkAndSetTarget(f): return self
                #-- Local pool: Check if the file exists
                if wildcard:
                    fl = self.findFileWithWildcard(src, poolstr)
                    if len(fl) > 0: return fl
                else:
                    f = Path(poolstr, src)
                    if self.checkAndSetTarget(f):
                        self.logger.debug(f"Found at pool root '{f}'.")
                        return self
                    #-- Search the Pool
                    pool = Path(poolstr)
                    files = list(pool.rglob(src))
                    if len(files) == 0: continue
                    elif self.checkAndSetTarget(files[0]):
                        self.logger.debug(f"Single file found in pool '{files[0]}'.")
                        return self
                    else:
                        for f2 in files[1:]:
                            if not filecmp.cmp(files[0], f2):
                                self.logger.error(files)
                                raise FileNotFoundError(f"[414] Multiple files found in pool {poolstr!r}, but they are not equal.")
                        #-- This did not fail, i.e. all files are equal
                        if self.checkAndSetTarget(files[0]):
                            self.logger.debug(f"[414] Multiple identical files found in pool, using '{files[0]}'.")
                            return self
        #-- File was not found.
        if "INDIR" in self._pool_directories and wildcard == False:
            self._target = Path(expconf.INDIR) / Path(src).name
            self.logger.info(f"File not found, still setting target: '{self._target}'")
            return self
        raise FileNotFoundError(f"[414] File {src!r} could not be found!")

    def createLink(self, replace=True):
        """
        Create the symlink from '_target' to 'name'.
        Usually, '_target' is absolute and 'name' is only a file name.
        :param replace: replace an existing link name, finish otherwise
        """
        if self.skipLink: return
        if self._target == None: raise ValueError(f"[512] Link target for {self!r} is not set!")
        if self.filepath == None: raise ValueError(f"[512] Link name for {self!r} is not set!")
        if not self._target.exists(): raise FileNotFoundError(f"[414] File {self._target.name!r} does not exist!")
        if self.filepath.is_symlink():
            if not replace:
                self.logger.debug("Symlink exists, linking is done.")
                return
            self.logger.debug("Removing symlink.")
            self.filepath.unlink()
        self.logger.debug(f"Creating symlink: '{self._target}' -> '{self.filepath}'")
        self.filepath.symlink_to(self._target, target_is_directory=self._target.is_dir())
    
    def exists(self) -> bool:
        """ Return wether the file exists on disk. """
        if self._target != None:
            return self._target.exists()
        return False


class ExtraFile(InputFile, ABC):
    """
    Base class for general input files.
    """
    def __init__(self, fullstr: Optional[str] = None, date: Optional[datetime] = None, **kwargs):
        super().__init__(**kwargs)
        self._patternName = ""
        self.date = date    # Used for placeholder substitution
        if fullstr != None: self.setSourceNamePatterns(fullstr)

    def evalPattern(self, params={}, target=True) -> str:
        """
        Evaluate the pattern with provided parameters and return the evaluated string.
        The file format is further evaluated in the pattern.
        :param target: Evaluate the pattern for the target, otherwise for file name
        :param kwargs: further keywords to be repalced in the pattern
        :returns: evaluated pattern
        """
        s = super().evalPattern(params=params, target=target)
        if isinstance(self.date, datetime):
            return substitutePlaceholders(s, self.date)
        else: return s


class MiscFile(ExtraFile):
    """
    General input file (typically linked from INDIR or a pool).
    """
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self._pool_directories = ["INDIR", "EXP", "GENERAL"]


class JsbachFile(ExtraFile):
    """
    Input file for Jsbach.
    """
    def __init__(self, kind, ct, year, jsbss="", **kwargs):
        super().__init__(**kwargs)
        self._pool_directories = ["INDIR", "EXP", "GENERAL"]
        self.kind = kind    # e.g. phys, soil, sso
        self.ct = ct        # ic or bc
        self.iconFiletype = 4
        self._year = year
        self._jsbss = jsbss
        self._patternName = "{JSB_CT}_land_{JSB_KIND}.{FMT:s}"
        self._patternSource = "{JSB_CT}_land_{JSB_KIND}_{JSB_YEAR}.{FMT:s}"
        if ct == 'ic' and kind == 'soil':
            if len(jsbss) == 0: raise ValueError(f"No jsbach soil spinup identifier not provided.")
            self._patternSource = "{JSB_CT}_land_{JSB_KIND}_{JSB_YEAR}_{JSBSS}.{FMT:s}"
    
    def evalPattern(self, params={}, target=True):
        if len(self.kind) == 0: raise ValueError("Jsbach file kind is not set!")
        if len(self.ct) == 0: raise ValueError("Jsbach file conditions type not set!")
        if self._year <= 0: raise ValueError("Jsbach year is not set!")
        d = {"JSB_CT" : self.ct,
             "JSB_YEAR" : self._year,
             "JSB_KIND" : self.kind}
        if len(self._jsbss) > 0: d['JSBSS'] = self._jsbss
        d.update(params)
        return super().evalPattern(params=d, target=target)


class IconFile(ExtraFile):
    """
    General ICON input file (typically linked from the ICON repository).
    """
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self._pool_directories = ["ICON"]


class ArtFile(ExtraFile):
    """
    General ART input file (typically linked from the ART repository).
    """
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self._pool_directories = ["ART"]


class GridFile(InputFile):
    """
    Class for an ICON grid file.
    """
    def __init__(self):
        super().__init__()
        self._pool_directories = ["INDIR", "GRID"]
        self._patternSource = "icon_grid_{GRID_NUMBER:04d}_R{R:02d}B{B:02d}_{TYPE:s}.{FMT:s}"
        self._patternName = "icon_" + self._patternName
        self.iconFiletype = 4
        self.uuid = ""
        self.r = -1
        self.b = -1
        self.number = -1
        self.type = ""
        self.kind = 'GRID'
    
    def __eq__(self, value: object) -> bool:
        if isinstance(value, self.__class__):
            return super().__eq__(value) and self.getGridParams() == value.getGridParams()
        return False
    
    def getGridParams(self):
        d = {
            "GRID_NUMBER" : self.number,
            "R" : self.r,
            "B" : self.b,
            "TYPE" : self.type
        }
        return d
    
    def evalPattern(self, params={}, target=True):
        d = self.getGridParams()
        d.update(params)
        return super().evalPattern(params=d, target=target)
    
    def duplicate(self, newName: Union[str, Path]):
        """
        Duplicates the current file instance with a new file name.
        Used for e.g. creating files from patterns and grfinfo files.
        :param newName: target of the new file
        """
        newFile = super().duplicate(newName)
        if not isinstance(newFile, GridFile):
            raise TypeError(f"[510] Duplicate of GridFile is not of type GridFile but {type(newFile)}.")
        newFile.kind = 'GRFINFO'
        if self.filepath != None:
            newFile.filepath = self.filepath.with_stem(self.filepath.stem + "-grfinfo")
        return newFile
    
    def findFile(self):
        """ Adding to findFile of InputFile to account for (legacy) grfinfo files. """
        expconf = ExperimentConfig()
        res = super().findFile()
        if res != self:
            raise NotImplementedError("[411] Patterns with grid files are not suppored.")
        if self._target == None:
            raise RuntimeError(f"[512] Target for {self!r} should have been set but is not.")
        if len(self._sourceUrl) > 0:
            ps = Path(self._sourceUrl)
            srcgrfinfo = ps.with_stem(ps.stem + "-grfinfo")
            fgrfinfo = self._target.with_stem(self._target.stem + "-grfinfo")
            f2 = downloadFile(str(srcgrfinfo),
                              expconf.INDIR,
                              raiseOnFailure=False,
                              loglvl=self.logger.level)
            if len(f2) > 0 and fgrfinfo.exists():
                return [self, self.duplicate(fgrfinfo)]
        else:
            fgrfinfo = self._target.with_stem(self._target.stem + "-grfinfo")
            if fgrfinfo.exists():
                return [self, self.duplicate(fgrfinfo)]
        return self


class ExtparFile(InputFile):
    """
    Class for an ICON extpar file.
    """
    def __init__(self):
        super().__init__()
        self._pool_directories = ["INDIR", "GRID"]
        self._patternSource = "icon_extpar_{GRID_NUMBER:04d}_R{R:02d}B{B:02d}_{TYPE:s}_{EXTPAR_DATE}{TILES:s}.{FMT:s}"
        self._patternName = "extpar_icon_" + self._patternName
        self.iconFiletype = 4
        self.tiles = False
        self.kind = 'EXTPAR'
        self.date: Optional[datetime] = None
        self.grid: Optional[GridFile] = None
    
    def evalPattern(self, params={}, target=True):
        if not isinstance(self.grid, GridFile):
            raise ValueError("[510] Grid file is not yet set!")
        dateformat = "%Y" + "%m" + "%d"
        if self.date == None: dt = ""
        else: dt = self.date.strftime(dateformat)
        d = {
            "EXTPAR_DATE" : dt,
            "TILES"       : ""
        }
        if self.tiles: d["TILES"] = "_tiles"
        d.update(self.grid.getGridParams())
        d.update(params)
        return super().evalPattern(params=d, target=target)


class UnknownDomainFile(InputFile):
    """
    Class for an ICON extpar file.
    """
    def __init__(self):
        super().__init__()
        self._pool_directories = ["INDIR", "GRID", "GENERAL"]
        self._patternSource = ""
        self._patternName = ""
        self.grid: Optional[GridFile] = None
    
    def evalPattern(self, params={}, target=True):
        if not isinstance(self.grid, GridFile):
            raise ValueError("Grid file is not yet set!")
        if self.grid != None: d = self.grid.getGridParams()
        else: d = {}
        d.update(params)
        return super().evalPattern(target=target, params=d)


class EmissFile(InputFile):
    """
    Emission files for ICON-ART.
    """
    def __init__(self, kind=""):
        super().__init__()
        self._pool_directories = ["INDIR", "EXP", "GENERAL"]
        self._patternSource = "ART_{AFT:3s}_iconR{R:1d}B{B:02d}-grid_{ART_IO_SUFFIX:s}.{FMT:s}"
        self._patternName = self._patternSource
        self.iconFiletype = 4
        self.kind = kind
        self.grid: Optional[GridFile] = None
    
    def evalPattern(self, params={}, target=True):
        if not isinstance(self.grid, GridFile):
            raise ValueError("[510] Grid file is not yet set!")
        if len(self.kind) == 0: raise ValueError("[510] Emission file kind is not set!")
        d = { "AFT" : self.kind }
        d.update(self.grid.getGridParams())
        d.update(params)
        if len(d["ART_IO_SUFFIX"]) == 0:
            d["ART_IO_SUFFIX"] = f"{self.grid.number:04d}"
        return super().evalPattern(params=d, target=target)


#-- ----- RAW FILES (for icbc files) ----- --#
class RawFile(InputFile):
    """
    Class for raw data for icbc files.
    """
    def __init__(self, date: Optional[datetime] = None):
        super().__init__()
        self._pool_directories = ["INDIR", "EXP", "GENERAL"]
        self._patternSource = ""
        self._patternName = ""
        self.date = date
        self.iconFiletype = 2

    def evalPattern(self, params={}, target=True):
        dateformat = "%Y" + "%m" + "%d" + "%H"
        if self.date == None: dt = ""
        else: dt = self.date.strftime(dateformat)
        d = { "SDATE" : dt }
        d.update(params)
        return super().evalPattern(params=d, target=target)
    
    def setAllNames(self, pattern: str):
        """ Set name and target from a single file name. """
        self.setPattern(pattern, target=True)
        self.setPattern(pattern, target=False)
        self.evalPattern2Source()
        self.evalPattern2Name()


#-- ----- ICBC FILES ----- --#
class IcbcFile(InputFile, ABC):
    """
    Base class for an initial conditions file for ICON.
    """
    def __init__(self):
        super().__init__()
        self._pool_directories = ["INDIR", "EXP", "GENERAL"]
        self.grid: Optional[GridFile] = None
        self.date: Optional[datetime] = None
    
    def evalPattern(self, params={}, target=True):
        if not isinstance(self.grid, GridFile):
            raise ValueError("[510] Grid file is not yet set!")
        dateformat = "%Y" + "%m" + "%d" + "%H"
        if self.date == None: dt = ""
        else: dt = self.date.strftime(dateformat)
        d = { "SDATE" : dt }
        d.update(self.grid.getGridParams())
        d.update(params)
        return super().evalPattern(params=d, target=target)


class IfsFile(IcbcFile):
    """
    IFS initial conditions file for ICON.
    """
    def __init__(self):
        super().__init__()
        self._patternSource = "icon_ifs_{GRID_NUMBER:04d}_R{R:02d}B{B:02d}_{TYPE:s}_{SDATE}.{FMT:s}"
        self._patternName = "ifs2icon_" + self._patternName
        self.iconFiletype = 4
        self.kind = 'IFS'


class Era5File(IcbcFile):
    """
    ERA5 initial conditions file for ICON.
    """
    def __init__(self):
        super().__init__()
        self._patternSource = "icon_era5_{GRID_NUMBER:04d}_R{R:02d}B{B:02d}_{TYPE:s}_{SDATE}.{FMT:s}"
        self._patternName = self._patternSource
        self.iconFiletype = 4
        self.kind = 'ERA5'


class EraIFile(IcbcFile):
    """
    ERA interim initial conditions file for ICON.
    """
    def __init__(self):
        super().__init__()
        self._patternSource = "icon_erai_{GRID_NUMBER:04d}_R{R:02d}B{B:02d}_{TYPE:s}_{SDATE}.{FMT:s}"
        self._patternName = self._patternSource
        self.iconFiletype = 4
        self.kind = 'ERAI'


class DwdFgFile(IcbcFile):
    """
    DWDFG initial conditions file for ICON.
    """
    def __init__(self):
        super().__init__()
        self._patternSource = "dwdFG_R{R:1d}B{B:02d}_DOM{DOM:02d}.{FMT:s}"
        self._patternName = "dwdFG_" + self._patternName
        self.iconFiletype = 2
        self.kind = 'DWDFG'


class DwdAnaFile(IcbcFile):
    """
    DWDANA initial conditions file for ICON.
    """
    def __init__(self):
        super().__init__()
        self._patternSource = "dwdana_R{R:1d}B{B:02d}_DOM{DOM:02d}.{FMT:s}"
        self._patternName = "dwdana_" + self._patternName
        self.iconFiletype = 2
        self.kind = 'DWDANA'


class ArtIcbcFile(IcbcFile):
    """
    ART initial conditions file for ICON.
    """
    def __init__(self, kind=""):
        super().__init__()
        self._patternSource = "ART_{AFT:3s}_iconR{R:1d}B{B:02d}-grid_{ART_IO_SUFFIX:s}.{FMT:s}"
        self._patternName = self._patternSource
        self.iconFiletype = 4
        self.kind = kind
    
    def evalPattern(self, params={}, target=True):
        if not isinstance(self.grid, GridFile):
            raise ValueError("[510] Grid file is not yet set!")
        if len(self.kind) == 0: raise ValueError("[510] Emission file kind is not set!")
        dateformat = "%Y" + "%m" + "%d" + "%H"
        if self.date == None: dt = ""
        else: dt = self.date.strftime(dateformat)
        d = {
            "SDATE" : dt,
            "AFT" : self.kind
        }
        d.update(self.grid.getGridParams())
        d.update(params)
        if len(d["ART_IO_SUFFIX"]) == 0:
            d["ART_IO_SUFFIX"] = f"{self.grid.number:04d}"
        return super().evalPattern(params=d, target=target)


#-- ----- OUTPUT FILES ----- --#
class OutputFile(AutoIconFile, ABC):
    """
    Base class for all output files in ICON.
    """


class ResultFile(AutoIconFile):
    """
    Class for all results, i.e. regular prognostic and diagnostic files
    to be post processed or analyzed.
    """
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.samoa = "UNKNOWN"      # States: "UNKNOWN", "FAILED", "PASSED"
        self.vars: list[str] = []
        self.primary = False
        if self.filepath != None and self.filepath.exists(): self.populateVariables()
    
    def populateVariables(self):
        if self.filepath != None and self.filepath.exists():
            ds = xr.open_dataset(self.filepath)
            self.vars = [str(var) for var in ds.variables]
            ds.close()
            del ds


class IntermediateFile(AutoIconFile):
    """
    Class for all intermediate files, e.g. reinit files, restart files, etc.
    """
    def __init__(self, filetype = -1, date: Optional[datetime] = None):
        super().__init__()
        self.iconFiletype = filetype
        self.date = date
    
    def evalPattern(self, params={}):
        dateformat = "%Y" + "%m" + "%d" + "%H"
        if self.date == None:
            return super().evalPattern()
        else:
            d = { "SDATE" : self.date.strftime(dateformat) }
            d.update(params)
            return super().evalPattern(params=d)

TYPE_AIF = TypeVar('TYPE_AIF', bound=AutoIconFile)
CLASS_OF_KIND = {
    #-- Domain specific input data.
    'GRID'      : GridFile,
    'GRFINFO'   : GridFile,
    'EXTPAR'    : ExtparFile,
    'IFS'       : IfsFile,
    'ERA5'      : Era5File,
    'ERAI'      : EraIFile,
    'DWDFG'     : DwdFgFile,
    'DWDANA'    : DwdAnaFile,
    'ICE'       : ArtIcbcFile,
    'IAE'       : ArtIcbcFile,
    'STY'       : EmissFile,
    'BIO'       : EmissFile,
    'BCF'       : EmissFile,
    'ANT'       : EmissFile
    #-- Other input data:
    # 'INPUT'     : InputFile,
    # 'EXTRA'     : ExtraFile,
    # 'MISC'      : MiscFile,
    # 'ICON'      : IconFile,
    # 'ART'       : ArtFile,
    # 'RAW'       : RawFile,
}
