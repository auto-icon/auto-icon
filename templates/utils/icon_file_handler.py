#!/usr/bin/python3

#-- > Process the grid section of an experiment configuration file. < --#

# Copyright 2023 - 2024 Karlsruhe Institute of Technology (KIT) and Ludwig-Maximilians-Universität München (LMU)
# SPDX-FileContributor: Andreas Baer
#
# SPDX-License-Identifier: BSD-3-Clause

import argparse
import pickle
import yaml
import logging
from pathlib import Path
from typing import Union

from experiment_config import ExperimentConfig, FullExperimentConfig
from icon_domain import IconDomain
from icon_file import TYPE_AIF, AutoIconFile, ResultFile


def createDomainList(fn_yaml: str, date_str=None, loglvl=logging.INFO) -> list[IconDomain]:
    """
    Initialize the grid info from a yaml file.
    :param fn_yaml: File name of yaml file with grid info.
    :param loglvl: level of the logger used (default=logging.INFO)
    :return: list of domains, sorted by domain index
    """
    logger = logging.getLogger(__name__)
    logger.setLevel(loglvl)
    #-- Initialize
    USE_RADGRID = False
    with open(fn_yaml, 'r') as stream:
        grid_info = yaml.safe_load(stream).get('GRID', {})
    if len(grid_info) == 0:
        raise RuntimeError(f"[411] Grid info could not be read from file {fn_yaml!r}. Please provide a valid GRID section.")
    domains: list[IconDomain] = []
    FILENAMES = {}
    FILELIST = ['GRID']
    dom_info = {}

    #-- ----- Parse file ----- --#
    #-- Parse all non-domain sections.
    for domain_str, data in grid_info.items():
        if domain_str[:3] == "DOM" or domain_str[:3] == "OCE":
            dom_info[domain_str] = data
        else:
            #-- Not a domain_str. Could be RADGRID, FILENAMES, FILELIST or invalid
            if domain_str == "RADGRID": USE_RADGRID = bool(data)
            elif domain_str == "FILENAMES":
                FILENAMES.update(data)
            elif domain_str == "FILELIST":
                for val in data:
                    if val not in FILELIST:
                        FILELIST.append(val)
    #-- Sort and parse all domain sections.
    dom_info = dict(sorted(dom_info.items()))
    for domain_str, data in dom_info.items():
        logger.info(f"Creating domain {domain_str!r}.")
        logger.debug(data)
        domain_index = int(domain_str[3:].lstrip('-_'))
        if (len(domains) > 0 and
            domain_index == domains[-1].index() + 1):
            domain = domains[-1].createChildDomain()
        else:
            domain = IconDomain(dom_index=domain_index, start_date=date_str, ocean=(domain_str[:3] == "OCE"))
            domain.logger.setLevel(loglvl)
        domain.populate(data)
        logger.info(domain)
        domains.append(domain)
        for f in FILELIST:
            if domain_str[:3] != "OCE" and f not in domain.skipFiles and domain.getFile(f) == None:
                domain.addFile(f)
    #-- Check if domains are properly sorted and asign hierarchy.
    for idx, domain in enumerate(domains):
        if idx == 0: continue
        if domain.type() == 'O': break
        if domain.parent != domains[idx - 1]:
            logger.warning("The domains are not properly stored in a hierarchy. Issues might arise in the future.")
    
    #-- ----- Add radiation grid ----- --#
    if USE_RADGRID:
        #-- The radiation grid has the smallest domain index
        if domains[0].type() != 'R':
            logger.info("Creating a radiation grid domain.")
            dom_base = domains[0]
            domains.insert(0, dom_base.createParentDomain())
            logger.info(domains[0])
        else:
            raise RuntimeError("[411] No domain found, only a radiation grid was requested! Check your GRID section.")
    logger.debug("Final list of domains:")
    logger.debug(domains)
    return domains

def createResultFilesList(expconf: ExperimentConfig, fnResultFiles: Path = Path("resultFiles.pkl"),
                          read=True, write=True, initAsPrimary=False) -> list[ResultFile]:
    """ Create the list of output files. """
    logger = logging.getLogger(__name__)
    logger.setLevel(expconf.LOGLEVEL)

    if fnResultFiles.exists() and read:
        logger.info(f"Loading result files list '{fnResultFiles}'.")
        resultFiles = [ f for f in loadFiles(fnResultFiles) if isinstance(f, ResultFile) ]
    else: resultFiles = []
    outdir = Path(expconf.OUTDIR)
    if isinstance(expconf, FullExperimentConfig):
        if expconf.SDATE:
            outdir = outdir / expconf.SDATE
            if expconf.MEMBER:
                outdir = outdir / expconf.MEMBER
    logger.debug(f"Outdir: '{outdir}', pattern: {expconf.FNO_PATTERN!r}")
    presentFiles = list(outdir.rglob(expconf.FNO_PATTERN))

    for fn_present in presentFiles:
        if fn_present.is_dir(): continue
        fileIsPresent = False
        for result_file in resultFiles:
            if fn_present == result_file.filepath:
                fileIsPresent = True
                break
        if fileIsPresent: continue
        newFile = ResultFile(fn=fn_present)
        if initAsPrimary: newFile.primary = True
        resultFiles.append(newFile)
    if write:
        saveFiles(fnResultFiles, resultFiles)
    logger.info(f"Final list of result files contains {len(resultFiles)} files.")
    return resultFiles

def loadFromPickle(fni: Union[str,Path], tp) -> list:
    """ Core functionality for loadDomains and loadFiles. """
    with open(fni, 'rb') as f:
        l = pickle.load(f)
    if not isinstance(l, list):
        raise TypeError(f"[412] Loaded object of type {type(l)} is not list!")
    for entry in l:
        if not isinstance(entry, tp):
            raise TypeError(f"[412] List entry {entry!r} is of type {type(entry)}, not {tp!s}!")
    return l

def saveToPickle(fno: Union[str,Path], l: list, tp):
    """ Core functionality for saveDomains and saveFiles. """
    for entry in l:
        if not isinstance(entry, tp):
            raise TypeError(f"[510] List entry {entry!r} is of type {type(entry)}, not {tp!s}!")
    with open(fno, 'wb') as f:
        pickle.dump(l, f, pickle.HIGHEST_PROTOCOL)


def saveDomains(fno: Union[str,Path], domains: list[IconDomain]):
    """
    Save the list of domains to a file.
    :param fno: output file (str or Path object)
    :param domains: list of domains to save
    """
    saveToPickle(fno, domains, IconDomain)

def loadDomains(fni: Union[str,Path]) -> list[IconDomain]:
    """
    Load the list of domains from a file.
    :param fni: input file (str or Path object)
    """
    return loadFromPickle(fni, IconDomain)

def saveFiles(fno: Union[str,Path], files: list[TYPE_AIF]):
    """
    Save the list of files to a file.
    :param fno: output file (str or Path object)
    :param files: list of files to save
    """
    saveToPickle(fno, files, AutoIconFile)

def loadFiles(fni: Union[str,Path]) -> list[AutoIconFile]:
    """
    Load the list of files from a file.
    :param fni: input file (str or Path object)
    """
    return loadFromPickle(fni, AutoIconFile)


#-- ----- Testing ----- --#
if __name__ == "__main__":
    parser = argparse.ArgumentParser(prog='GIP', description='This short porgramm provides functions for handling auto-icon file and domain lists.')
    parser.add_argument('--yamlFile', type=str, required=True)
    parser.add_argument('--date', type=str)
    args = parser.parse_args()
    expconf = ExperimentConfig()

    logging.basicConfig(level=expconf.LOGLEVEL)
    logger = logging.getLogger(__name__)
    logger.setLevel(expconf.LOGLEVEL)

    logger.info("Read yaml file.")
    domains = createDomainList(args.yamlFile, args.date, loglvl=expconf.LOGLEVEL)
    logger.info(domains)
