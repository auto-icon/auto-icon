#!/usr/bin/python3

#-- > Utilities file for ICON domains. < --#

# Copyright 2023 - 2024 Karlsruhe Institute of Technology (KIT) and Ludwig-Maximilians-Universität München (LMU)
# SPDX-FileContributor: Andreas Baer
#
# SPDX-License-Identifier: BSD-3-Clause

from datetime import datetime
import logging
from types import SimpleNamespace
from typing import Optional, Union

from icon_file import CLASS_OF_KIND, ArtIcbcFile, EmissFile, ExtparFile, GridFile, IcbcFile, InputFile, UnknownDomainFile
from python_utils import idate2date, sdate2date


class IconDomain:
    """
    This class contains the info of a single patch for an ICON run.
    It contains info on the grid type, resolution, external parameters
    and initial conditions file. Methods exist to output info like
    filenames.
    """
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.INFO)
    
    def __init__(self, dom_index=1, start_date=None, ocean=False):
        self._data = SimpleNamespace()
        self._data.DOM = dom_index
        self._files: dict[str, InputFile] = {}
        self._icbcFiles: list[IcbcFile] = []
        self.fileTypes: set[str] = set()
        self.skipFiles = {}
        self.parent: Optional[IconDomain] = None
        self.child:  Optional[IconDomain] = None
        #-- Start date
        if start_date == None:  self._data.SDATE = -1
        else:                   self.setStartDate(start_date)
        self.addFile("GRID")
        #-- Set default values
        self.setGridR(-1)
        self.setGridB(-1)
        self.setGridT('')
        self.setGridN(-1)
        self.setExtparDate(-1)
        self.setExtparTiles(False)
        self._data.ART_IO_SUFFIX = ''
        if ocean: self.setGridT('O')

    def __repr__(self):
        return "IconDomain #{DOM}: R={R}, B={B}, {files!r}".format(**self._data.__dict__,
                                                                   files=self.getFilesList())
    
    #-- ----- Some setters to ensure consistency. ----- --#
    def setGridR(self, r: int):
        """ Simple setter function to ensure consistency. """
        self._data.R = r
        f = self._files['GRID']
        if not isinstance(f, GridFile): raise ValueError("[511] File is no grid file!")
        f.r = r
    
    def setGridB(self, b: int):
        """ Simple setter function to ensure consistency. """
        self._data.B = b
        f = self._files['GRID']
        if not isinstance(f, GridFile): raise ValueError("[511] File is no grid file!")
        f.b = b
    
    def setGridN(self, n: int):
        """ Simple setter function to ensure consistency. """
        self._data.GRID_NUMBER = n
        f = self._files['GRID']
        if not isinstance(f, GridFile): raise ValueError("[511] File is no grid file!")
        f.number = n
    
    def setGridT(self, t: str):
        """ Simple setter function to ensure consistency. """
        self._data.TYPE = t
        f = self._files['GRID']
        if not isinstance(f, GridFile): raise ValueError("[511] File is no grid file!")
        f.type = t
    
    def setExtparDate(self, d: int):
        """ Simple setter function to ensure consistency. """
        self._data.EXTPAR_DATE = d
        if 'EXTPAR' in self._files:
            f = self._files['EXTPAR']
            if not isinstance(f, ExtparFile): raise ValueError("[511] File is no grid file!")
            f.date = idate2date(d)
    
    def setExtparTiles(self, t: bool):
        """ Simple setter function to ensure consistency. """
        self._data.EXTPAR_TILES = t
        if 'EXTPAR' in self._files:
            f = self._files['EXTPAR']
            if not isinstance(f, ExtparFile): raise ValueError("[511] File is no grid file!")
            f.tiles = t
    
    def setStartDate(self, d: Union[str, int]):
        """ Simple setter function to ensure consistency. """
        if isinstance(d, str):
            di = int(d)
            dt = sdate2date(d)
        elif isinstance(d, int):
            di = d
            dt = idate2date(d)
        else:
            raise TypeError(f"[512] Start date can only be str or int, not {type(d)}.")
        self._data.SDATE = di
        #-- If SDATE does not contain the hour, set it to zero
        if self._data.SDATE < 100000000: self._data.SDATE *= 100
        for f in self._icbcFiles:
            f.date = dt
    
    #-- ----- Some getters. ----- --#
    def type(self):
        """ Simple getter for the type of the grid file. """
        return self._data.TYPE
    
    def index(self):
        """ Simple getter for the index of the domain. """
        return self._data.DOM
    
    def getGrid(self) -> GridFile:
        """ Simple getter for the grid file. Raises on error. """
        gridFile = self._files.get('GRID')
        if not isinstance(gridFile, GridFile):
            raise RuntimeError("[512] No grid file present in the current domain.")
        return gridFile
    
    def getFile(self, kind: str, date: Optional[datetime] = None):
        """ Simple getter for the associated files. """
        if kind in self._files:
            return self._files[kind]
        else:
            cl = CLASS_OF_KIND.get(kind, UnknownDomainFile)
            f = None
            for icbcFile in self._icbcFiles:
                if not isinstance(icbcFile, cl): continue
                if date != None and icbcFile.date != date: continue
                if f == None:
                    f = icbcFile
                else:
                    raise RuntimeError(f"[512] Multiple files found: {f!r}, {icbcFile!r}")
            return f
    
    def getFilesList(self) -> list[InputFile]:
        """ Simple getter for the associated files. """
        files = list(self._files.values())
        files.extend(self._icbcFiles)
        return files
    
    #-- ----- Methods for setting up domains. ----- --#
    def addFile(self, kind: str, raiseIfExists=False):
        """
        Add a specific kind of file (e.g. GRID, EXTPAR, ...) to
        the domain. A corresponding file object will be created.
        :param kind: kind of file
        :param raiseIfExists: raise an error if the file already exists
        """
        #-- Do not add any files except a grid to a
        #-- radiation grid domain.
        if kind != 'GRID' and self._data.TYPE == 'R':
            return
        if kind in self._files:
            if raiseIfExists:
                raise KeyError(f"[512] File of kind {kind!r} already exists! Maybe you added two files of the same kind?")
            else: return
        cl = CLASS_OF_KIND.get(kind, UnknownDomainFile)
        if cl == ArtIcbcFile or cl == EmissFile:
            newFile = cl(kind=kind)
        else:
            newFile = cl()
        newFile.logger.setLevel(self.logger.level)
        self.logger.debug(f"Added file of {kind=}: {newFile!r}")
        if kind != 'GRID':
            gridfile = self._files['GRID']
            if gridfile != None:
                if not isinstance(gridfile, GridFile): raise ValueError("[511] File is no grid file!")
                if not (isinstance(newFile, ExtparFile) or
                        isinstance(newFile, IcbcFile) or
                        isinstance(newFile, EmissFile) or
                        isinstance(newFile, UnknownDomainFile)):
                    raise ValueError(f"[511] File for {kind!r} is no correct file type.")
                newFile.grid = gridfile
        #-- Store the new file in the internal structures.
        if isinstance(newFile, IcbcFile):
            self._icbcFiles.append(newFile)
            if self._data.SDATE > 0:
                newFile.date = idate2date(self._data.SDATE)
        else:
            self._files[kind] = newFile
        self.fileTypes.add(kind)
        return newFile
    
    def addFileHandle(self, newFile: Union[GridFile, ExtparFile, IcbcFile, EmissFile]):
        """ Add an existing file to a domain. """
        if not (isinstance(newFile, GridFile) or
                isinstance(newFile, ExtparFile) or
                isinstance(newFile, IcbcFile) or
                isinstance(newFile, EmissFile)):
            raise ValueError(f"[511] New file of type {type(newFile)} is no domain file type.")
        kind = newFile.getKind()
        if kind in self._files:
            raise KeyError(f"[512] File of kind {kind!r} already exists! Maybe you added two files of the same kind?")
        self.logger.debug(f"Adding file of {kind=}: {newFile!r}")
        #-- Store the new file in the internal structures.
        if isinstance(newFile, IcbcFile):
            self._icbcFiles.append(newFile)
            if newFile.date == None and self._data.SDATE > 0:
                newFile.date = idate2date(self._data.SDATE)
        else:
            self._files[kind] = newFile
        self.fileTypes.add(kind)
    
    def createParentDomain(self):
        """
        Create a parent domain to the current domain. This is typically
        used for creating the radiation grid domain from the global domain.
        """
        parentDomain = IconDomain(dom_index = self._data.DOM - 1)
        self.parent = parentDomain
        parentDomain.child = self
        parentDomain.setGridR(self._data.R)
        parentDomain.setGridB(self._data.B - 1)
        parentDomain.setGridN(self._data.GRID_NUMBER - 1)
        if self._data.TYPE == 'G': parentDomain.setGridT('R')
        return parentDomain
    
    def createChildDomain(self):
        """
        Create a child domain to the current domain. This is typically
        used for creating a nest.
        """
        if self._data.SDATE >= 0:
            sd = self._data.SDATE
        else:
            sd = None
        childDomain = IconDomain(dom_index = self._data.DOM + 1, start_date=sd)
        childDomain.parent = self
        self.child = childDomain
        childDomain.setGridR(self._data.R)
        childDomain.setGridB(self._data.B + 1)
        if self._data.GRID_NUMBER >= 0:
            childDomain.setGridN(self._data.GRID_NUMBER + 1)
        return childDomain

    def populate(self, data: dict):
        """
        Populate the class attributes with content of the data dict.
        :param data: dict with data corresponding to the grid info.
        """
        self.logger.debug("Populating domain:")
        self.logger.debug(f"Before {self!r}.")
        #-- Grid
        if 'R' in data:
            self.setGridR(int(data['R']))
        if 'B' in data:
            self.setGridB(int(data['B']))
        if 'TYPE' in data:
            self.setGridT(str(data['TYPE']))
        if 'GRID_NUMBER' in data:
            self.setGridN(int(data['GRID_NUMBER']))
        #-- Extpar
        if (('EXTPAR_DATE' in data or 'EXTPAR_TILES' in data)
            and not 'EXTPAR' in self._files):
            self.addFile('EXTPAR')
        if 'EXTPAR_DATE' in data:
            self.setExtparDate(int(data['EXTPAR_DATE']))
        if 'EXTPAR_TILES' in data:
            self.setExtparTiles(bool(data['EXTPAR_TILES']))
        #-- Further stuff
        if 'ART_IO_SUFFIX' in data:
            self._data.ART_IO_SUFFIX = str(data['ART_IO_SUFFIX'])
        if 'SKIP_FILES' in data:
            self.skipFiles = data['SKIP_FILES']

        #-- Set non-default file names
        filenames = data.get('FILENAMES', {})
        for fnt in CLASS_OF_KIND.keys():
            filename = str(data.get('FULLNAME', {}).get(fnt, ''))
            if len(filename) > 0:
                filenames[fnt] = filename
            filename = str(data.get('BASENAME', {}).get(fnt, ''))
            if len(filename) > 0:
                p = filename + "_{GRID_NUMBER:04d}_R{R:02d}B{B:02d}"
                if fnt == 'GRID' or fnt == 'EXTPAR':
                    if len(self._data.TYPE) > 0: p += "_{TYPE:s}"
                    if fnt == 'EXTPAR':
                        if self._data.EXTPAR_DATE > 0:
                            p += "_{0:d}".format(self._data.EXTPAR_DATE)
                        p += "{TILES:s}"
                elif (fnt == 'IFS' or
                      fnt == 'DWDFG' or
                      fnt == 'DWDANA' or
                      fnt == 'ERA5' or
                      fnt == 'ERAI'):
                    p += "_{SDATE}"
                p += ".{FMT:s}"
                filenames[fnt] = p
        for fnt, name in filenames.items():
            f = self.getFile(fnt)
            if f == None: f = self.addFile(fnt)
            if isinstance(f, InputFile): f.setSourceNamePatterns(name)
        self.logger.debug(f"After {self!r}.")
    
    def evalPatterns(self):
        """ Simple wrapper to evaluate all patterns for all files. """
        filesList = self.getFilesList()
        for f in filesList:
            f.evalPattern2Source(params=self._data.__dict__)
            f.evalPattern2Name(params=self._data.__dict__)
