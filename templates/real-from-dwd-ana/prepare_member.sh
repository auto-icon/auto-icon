#!/bin/bash -l

# Copyright 2023 - 2024 Karlsruhe Institute of Technology (KIT) and Ludwig-Maximilians-Universität München (LMU)
# SPDX-FileContributor: Oriol Tinto Prims, Fabian Jakub
#
# SPDX-License-Identifier: BSD-3-Clause

# Get some variables provided by autosubmit.
WORKDIR=%HPCROOTDIR%
STARTDATE=%SDATE%
MEMBER=%MEMBER%

# Common folder with data needed for all simulations
COMMON_INIDATA_FOLDER=${WORKDIR}/inidata
# Common folder for the same start date
COMMON_DATE_FOLDER=${WORKDIR}/${STARTDATE}/inidata

# Member folder
MEMBER_DIR=${WORKDIR}/${STARTDATE}/${MEMBER}

# Create member folder and go there
mkdir -p ${MEMBER_DIR}

cd ${MEMBER_DIR} || exit


# Link all files from the common inidata folder and the common date folder
ln -sf ${COMMON_INIDATA_FOLDER}/* .
ln -sf ${COMMON_DATE_FOLDER}/* .
