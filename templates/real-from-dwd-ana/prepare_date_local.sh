#!/bin/bash -l

# Copyright 2023 - 2024 Karlsruhe Institute of Technology (KIT) and Ludwig-Maximilians-Universität München (LMU)
# SPDX-FileContributor: Oriol Tinto Prims, Fabian Jakub
#
# SPDX-License-Identifier: BSD-3-Clause

# This script is executed on the machine at which autosubmit is executed.
# and will be used if the initial conditions are in this same system.

# Because it can happen that the initial conditions as well and the execution happens in the local
# system we need to define these two variables:
DATA_IS_LOCAL=%SIMULATION.INITIAL_CONDITIONS.LOCAL%

if [ "%HPCARCH%" == "local" ]; then
  RUN_MACHINE_IS_LOCAL="True"
else
  RUN_MACHINE_IS_LOCAL="False"
fi



if [ "${DATA_IS_LOCAL}" == "True" ]; then
  # Get some variables provided by autosubmit.
  WORKDIR=%HPCROOTDIR%
  STARTDATE=%SDATE%
  HPCUSER=%HPCUSER%
  HPCHOST=%HPCHOST%

  # Define date directory, create it and go there
  COMMON_DATE_FOLDER=${WORKDIR}/${STARTDATE}/inidata

  AN_MEMBER=$(printf "%03d" %SIMULATION.INITIAL_CONDITIONS.MEMBER%)
  INITIAL_CONDITIONS_PARENT_FOLDER=%SIMULATION.INITIAL_CONDITIONS.PARENT_FOLDER%
  INITIAL_CONDITIONS_PATH=${INITIAL_CONDITIONS_PARENT_FOLDER}/${STARTDATE:0:6}/${STARTDATE:0:8}T00

  AN_SOURCE=$(find "${INITIAL_CONDITIONS_PATH}" -name "igaf*00.m${AN_MEMBER}.grb" | sort | tail -n 1)
  FG_SOURCE=$(find "${INITIAL_CONDITIONS_PATH}" -name "igfff00030000.m${AN_MEMBER}.grb" | sort | tail -n 1)

  AN_FILE=$(basename "${AN_SOURCE}")
  FG_FILE=$(basename "${FG_SOURCE}")

  # Find files
  if [ ! -f "${AN_SOURCE}" ]; then
    echo "Analysis file for date ${STARTDATE} not found!"
    exit 1
  fi

  if [ ! -f "${FG_SOURCE}" ]; then
    echo "FG file for date ${STARTDATE} not found!"
    exit 1
  fi


  # Check if we copy the initial conditions from the local system or the remote one
  if [ "${RUN_MACHINE_IS_LOCAL}" != "True" ]; then
    # Create member folder
    ssh "${HPCUSER}@${HPCHOST}" mkdir -p ${COMMON_DATE_FOLDER}

    # Save filenames to be used later by other scripts.
    echo "${AN_FILE}" > an_file.txt
    echo "${FG_FILE}" > fg_file.txt
    rsync -v an_file.txt "${HPCUSER}@${HPCHOST}":"${COMMON_DATE_FOLDER}/an_file.txt"
    rsync -v fg_file.txt "${HPCUSER}@${HPCHOST}":"${COMMON_DATE_FOLDER}/fg_file.txt"

    # Remove temporary files.
    rm an_file.txt
    rm fg_file.txt

    # Copy the first-guess and analysis files.
    rsync -v "${FG_SOURCE}" "${HPCUSER}@${HPCHOST}":"${COMMON_DATE_FOLDER}/${FG_FILE}"
    rsync -v "${AN_SOURCE}" "${HPCUSER}@${HPCHOST}":"${COMMON_DATE_FOLDER}/${AN_FILE}"

    # Change permissions to read only.
    ssh "${HPCUSER}@${HPCHOST}" chmod 440 "${COMMON_DATE_FOLDER}/*"
  else
    # Create member folder and go there
    mkdir -p ${COMMON_DATE_FOLDER}
    cd ${COMMON_DATE_FOLDER} || exit

    # Save filenames to be used later by other scripts.
    echo "${AN_FILE}" > an_file.txt
    echo "${FG_FILE}" > fg_file.txt

    # Copy the first-guess and analysis files.
    cp "${FG_SOURCE}" "${FG_FILE}"
    cp "${AN_SOURCE}" "${AN_FILE}"

    # Change permissions to read only.
    chmod 440 ./*
  fi
fi