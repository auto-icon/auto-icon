#!/usr/bin/python3

#-- > Clean up restart files during runtime. < --#

# Copyright 2023 - 2024 Karlsruhe Institute of Technology (KIT) and Ludwig-Maximilians-Universität München (LMU)
# SPDX-FileContributor: Andreas Baer
#
# SPDX-License-Identifier: BSD-3-Clause

import sys
sys.path.append("%PYTHON_ENVIRONMENT.MODULES_DIR%")

import logging
from pathlib import Path
import shutil

from experiment_config import FullExperimentConfig
import icon_file_handler as ifh


def getRestartFilesToDelete(expconf: FullExperimentConfig) -> list[Path]:
    """
    Create the list of restart files.
    :param expconf: full experiment configuration
    """
    logger = logging.getLogger(__name__)
    logger.setLevel(expconf.LOGLEVEL)
    expconf.check(level="member")
    FREQUENCY = "%JOBS.CLEAN_RESTART.FREQUENCY%"
    try: NFILESTOKEEP = int(FREQUENCY)
    except ValueError: NFILESTOKEEP = 1
    logger.info(f"Keeping {NFILESTOKEEP} restart files per domain.")

    outdir = Path(expconf.OUTDIR, expconf.SDATE, expconf.MEMBER)
    fn_domains = Path(expconf.WORKFLOWDIR, expconf.SDATE, "domains.pkl")
    domains = ifh.loadDomains(fn_domains)
    restartFiles = []
    for domain in domains:
        if domain.type() == 'R': continue
        currentFiles = []
        gridName = domain.getGrid().filepath
        if gridName == None: continue
        gridStem = gridName.stem
        # TODO(abaer): get pattern from namelist
        patterns = ["multifile_restart*.mfr",
                    gridStem + "_restart_*"]
        for pattern in patterns:
            currentFiles.extend(outdir.glob(pattern))
        #-- Remove all files that are symlinks.
        currentFiles = [ f for f in currentFiles if not f.is_symlink() ]
        currentFiles.sort()
        logger.debug(f"Found files:")
        logger.debug(currentFiles)
        restartFiles.extend(currentFiles[:-NFILESTOKEEP])
    return restartFiles

#-- ----- Initialization ----- --#
logger = logging.getLogger(__name__)
expconf = FullExperimentConfig()
logging.basicConfig(level=expconf.LOGLEVEL)
logger.setLevel(expconf.LOGLEVEL)
expconf.SDATE = "%SDATE%"
expconf.MEMBER = "%MEMBER%"

restartFiles = getRestartFilesToDelete(expconf)
logger.debug("Files to delete:")
logger.debug(restartFiles)
for filepath in restartFiles:
    if filepath.exists():
        logger.info(f"Deleting {filepath!r}")
        if filepath.is_dir(): shutil.rmtree(filepath)
        else:                 filepath.unlink()
