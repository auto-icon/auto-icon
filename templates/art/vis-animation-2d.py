#!/usr/bin/python3

# Copyright 2023 - 2024 Karlsruhe Institute of Technology (KIT) and Ludwig-Maximilians-Universität München (LMU)
# SPDX-FileContributor: Andreas Baer
#
# SPDX-License-Identifier: BSD-3-Clause

import sys
sys.path.append("%PYTHON_ENVIRONMENT.MODULES_DIR%")

import logging
import itertools
import functools as fct
import xarray as xr
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import matplotlib.figure
from matplotlib.artist import Artist
from matplotlib.colors import Colormap, Normalize
from pathlib import Path
from typing import Iterator, Optional

from experiment_config import FullExperimentConfig
from visualization_utils import DIM_NAMES_PLANE, addContours, baseFigure, createColormap,  processPlots, removeContours, selectSpatialCoordinates, setColorScale, treatData


def saveAnim(ani: animation.Animation,
             varGroup: str,
             varname: str,
             formats: list[str],
             outputPath: Path):
    """ Save the animation in the desired format. """
    if len(formats) == 0: raise ValueError("[411] No output format for the animation specified!")
    for fmt in formats:
        fnout = outputPath / f"anim_{varGroup}_{varname}.{fmt}"
        ani.save(filename=fnout, writer="pillow")

def plotAnimationFrame(frameSelection,
                       fig: matplotlib.figure.Figure,
                       x: xr.DataArray,
                       y: xr.DataArray,
                       var: xr.DataArray,
                       cmap: Colormap,
                       norm: Normalize,
                       visConf: dict,
                       triangGrid: bool) -> Iterator[Artist]:
    """ Plot a single frame. """
    ret = iter([])
    logger = logging.getLogger(__name__)
    var = var.sel(frameSelection)
    if not var.any() or var.count() == 0:
        logger.warning(f"Skipping frame as it is all-zero.")
        return ret
    removeContours(fig=fig)
    addContours(fig=fig, x=x, y=y, z=var, cmap=cmap, norm=norm, triangGrid=triangGrid,
                cconf=visConf.get("COLORBAR", {}), selection=frameSelection)
    #-- The return value is only a dummy, that will be unused.
    return ret

def plotDataset(ds: xr.Dataset,
                varGroup: str,
                visConf: dict,
                grid: Optional[xr.Dataset],
                triangGrid: bool,
                outputPath: Path):
    """ Loop over all variables/time steps/levels. """
    logger = logging.getLogger(__name__)
    indexes = ds.indexes
    variables = visConf["VARIABLES"]
    cconf = visConf.get("COLORBAR", {})
    x, y = selectSpatialCoordinates(ds, grid)
    #-- 'lat' and 'lon' are represented as indexes, ncells is not.
    if ((not triangGrid and len(indexes) == 2) or
        (    triangGrid and len(indexes) == 0)):
        raise RuntimeError("[414] Selection for variables of dataset results in only one frame.")
    ds = treatData(data=ds, visConf=visConf)
    ids = {}
    for key, value in indexes.items():
        if key not in DIM_NAMES_PLANE: ids[key] = value
    keys = list(ids.keys())
    for varname in variables:
        #-- One animation per variable.
        var = ds[varname]
        cmap = createColormap(cconf=cconf)
        norm = setColorScale(var, cconf=cconf)
        fig = baseFigure(cmap=cmap)
        frames = []
        for entry in itertools.product(*ids.values()):
            selection = {}
            for nd, item in enumerate(entry):
                selection[keys[nd]] = item
            frames.append(selection)
        #-- Init and Update functions.
        initfunc   = fct.partial(plotAnimationFrame, frameSelection=frames[0], fig=fig, x=x, y=y, var=var,
                                 cmap=cmap, norm=norm, visConf=visConf, triangGrid=triangGrid)
        updatefunc = fct.partial(plotAnimationFrame, fig=fig, x=x, y=y, var=var,
                                 cmap=cmap, norm=norm, visConf=visConf, triangGrid=triangGrid)
        logger.info(f"Creating animation for {varname!r} and frames {frames!r}.")
        ani = animation.FuncAnimation(fig=fig, func=updatefunc, frames=frames, init_func=initfunc,
                                      interval=visConf["ANIM"].get("INTERVAL", 100))
        saveAnim(ani=ani, varGroup=varGroup, varname=str(var.name), formats=visConf.get("OUTPUT", {}).get("ANIM_FORMAT", []),
                 outputPath=outputPath)
        plt.close(fig)


if __name__ == '__main__':
    #-- ----- Logger ----- --#
    logger = logging.getLogger(__name__)
    expconf = FullExperimentConfig()
    logging.basicConfig(level=expconf.LOGLEVEL)
    logger.setLevel(expconf.LOGLEVEL)
    expconf.JOBNAME = "%JOBNAME%"
    expconf.SDATE   = "%SDATE%"
    expconf.MEMBER  = "%MEMBER%"
    processPlots(expconf=expconf, plotType="ANIMATIONS", plotDatasetFunc=plotDataset, concatTimes=True)
