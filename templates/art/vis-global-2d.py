#!/usr/bin/python3

# Copyright 2023 - 2024 Karlsruhe Institute of Technology (KIT) and Ludwig-Maximilians-Universität München (LMU)
# SPDX-FileContributor: Andreas Baer
#
# SPDX-License-Identifier: BSD-3-Clause

import sys
sys.path.append("%PYTHON_ENVIRONMENT.MODULES_DIR%")

import logging
import itertools
import xarray as xr
import matplotlib.pyplot as plt
import matplotlib.figure
from matplotlib.colors import Colormap, Normalize
from pathlib import Path
from typing import Optional

from experiment_config import FullExperimentConfig
from visualization_utils import DIM_NAMES_PLANE, addContours, baseFigure, createColormap, extendFromCconf, processPlots, removeContours, selectSpatialCoordinates, setColorScale, treatData


def savePlot(fig: matplotlib.figure.Figure,
             varGroup: str,
             varname: str,
             formats: list[str],
             outputPath: Path,
             selection={}):
    """ Save the figure in the desired format. """
    if len(formats) == 0: raise ValueError("[411] No output format for the figure specified!")
    for fmt in formats:
        extraArgs = ""
        for key, value in selection.items():
            extraArgs += f"_{key}-{value}"
        fnout = outputPath / f"plot_{varGroup}_{varname}{extraArgs}.{fmt}"
        fig.savefig(fnout, dpi=300)

def plotSingleFigure(fig: Optional[matplotlib.figure.Figure],
                     x: xr.DataArray,
                     y: xr.DataArray,
                     var: xr.DataArray,
                     cmap: Colormap,
                     norm: Normalize,
                     varGroup: str,
                     visConf: dict,
                     outputPath: Path,
                     triangGrid: bool,
                     selection={}):
    """ Plot a single figure. """
    logger = logging.getLogger(__name__)
    if not var.any() or var.count() == 0:
        logger.warning(f"Skipping plot as it is all-zero.")
        return
    closeFig = (fig == None)
    if fig: removeContours(fig=fig)
    else:   fig = baseFigure(cmap)
    addContours(fig=fig, x=x, y=y, z=var, cmap=cmap, norm=norm, triangGrid=triangGrid,
                cconf=visConf.get("COLORBAR", {}), selection=selection)
    savePlot(fig=fig, varGroup=varGroup, varname=str(var.name),
             formats=visConf.get("OUTPUT", {}).get("FORMAT", []),
             outputPath=outputPath, selection=selection)
    if closeFig: plt.close(fig)

def plotDataset(ds: xr.Dataset,
                varGroup: str,
                visConf: dict,
                grid: Optional[xr.Dataset],
                triangGrid: bool,
                outputPath: Path):
    """ Loop over all variables/time steps/levels. """
    logger = logging.getLogger(__name__)
    indexes = ds.indexes
    variables = visConf["VARIABLES"]
    cconf = visConf.get("COLORBAR", {})
    x, y = selectSpatialCoordinates(ds, grid)
    ds = treatData(data=ds, visConf=visConf)
    cmap = createColormap(cconf=cconf)
    #-- Use a single figure per plot. Current workaround for extended colorbars.
    singleFigures = (extendFromCconf(cconf) != None)
    if singleFigures: fig = None
    else: fig = baseFigure(cmap=cmap)
    #-- 'lat' and 'lon' are represented as indexes, ncells is not.
    if ((not triangGrid and len(indexes) == 2) or
        (    triangGrid and len(indexes) == 0)):
        for varname in variables:
            logger.info(f"Plot {varname!r}.")
            var = ds[varname]
            norm = setColorScale(var, cconf=cconf)
            plotSingleFigure(fig=fig, x=x, y=y, var=var, cmap=cmap, norm=norm,
                             varGroup=varGroup, visConf=visConf, outputPath=outputPath,
                             triangGrid=triangGrid)
    else:
        ids = {}
        for key, value in indexes.items():
            if key not in DIM_NAMES_PLANE: ids[key] = value
        keys = list(ids.keys())
        for varname in variables:
            var = ds[varname]
            for entry in itertools.product(*ids.values()):
                selection = {}
                for nd, item in enumerate(entry):
                    selection[keys[nd]] = item
                #-- Do the actual plotting.
                z = var.sel(selection)
                norm = setColorScale(z, cconf=cconf)
                logger.info(f"Plot {varname!r} for {selection!r}.")
                plotSingleFigure(fig=fig, x=x, y=y, var=z, cmap=cmap, norm=norm,
                                 varGroup=varGroup, visConf=visConf, outputPath=outputPath,
                                 triangGrid=triangGrid, selection=selection)
    if fig: plt.close(fig)


if __name__ == '__main__':
    #-- ----- Logger ----- --#
    logger = logging.getLogger(__name__)
    expconf = FullExperimentConfig()
    logging.basicConfig(level=expconf.LOGLEVEL)
    logger.setLevel(expconf.LOGLEVEL)
    expconf.JOBNAME = "%JOBNAME%"
    expconf.SDATE   = "%SDATE%"
    expconf.MEMBER  = "%MEMBER%"
    processPlots(expconf=expconf, plotType="PLOTS", plotDatasetFunc=plotDataset)
