# Copyright 2023 - 2024 Karlsruhe Institute of Technology (KIT) and Ludwig-Maximilians-Universität München (LMU)
# SPDX-FileContributor: Oriol Tinto Prims, Fabian Jakub
#
# SPDX-License-Identifier: BSD-3-Clause

# Get some variables provided by autosubmit.
WORKDIR=%HPCROOTDIR%
ICON_VERSION=%ICON_VERSION%

STARTDATE=%SDATE%

# Define rundir
RUNDIR=${WORKDIR}/${STARTDATE}/ideal

# Go to the ideal rundir
cd ${RUNDIR}

# Activate spack
. ${WORKDIR}/proj/platforms/common/spack_utils.sh
load_spack "%spack.init%" "%spack.root%" "%spack.url%" "%spack.branch%" "%spack.externals%" "%spack.compiler%" "%spack.disable_local_config%" "%spack.user_cache_path%" "%spack.user_config_path%" "%spack.upstreams%"


# Load icon module
spack load --first icon-nwp@%ICON_VERSION%

# Set environment variable for eccodes-dwd definitions:
source ${WORKDIR}/eccodes_defs.env

# Run icon
srun icon
